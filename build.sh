#!/usr/bin//env bash

build_type="Release"
script=$(readlink -f $0)
src_root_path=`dirname $script`
build_root_path="build/cilab-gimiax-3party-${build_type}/"

# getting number of cores
n_cores=$(grep -c ^processor /proc/cpuinfo)
n_cores=$(( $n_cores * 2 ))

echo "$src_root_path"
cd $src_root_path && cd ..
[ -d "${build_root_path}" ] || mkdir -p "${build_root_path}"
cd "${build_root_path}"

cmake -DCMAKE_BUILD_TYPE=${build_type} $src_root_path && \
    echo "compilation process is running in background!!!" && \
    echo " just check in source dir compile-out.log" && \
    ( make -j $n_cores &> "${src_root_path}/compile-out.log"; ) &
