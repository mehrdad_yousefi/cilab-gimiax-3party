# -----------------------
# Project CLAPACK-3.2.1
# -----------------------
PROJECT(CLAPACK-3.2.1)

# Register as third party
REGISTER_THIRDPARTY_PROJECT_NAME(${PROJECT_NAME}
    "The CLAPACK Project.")

# Only run this 'if' if in compile mode, not during third party configuration
IF(${PROJECT_NAME}_TO_BE_COMPILED)

    # do not build testing
    MARK_AS_ADVANCED(BUILD_TESTING)
    SET(BUILD_TESTING OFF CACHE BOOL "")
    # only build 'double' value related classes
    MARK_AS_ADVANCED(BUILD_DOUBLE)
    SET(BUILD_DOUBLE ON CACHE BOOL "")
    MARK_AS_ADVANCED(BLAS_DOUBLE)
    SET(BLAS_DOUBLE ON CACHE BOOL "")

    # Set subdirs: where to find other CMakeLists.txt
    SUBDIRS(clapack)

    # Help other projects use this one by creating
    # the XXXConfig.cmake file and UseXXX.cmake

    # Project include directories
    SET(${PROJECT_NAME}_INCLUDE_DIRS
        "${${PROJECT_NAME}_SOURCE_DIR}/clapack/INCLUDE")

    # Project libraries directories
    SET(${PROJECT_NAME}_LIBRARY_DIRS "${LIBRARY_OUTPUT_DIRECTORY}")

    # Project libraries
    SET(${PROJECT_NAME}_LIBRARIES
        blas
		lapack
        f2c)

    # The project "use" file var
    SET(${PROJECT_NAME}_USE_FILE "${${PROJECT_NAME}_BINARY_DIR}/Use${PROJECT_NAME}.cmake")
	
    # Create the config.cmake file with these vars
    CONFIGURE_FILE(
        "${${PROJECT_NAME}_SOURCE_DIR}/${PROJECT_NAME}Config.cmake.in"
        "${${PROJECT_NAME}_BINARY_DIR}/${PROJECT_NAME}Config.cmake"
        @ONLY
        IMMEDIATE)

    # Create the use.cmake file
    CONFIGURE_FILE(
        "${${PROJECT_NAME}_SOURCE_DIR}/Use${PROJECT_NAME}.cmake.in"
        "${${PROJECT_NAME}_USE_FILE}"
        COPYONLY
        IMMEDIATE)

ENDIF(${PROJECT_NAME}_TO_BE_COMPILED)
