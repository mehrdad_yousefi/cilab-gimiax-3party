# Define settings for Intel Compiler in Altix 350
# increase heap limit for WIN32. Assumes /Zm1000 is set by ITK or CMAKE
MACRO(INCREASE_MSVC_HEAP_LIMIT _value)
IF(WIN32)
 STRING(REPLACE /Zm1000 "/Zm${_value}" CMAKE_CXX_FLAGS ${CMAKE_CXX_FLAGS})  
ENDIF(WIN32)
ENDMACRO(INCREASE_MSVC_HEAP_LIMIT)


# suppress some warnings in VC8 about using unsafe/deprecated c functions
MACRO(SUPPRESS_VC8_DEPRECATED_WARNINGS)
IF(MSVC90)
  ADD_DEFINITIONS(-D_CRT_SECURE_NO_WARNINGS -D_CRT_NONSTDC_NO_WARNINGS)
ENDIF(MSVC90)
ENDMACRO(SUPPRESS_VC8_DEPRECATED_WARNINGS)

# Supress warning LNK4089: all references to 'ADVAPI32.dll' discarded by /OPT:REF
MACRO(SUPPRESS_LINKER_WARNING_4089 _target)
IF(WIN32)
  SET_TARGET_PROPERTIES(${_target} PROPERTIES LINK_FLAGS "/IGNORE:4089")
ENDIF(WIN32)
ENDMACRO(SUPPRESS_LINKER_WARNING_4089)

# Supress warning C4251: 'blTestParams::m_strVectorInputBaseName' : class 'std::vector<_Ty>'
# needs to have dll-interface to be used by clients of class 'blTestParams'
# warning C4275: non dll-interface class 'blObject' used as base for
# dll-interface class 'blSignalAnnotation'
MACRO(SUPPRESS_COMPILER_WARNING_DLL_EXPORT _target)
IF(WIN32)
  GET_TARGET_PROPERTY(oldProps ${_target} COMPILE_FLAGS)
  if (${oldProps} MATCHES NOTFOUND)
    SET(oldProps "")
  endif(${oldProps} MATCHES NOTFOUND)
  SET(newProperties "${oldProps} /wd4251 /wd4275")
  SET_TARGET_PROPERTIES(${_target} PROPERTIES COMPILE_FLAGS "${newProperties}" )
ENDIF(WIN32)
ENDMACRO(SUPPRESS_COMPILER_WARNING_DLL_EXPORT)



