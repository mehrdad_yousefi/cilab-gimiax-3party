# -------------
# Project LOG4CPLUS
# -------------
PROJECT(LOG4CPLUS)

# Register as third party
REGISTER_THIRDPARTY_PROJECT_NAME(${PROJECT_NAME}
    "C++ logging library.")

# Only run this 'if' if in compile mode, not during third party configuration
IF(${PROJECT_NAME}_TO_BE_COMPILED)

    # Set subdirs: where to find other CMakeLists.txt
    SUBDIRS(log4cplus-1.2.0-rc2)

    # Help other projects use this one by creating
    # the XXXConfig.cmake file and UseXXX.cmake

    # Project include directories
    SET(${PROJECT_NAME}_INCLUDE_DIRS
        "${${PROJECT_NAME}_SOURCE_DIR}/log4cplus-1.2.0-rc2/include"
        "${${PROJECT_NAME}_BINARY_DIR}/log4cplus-1.2.0-rc2/include")

    # Project libraries directories
    SET(${PROJECT_NAME}_LIBRARY_DIRS "${LIBRARY_OUTPUT_DIRECTORY}")

    # Project libraries
    SET(${PROJECT_NAME}_LIBRARIES log4cplus)

    # The project "use" file var
    SET(${PROJECT_NAME}_USE_FILE "${${PROJECT_NAME}_BINARY_DIR}/Use${PROJECT_NAME}.cmake")
     
    # Create the config.cmake file with these vars
    CONFIGURE_FILE(
        "${${PROJECT_NAME}_SOURCE_DIR}/${PROJECT_NAME}Config.cmake.in"
        "${${PROJECT_NAME}_BINARY_DIR}/${PROJECT_NAME}Config.cmake"
        @ONLY
        IMMEDIATE)

    # Create the use.cmake file
    CONFIGURE_FILE(
        "${${PROJECT_NAME}_SOURCE_DIR}/Use${PROJECT_NAME}.cmake.in"
        "${${PROJECT_NAME}_USE_FILE}"
        COPYONLY
        IMMEDIATE)

IF(WIN32)
 IF( MSVC) ## FOR GIMIAS: NOT SURE THIS IS NEEDED FOR OTHER THAN MSVC
  ADD_DEFINITIONS(
  /DUNICODE
  )
 ENDIF(MSVC)
ELSE(WIN32)
 ## FOR GIMIAS: NOT SURE THIS IS NEEDED FOR LINUX
 ADD_DEFINITIONS(-DUNICODE)
ENDIF(WIN32)
  
ENDIF(${PROJECT_NAME}_TO_BE_COMPILED)
