-----------------------------------
LOG4CPLUS: logging library
-----------------------------------

Main Web: http://sourceforge.net/p/log4cplus/wiki/Home/
Version: 1.2.0-rc2
Web: http://sourceforge.net/projects/log4cplus/files/log4cplus-stable/1.2.0/log4cplus-1.2.0-rc2.zip/download

Modifications for GIMIAS:

Modified files: .\log4cplus-1.2.0-rc2\CMakeLists.txt, .\log4cplus-1.2.0-rc2\src\CMakeLists.txt
Modified files: .\log4cplus-1.2.0-rc2\src\fileinfo.cxx, .\log4cplus-1.2.0-rc2\src\lockfile.cxx