----------------------------------
CGNS: CFD General Notation System
----------------------------------
Main Web: http://www.cgns.org

Version: 3.2.1
Web: https://cgns.svn.sourceforge.net/svnroot/cgns/cgns/release/3.2.1/
Web: http://cgns.sourceforge.net/

Modifications for GIMIAS:

 * ./cgns/CMakeLists.txt: set(HDF5_INCLUDE_PATH ${HDF5_INCLUDE_DIR}) + CGNS_ENABLE_HDF5 ON by default
 * ./cgns/src/CMakeLists.txt: guards around tools and set them to OFF by default
