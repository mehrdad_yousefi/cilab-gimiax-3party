/*=========================================================================

  Program:   SparseFieldLevelSetContour
  Module:    $HeadURL: http://svn.slicer.org/Slicer3/trunk/Applications/CLI/SparseFieldLevelSetContour/MeanCurvatureEnergy.h $
  Language:  C++
  Date:      $Date: 2010-04-30 22:13:05 +0200 (vie, 30 abr 2010) $
  Version:   $Revision: 13140 $

  Copyright (c) Brigham and Women's Hospital (BWH) All Rights Reserved.

  See License.txt or http://www.slicer.org/copyright/copyright.txt for details.

==========================================================================*/
#ifndef MEANCURVATUREENERGY
#define MEANCURVATUREENERGY

#include "MeshEnergy.h"

class MeanCurvatureEnergy : public MeshEnergy
{
public:
MeanCurvatureEnergy( MeshData* data ) {meshdata = data;}
virtual ~MeanCurvatureEnergy( ) {}

double eval_energy( const vector<int>& C );
valarray<double> getforce( const list<int>& C);
valarray<double> getforce( const list<int>& C, 
const std::list<int>& L_p1, const list<int>& L_n1,
const std::vector<double>& phi);

};



#endif
