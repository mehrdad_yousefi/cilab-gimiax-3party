from csnGIMIASDef import *
from csnAPIPublic import GetAPI

api = GetAPI("2.5.0-beta")

# Command Line Applications

slicerCLIApps = api.CreateStandardModuleProject("slicerCLIApps", "container")
baseCLIFolder = u'Slicer3/Applications/CLI'

def CreateCLP(_name, _projects = None ):
    clp = CommandLinePlugin( _name )
    clp = api.RewrapProject(clp) # Make sure we're using *our* API version and not the one that the function CommandLinePlugin uses - it could be different
    clp.AddSources( [u'%s/%s.cxx' % (baseCLIFolder,_name)] )
    clp.AddProjects( [generateClp, slicer] )
    if not _projects is None:
        clp.AddProjects( _projects )
    slicerCLIApps.AddProjects( [clp] )
    commandLinePlugins().AddProjects( [clp] )
    if api.GetCompiler().TargetIsWindows():
        clp.AddDefinitions(["/bigobj"], private = 1) 
    return clp

CreateCLP( "Add", [itk] )
CreateCLP( "AffineRegistration", [itk] )
CreateCLP( "BSplineDeformableRegistration", [itk] )
CreateCLP( "Cast", [itk] )
CreateCLP( "CheckerBoard", [itk] )
CreateCLP( "ConfidenceConnected", [itk] )
CreateCLP( "CurvatureAnisotropicDiffusion", [itk] )
#CreateCLP( "DiffusionTensorEstimation", [vtk,itk] )
#CreateCLP( "DiffusionTensorMathematics", [itk] )
#CreateCLP( "ExecutionModelTour" )
CreateCLP( "GaussianBlurImageFilter", [itk] )
CreateCLP( "GradientAnisotropicDiffusion", [itk] )
CreateCLP( "GrayscaleFillHoleImageFilter", [itk] )
CreateCLP( "GrayscaleGrindPeakImageFilter", [itk] )
#CreateCLP( "GrayscaleModelMaker", [itk] )
CreateCLP( "HistogramMatching", [itk] )
CreateCLP( "ImageLabelCombine", [itk] )
CreateCLP( "ImageReadDicomWrite", [itk] )
CreateCLP( "LabelMapSmoothing", [itk] )
CreateCLP( "LinearRegistration", [itk] )
CreateCLP( "Mask", [itk] )
CreateCLP( "MedianImageFilter", [itk] )
CreateCLP( "MergeModels", [vtk,itk] )
#CreateCLP( "ModelMaker", [itk] )
#CreateCLP( "Multiply", [itk] )
CreateCLP( "OrientImage", [itk] )
CreateCLP( "OtsuThresholdImageFilter", [itk] )
CreateCLP( "OtsuThresholdSegmentation", [itk] )
CreateCLP( "PolyDataToLabelmap", [vtk,itk] )
CreateCLP( "ResampleVolume", [itk] )
ResampleVolume2 = CreateCLP( "ResampleVolume2", [itk] )
ResampleVolume2.AddSources( [u'%s/DiffusionApplications/ResampleDTI/itkWarpTransform3D.txx' % baseCLIFolder] )
ResampleVolume2.AddSources( [u'%s/DiffusionApplications/ResampleDTI/itkTransformDeformationFieldFilter.h' % baseCLIFolder] )
ResampleVolume2.AddSources( [u'%s/DiffusionApplications/ResampleDTI/itkTransformDeformationFieldFilter.txx' % baseCLIFolder] )
ResampleVolume2.AddSources( [u'%s/DiffusionApplications/ResampleDTI/dtiprocessFiles/deformationfieldio.h' % baseCLIFolder] )
ResampleVolume2.AddSources( [u'%s/DiffusionApplications/ResampleDTI/dtiprocessFiles/deformationfieldio.cxx' % baseCLIFolder] )
ResampleVolume2.AddSources( [u'%s/DiffusionApplications/ResampleDTI/dtiprocessFiles/dtitypes.h' % baseCLIFolder] )
ResampleVolume2.AddSources( [u'%s/DiffusionApplications/ResampleDTI/dtiprocessFiles/itkHFieldToDeformationFieldImageFilter.h' % baseCLIFolder] )
ResampleVolume2.AddSources( [u'%s/DiffusionApplications/ResampleDTI/dtiprocessFiles/itkHFieldToDeformationFieldImageFilter.txx' % baseCLIFolder] )

CreateCLP( "RigidRegistration", [itk] )
CreateCLP( "Subtract", [itk] )
CreateCLP( "Threshold", [itk] )
CreateCLP( "VotingBinaryHoleFillingImageFilter", [itk] )
CreateCLP( "ZeroCrossingBasedEdgeDetectionImageFilter", [itk] )

