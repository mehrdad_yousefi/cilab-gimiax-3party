--------------------------------------------------------
BOOST: Free peer-reviewed portable C++ source libraries
--------------------------------------------------------
Main Web: http://www.boost.org/

Version: 1.45.0
Web: 

CMake configuration comes from: http://gitorious.org/boost/cmake

Modified files:
 - boost/CMakeLists.txt: set BUILD_SHARED to OFF, do not "boost_add_default_variant" for debug and release
 - libs/CMakeLists.txt: only select specific libs
 - tools/build/BoostConfig.cmake: do not initialise local vars with CMake ones, do not use release as default
 - tools/build/BoostCore.cmake
 - boost/libs/serialization/src/CMakeLists.txt: BUILD_BOOST_WSERIALIZATION to OFF
 - boost/boost/config/auto_link.hpp: removed gd suffix
 
Visual Studio Warning: 
 The modifications try to create only one solution where the original files created two, 
 some options were maybe missed...