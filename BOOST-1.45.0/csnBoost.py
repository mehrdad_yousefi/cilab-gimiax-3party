# Used to configure Boost
from csnAPIPublic import GetAPI
api = GetAPI("2.5.0")

boost = api.CreateThirdPartyProject("Boost")
boost.SetUseFilePath("%s/BOOST-1.45.0/UseBOOST-1.45.0.cmake" % boost.GetBuildFolder())
boost.SetConfigFilePath("%s/BOOST-1.45.0/BOOST-1.45.0Config.cmake" % boost.GetBuildFolder())
