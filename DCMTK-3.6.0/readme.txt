-----------------------------------
DCMTK: the DICOM Toolkit
-----------------------------------
Main Web: http://dicom.offis.de/dcmtk.php.en
Help: http://support.dcmtk.org/docs/

Version: 3.6.0 (2011-01-06)
Web: ftp://dicom.offis.de/pub/dicom/offis/software/dcmtk/dcmtk360/dcmtk-3.6.0.tar.gz

Modified files:
- isolated local log4cplus to allow usage of another

