#ifndef NGLIB_GIMIAS
#define NGLIB_GIMIAS

#include "nglib.h"

namespace nglib {

// Return number of points of STL geometry
long Ng_STL_GetNumPoints (Ng_STL_Geometry * geom );

// Return a point of STL geometry. Index from 1 to 10
void Ng_STL_GetPoint (Ng_STL_Geometry * geom, int iNum, double * p1 );

// Return number of triangles of STL geometry
long Ng_STL_GetNumTriangles (Ng_STL_Geometry * geom );

// Return a triangle of STL geometry. Index from 1 to N
void Ng_STL_GetTriangle (Ng_STL_Geometry * geom, int iNum, int* p, double * nv = NULL );

// Deletes the geometry
void Ng_STL_DeleteGeometry (Ng_STL_Geometry * geom);

// generates mesh, empty mesh be already created.
Ng_Result Ng_STL_OptimizeSurfaceMesh (Ng_STL_Geometry * geom,	Ng_Mesh* mesh,	Ng_Meshing_Parameters * mp);

void Ng_MeshRefinement( Ng_STL_Geometry * geom, Ng_Mesh *mesh );
}

#endif

