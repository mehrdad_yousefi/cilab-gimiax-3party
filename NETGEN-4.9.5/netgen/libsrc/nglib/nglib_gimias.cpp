/*
  
  Interface to the netgen meshing kernel
  
*/
#include <mystdlib.h>
#include <myadt.hpp>

#include <linalg.hpp>
#include <csg.hpp>
#include <stlgeom.hpp>
#include <geometry2d.hpp>
#include <meshing.hpp>
#include <../visualization/soldata.hpp>

#ifdef OCCGEOMETRY
#include <occgeom.hpp>
#endif

//#include <nginterface.h>

//namespace nglib {
#include "nglib.h"
//}

using namespace netgen;

// constants and types:

namespace nglib
{
	// Return number of points of STL geometry
	long Ng_STL_GetNumPoints (Ng_STL_Geometry * geom )
	{
		STLGeometry* ngstlgeometry = (STLGeometry*)geom;

		return ngstlgeometry->GetNP();
	}

	// Return a point of STL geometry. Index from 1 to 10
	void Ng_STL_GetPoint (Ng_STL_Geometry * geom, int iNum, double * p1 )
	{
		STLGeometry* ngstlgeometry = (STLGeometry*)geom;

		Point<3>	point;

		point = ngstlgeometry->GetPoint( iNum );
		p1[ 0 ] = point( 0 );
		p1[ 1 ] = point( 1 );
		p1[ 2 ] = point( 2 );
	}

	// Return number of triangles of STL geometry
	long Ng_STL_GetNumTriangles (Ng_STL_Geometry * geom )
	{
		STLGeometry* ngstlgeometry = (STLGeometry*)geom;

		return ngstlgeometry->GetNT();
	}

	// Return a triangle of STL geometry. Index from 1 to N
	void Ng_STL_GetTriangle (Ng_STL_Geometry * geom,
		int iNum, int* p, double * nv )
	{
		STLGeometry* ngstlgeometry = (STLGeometry*)geom;

		//const Vec<3>		&normal = trig.Normal();

		p[ 0 ] = ngstlgeometry->GetTriangle( iNum ).PNum( 1 );
		p[ 1 ] = ngstlgeometry->GetTriangle( iNum ).PNum( 2 );
		p[ 2 ] = ngstlgeometry->GetTriangle( iNum ).PNum( 3 );

		//if ( nv )
		//{
		//	nv[ 0 ] = normal( 0 );
		//	nv[ 1 ] = normal( 1 );
		//	nv[ 2 ] = normal( 2 );
		//}
			
	}
	
	// Deletes the geometry
	void Ng_STL_DeleteGeometry (Ng_STL_Geometry * geom)
	{
		delete (STLGeometry*)geom;
	} 

	// generates mesh, empty mesh be already created.
	Ng_Result Ng_STL_OptimizeSurfaceMesh (Ng_STL_Geometry * geom,
		Ng_Mesh* mesh,
		Ng_Meshing_Parameters * mp)
	{
		STLGeometry* ngstlgeometry = (STLGeometry*)geom;
		Mesh* me = (Mesh*)mesh;

		mparam.maxh = mp->maxh;
		mparam.meshsizefilename = mp->meshsize_filename;
		mparam.optimize2d = mp->optimize2d;
		//mparam.optsteps2d = mp->optsteps2d;
		stlparam.recalc_h_opt = mp->recalc_h_opt;

		STLSurfaceOptimization (*ngstlgeometry, *me, mparam);

		if (stlparam.recalc_h_opt)
		{
			me -> SetLocalH (ngstlgeometry->GetBoundingBox().PMin() - Vec3d(10, 10, 10),
				ngstlgeometry->GetBoundingBox().PMax() + Vec3d(10, 10, 10),
				mparam.grading);
			me -> LoadLocalMeshSize (mparam.meshsizefilename);	      
			me -> CalcLocalHFromSurfaceCurvature (stlparam.resthsurfmeshcurvfac);
			mparam.optimize2d = "cmsmSm";
			STLSurfaceOptimization (*ngstlgeometry, *me, mparam);
		}

		ngstlgeometry->surfaceoptimized = 1;

		return NG_OK;
	}

	void Ng_MeshRefinement( Ng_STL_Geometry * geom, Ng_Mesh *mesh )
	{
		STLGeometry* ngstlgeometry = (STLGeometry*)geom;
		Mesh * m = (Mesh*)mesh;
		Refinement * ref;


		BisectionOptions biopt;
		biopt.femcode = "fepp";
		biopt.usemarkedelements = 1;

		// Ng_Bisect
		ref = new RefinementSTLGeometry(*ngstlgeometry);
		if(!m->LocalHFunctionGenerated())
			m->CalcLocalH();
		m->LocalHFunction().SetGrading (mparam.grading);
		ref -> Bisect (*m, biopt);
		m -> UpdateTopology();
		m -> GetCurvedElements().BuildCurvedElements (ref, mparam.elementorder);
		delete ref;

	}

}