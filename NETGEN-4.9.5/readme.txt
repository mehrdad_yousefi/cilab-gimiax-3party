Netgen Mesh Generator
NETGEN is an automatic 3d tetrahedral mesh generator 

Version: 4.9.5
Web: http://sourceforge.net/projects/netgen-mesher/files/netgen-mesher/4.9.5/

Netgen is not CMake based. A suggested CMake-based version can be found at https://github.com/philippose/netgen-cmake, by Philippose Rajan.

Modifications for GIMIAS:

- The nglib folder belongs under .\netgen, but was moved inside .\nertgen\libsrc\ for convenience.
- Removed unnecesary sample .\netgen\libsrc\nglib\ng_vol.cpp
- Modifications at line #872 of .\netgen\libsrc\visualization\mvdraw.cpp
- Commented out code at line 218 in .\netgen\libsrc\meshing\msghandler.cpp 


