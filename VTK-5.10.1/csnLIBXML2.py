# Used to configure HDF5
from csnToolkitOpen import *
from csnAPIPublic import GetAPI
api = GetAPI("2.5.0")

libxml2 = api.CreateThirdPartyProject("LIBXML2")
libxml2.SetUseFilePath("%s/LIBXML2/UseLIBXML2.cmake" % libxml2.GetBuildFolder())
libxml2.SetConfigFilePath("%s/LIBXML2/LIBXML2Config.cmake" % libxml2.GetBuildFolder())

# add dlls to be installed
if api.GetCompiler().TargetIsWindows():
    for dll in ["vtklibxml2.dll"]:
        libxml2.AddFilesToInstall(["bin/Debug/%s" % dll], debugOnly = 1)
        libxml2.AddFilesToInstall(["bin/Release/%s" % dll], releaseOnly = 1)
