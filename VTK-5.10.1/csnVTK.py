# Used to configure VTK
from csnToolkitOpen import *
from csnAPIPublic import GetAPI
api = GetAPI("2.5.0")

vtk = api.CreateThirdPartyProject("VTK-5.10.1")
vtk.SetUseFilePath("%s/VTK-5.10.1/UseVTK-5.10.1.cmake" % vtk.GetBuildFolder())
vtk.SetConfigFilePath("%s/VTK-5.10.1/VTK-5.10.1Config.cmake" % vtk.GetBuildFolder())
vtk.AddProjects([hdf5, zlib])

# add dlls to be installed
if api.GetCompiler().TargetIsWindows():
    for dll in (
      "Cosmo.dll",
      "LSDyna.dll",
	  "MapReduceMPI.dll",
	  "mpistubs.dll",
      "VPIC.dll",
	  "vtkalglib.dll",
	  "vtkCharts.dll",
      "vtkCommon.dll",
      "vtkDICOMParser.dll",
      "vtkexoIIc.dll",
      "vtkexpat.dll",
      "vtkFiltering.dll",
      "vtkfreetype.dll",
      "vtkftgl.dll",
      "vtkGenericFiltering.dll",
	  "vtkGeovis.dll",
      "vtkGraphics.dll",
      "vtkHybrid.dll",
      "vtkImaging.dll",
	  "vtkInfovis.dll",
      "vtkIO.dll",
      "vtkjpeg.dll",
      "vtklibxml2.dll",
      "vtkmetaio.dll",
      "vtkNetCDF.dll",
      "vtkNetCDF_cxx.dll",
      "vtkParallel.dll",
      "vtkpng.dll",
	  "vtkproj4.dll",
      "vtkRendering.dll",
	  "vtksqlite.dll",
      "vtksys.dll",
      "vtktiff.dll",
      "vtkverdict.dll",
	  "vtkViews.dll",
      "vtkVolumeRendering.dll",
      "vtkWidgets.dll"
	  ):
        vtk.AddFilesToInstall(["bin/Debug/%s" % dll], debugOnly = 1)
        vtk.AddFilesToInstall(["bin/Release/%s" % dll], releaseOnly = 1)

vtkStatic = api.CreateThirdPartyProject("VTKStatic")
vtkStatic.SetUseFilePath("%s/../Static/VTK-5.10.1/UseVTK-5.10.1.cmake" % vtkStatic.GetBuildFolder())
vtkStatic.SetConfigFilePath("%s/../Static/VTK-5.10.1/VTK-5.10.1Config.cmake" % vtkStatic.GetBuildFolder())
