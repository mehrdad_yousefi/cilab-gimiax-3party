# Used to configure HDF5
from csnToolkitOpen import *
from csnAPIPublic import GetAPI
api = GetAPI("2.5.0")

hdf5 = api.CreateThirdPartyProject("HDF5")
hdf5.AddProjects([zlib])
hdf5.SetUseFilePath("%s/HDF5/UseHDF5.cmake" % hdf5.GetBuildFolder())
hdf5.SetConfigFilePath("%s/HDF5/HDF5Config.cmake" % hdf5.GetBuildFolder())

# add dlls to be installed
if api.GetCompiler().TargetIsWindows():
    for dll in ("vtkhdf5.dll", "vtkhdf5_hl.dll"):
        hdf5.AddFilesToInstall(["bin/Debug/%s" % dll], debugOnly = 1)
        hdf5.AddFilesToInstall(["bin/Release/%s" % dll], releaseOnly = 1)
