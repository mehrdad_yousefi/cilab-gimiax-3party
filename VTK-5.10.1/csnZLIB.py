# Used to configure ZLIB
from csnAPIPublic import GetAPI
api = GetAPI("2.5.0")

zlib = api.CreateThirdPartyProject("ZLIB")
zlib.SetUseFilePath("%s/ZLIB/UseZLIB.cmake" % zlib.GetBuildFolder())
zlib.SetConfigFilePath("%s/ZLIB/ZLIBConfig.cmake" % zlib.GetBuildFolder())

# add dlls to be installed
if api.GetCompiler().TargetIsWindows():
  zlib.AddFilesToInstall(["bin/Debug/vtkzlib.dll"], debugOnly = 1)
  zlib.AddFilesToInstall(["bin/Release/vtkzlib.dll"], releaseOnly = 1)
