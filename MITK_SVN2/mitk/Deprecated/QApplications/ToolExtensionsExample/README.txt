This directory contains an example for how
to compile a couple of new tools into a
shared object without using the MITK build
system.

The idea of tools is used in the MITK functionality
InteractiveSegmentation, which is documented here:

http://mitk.org/documentation/doxygen/QmitkInteractiveSegmentationTechnicalPage.html

A lengthy description of the external extensions
can be found at

http://mitk.org/documentation/doxygen/toolextensions.html

Questions should be posted directly to mitk-users@lists.sourceforge.net

If you have created a nice new tool (e.g. livewire
segmentation (intelligent scissors), level sets, whatever)
and want it to be integrated into MITK, we happily welcome
your code contribution. You can use mitk@dkfz.de for off-mailinglist
communication.

