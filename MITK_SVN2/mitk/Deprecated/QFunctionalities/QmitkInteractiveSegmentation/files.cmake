SET( MOC_H_FILES 
  QmitkInteractiveSegmentation.h 
)

SET( CPP_FILES 
  QmitkInteractiveSegmentation.cpp 
  QmitkInteractiveSegmentationTesting.cpp
)

SET (UI_FILES 
  QmitkInteractiveSegmentationControls.ui
)

