
/*!

\page QmitkMovieMakerUserManual Movie maker

\author Tobias Schwarz, German Cancer Research Center
\version 0.1
\date    2008



Available sections:
  - \ref QmitkMovieMakerUserManualOverview 
  - \ref QmitkMovieMakerUserManualFeatures 
  - \ref QmitkMovieMakerUserManualUsage 

\section QmitkMovieMakerUserManualOverview Overview

MovieMaker is a functionality for easily creating fancy movies from scenes displayed in MITK widgets.
It is also possible to slide through your data, automatically rotate 3D scenes and take screenshots of widgets.

\section QmitkMovieMakerUserManualFeatures Features

 * Can write movies and screenshots from MITK widgets.
 * Can automatically scroll through datasets for movie creation
 * Can also scroll through time series
 
\section QmitkMovieMakerUserManualUsage Usage

\image html QmitkMovieMakerControlArea.jpg "A view of the command area of QmitkMovieMaker"

Description of functions and buttons from top to bottom:

 * Window for stepping: If stepping is set to spatial (see below), slices in this window will be stepped through.
 * Window for recording: What is shown in this winow will be recorded.
 
 Note 1: You can set both stepping and recording window to a one you like by clicking inside of one.
 Note 2: Windows for stepping and recording do not have to be the same.
 
 * Recording options: 
  * The step slider can be used to manually step through the possible slices.
  * Start and Stop buttons should be self explanatory
  * "Write Movie" button opens a file dialog, and after confiming the movie is created according to the options set in the lower fields
  * "Write Screenshot" creates a screenshot of the selected Widget
   
 * Playing options
  * The first section controls whether the movie steps through slices (if a 2D view is selected), 
  rotate the shown scene (if a 3D view is selected), or step through time steps (if set to temporal and a time resolved dataset is selected).
  If set to combined, a combination of both above options is used, with their speed relation set via the S/T Relation Spinbox.
  * In the second section the direction of stepping can be set. Options are: Forward, backward and Ping-Pong, which is back-and-forth. Via the spinbox, stepping speed can be set (total time in seconds).
  
Note 3: Although stepping speed is a total time in sec., this can not always be achieved. As a minimal frame rate of 25 fps is assumed to provide smooth movies,
a dataset with only 25 slices will always be stepped through in 1 sec or faster.

*/

