INCLUDE_DIRECTORIES( ${IP_ROOT}/Functionalities/QmitkDataTreeFilterDemo)

# DataTreeFilterDemo

SET( APPMOD_H ${APPMOD_H} ${IP_ROOT}/Functionalities/QmitkDataTreeFilterDemo/QmitkDataTreeFilterDemo.h )
SET( APPMOD_MOC_H ${APPMOD_MOC_H} ${IP_ROOT}/Functionalities/QmitkDataTreeFilterDemo/QmitkDataTreeFilterDemo.h )
SET( APPMOD_CPP ${APPMOD_CPP} ${IP_ROOT}/Functionalities/QmitkDataTreeFilterDemo/QmitkDataTreeFilterDemo.cpp )

SET (APPMOD_FORMS ${APPMOD_FORMS} ${IP_ROOT}/Functionalities/QmitkDataTreeFilterDemo/QmitkDataTreeFilterDemoControls.ui)
