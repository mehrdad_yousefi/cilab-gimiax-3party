SET( MOC_H_FILES 
  QmitkIGTTutorial.h 
)

SET( CPP_FILES 
  QmitkIGTTutorial.cpp 
  QmitkIGTTutorialTesting.cpp 
)

SET (UI_FILES 
  QmitkIGTTutorialControls.ui
)

