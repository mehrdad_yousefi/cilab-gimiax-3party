# QmitkRigidRegistration


SET( APPMOD_H ${APPMOD_H} QmitkRigidRegistration.h
                          QmitkLoadPresetDialog.h )
SET( APPMOD_MOC_H ${APPMOD_MOC_H} QmitkRigidRegistration.h
                                  QmitkLoadPresetDialog.h )
SET( APPMOD_CPP ${APPMOD_CPP} QmitkRigidRegistration.cpp
                              QmitkLoadPresetDialog.cpp )
SET (APPMOD_FORMS ${APPMOD_FORMS} QmitkRigidRegistrationControls.ui
          QmitkRigidRegistrationSelector.ui )