# QmitkDeformableRegistration

SET( APPMOD_H ${APPMOD_H} QmitkDeformableRegistration.h )

SET( APPMOD_MOC_H ${APPMOD_MOC_H} QmitkDeformableRegistration.h )

SET( APPMOD_CPP ${APPMOD_CPP}   QmitkDeformableRegistration.cpp )

SET (APPMOD_FORMS ${APPMOD_FORMS} QmitkDeformableRegistrationControls.ui 
            QmitkDemonsRegistrationControls.ui )