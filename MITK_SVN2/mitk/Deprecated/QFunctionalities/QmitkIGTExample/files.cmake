SET( MOC_H_FILES 
  QmitkIGTExample.h 
)

SET( CPP_FILES 
  QmitkIGTExample.cpp 
  QmitkIGTExampleTesting.cpp 
)

SET (UI_FILES 
  QmitkIGTExampleControls.ui
)

