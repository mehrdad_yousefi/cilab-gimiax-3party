/*=========================================================================

Program:   Medical Imaging & Interaction Toolkit
Language:  C++
Date:      $Date: 2008-02-13 14:07:17 +0100 (mié, 13 feb 2008) $
Version:   $Revision: 13599 $ 
 
Copyright (c) German Cancer Research Center, Division of Medical and
Biological Informatics. All rights reserved.
See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notices for more information.

=========================================================================*/
#ifndef QMITK_BOOLPROPERTYEDITOR_H_INCLUDED
#define QMITK_BOOLPROPERTYEDITOR_H_INCLUDED

#include <QmitkBoolPropertyView.h>

/// @ingroup Widgets
class QMITK_EXPORT QmitkBoolPropertyEditor : public QmitkBoolPropertyView
{
  Q_OBJECT

  public:
    
    QmitkBoolPropertyEditor( const mitk::BoolProperty*, QWidget* parent, const char* name = 0 );
    virtual ~QmitkBoolPropertyEditor();
      
  protected:

    virtual void PropertyRemoved();

  protected slots:

    void onToggle(bool on);

};

#endif

