/*=========================================================================
 
Program:   Medical Imaging & Interaction Toolkit
Module:    $RCSfile: mitkPropertyManager.cpp,v $
Language:  C++
Date:      $Date: 2008-04-24 18:31:42 +0200 (jue, 24 abr 2008) $
Version:   $Revision: 1.12 $
 
Copyright (c) German Cancer Research Center, Division of Medical and
Biological Informatics. All rights reserved.
See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.
 
This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notices for more information.
 
=========================================================================*/

#include "QmitkToolGUIArea.h"

QmitkToolGUIArea::QmitkToolGUIArea( QWidget* parent, const char* name, WFlags f )
:QWidget(parent,name,f)
{
}

QmitkToolGUIArea::~QmitkToolGUIArea()
{
}

