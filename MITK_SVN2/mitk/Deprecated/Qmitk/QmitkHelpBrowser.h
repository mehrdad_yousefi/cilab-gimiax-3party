/*=========================================================================
 
Program:   Medical Imaging & Interaction Toolkit
Language:  C++
Date:      $Date: 2008-02-13 14:07:17 +0100 (mié, 13 feb 2008) $
Version:   $Revision: 13599 $
 
Copyright (c) German Cancer Research Center, Division of Medical and
Biological Informatics. All rights reserved.
See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.
 
This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notices for more information.
 
=========================================================================*/
#ifndef HELPWINDOW_H
#define HELPWINDOW_H

#include <qmainwindow.h>
#include <qtextbrowser.h>
#include <mitkCommon.h>

class QComboBox;
class QPopupMenu;

class QMITK_EXPORT QmitkHelpBrowser : public QMainWindow
{
    Q_OBJECT
public:
    QmitkHelpBrowser( const QString& home_,  const QString& path, QWidget* parent = 0, const char *name=0 );
    ~QmitkHelpBrowser();

private slots:
    void setBackwardAvailable( bool );
    void setForwardAvailable( bool );

    void sourceChanged( const QString& );
    void print();

    void pathSelected( const QString & );

private:

    QTextBrowser* browser;
    QComboBox *pathCombo;
    int backwardId, forwardId;

};

#endif

