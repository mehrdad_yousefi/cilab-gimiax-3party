/*=========================================================================

Program:   Medical Imaging & Interaction Toolkit
Language:  C++
Date:      $Date: 2008-09-29 18:39:38 +0200 (lun, 29 sep 2008) $
Version:   $Revision: 15346 $

Copyright (c) German Cancer Research Center, Division of Medical and
Biological Informatics. All rights reserved.
See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notices for more information.

=========================================================================*/

#include "QmitkAbortEventFilter.h"
#include "QmitkRenderingManager.h"

#include "mitkDataTree.h"
#include "mitkGeometry3D.h"
#include "mitkBaseRenderer.h"
#include "mitkSliceNavigationController.h"

#include <qapplication.h>


QmitkRenderingManager
::QmitkRenderingManager()
{
  qApp->installEventFilter( QmitkAbortEventFilter::GetInstance() );
}


void
QmitkRenderingManager
::DoMonitorRendering()
{
  QmitkAbortEventFilter::GetInstance()->ProcessEvents();
}


void
QmitkRenderingManager
::DoFinishAbortRendering()
{
  QmitkAbortEventFilter::GetInstance()->IssueQueuedEvents();
}


QmitkRenderingManager
::~QmitkRenderingManager()
{
}


void
QmitkRenderingManager
::GenerateRenderingRequestEvent()
{
  QApplication::postEvent( this, new QmitkRenderingRequestEvent );
}


bool 
QmitkRenderingManager
::event( QEvent *event ) 
{
  if ( event->type() == (QEvent::Type) QmitkRenderingRequestEvent::RenderingRequest )
  {
    // Directly process all pending rendering requests
    this->UpdateCallback();
    return true;
  }

  return false;
}

