<!DOCTYPE CW><CW>
<customwidgets>
    <customwidget>
        <class>QmitkSelectableGLWidget</class>
        <header location="global">QmitkSelectableGLWidget.h</header>
        <sizehint>
            <width>-1</width>
            <height>-1</height>
        </sizehint>
        <container>0</container>
        <sizepolicy>
            <hordata>3</hordata>
            <verdata>3</verdata>
        </sizepolicy>
        <pixmap>
            <data format="XPM.GZ" length="45">789cd3d7528808f055d0d2e72a2e492cc94c5648ce482c52d04a29cdcdad8c8eb5ade6523250004143a55a6b2e0026630c4f</data>
        </pixmap>
    </customwidget>
    <customwidget>
        <class>QmitkLevelWindowWidget</class>
        <header location="global">../../AppModule/QmitkLevelWindowWidget/QmitkLevelWindowWidget.h</header>
        <sizehint>
            <width>-1</width>
            <height>-1</height>
        </sizehint>
        <container>0</container>
        <sizepolicy>
            <hordata>5</hordata>
            <verdata>5</verdata>
        </sizepolicy>
        <pixmap>
            <data format="XPM.GZ" length="45">789cd3d7528808f055d0d2e72a2e492cc94c5648ce482c52d04a29cdcdad8c8eb5ade6523250004143a55a6b2e0026630c4f</data>
        </pixmap>
        <signal>levelWindow(mitk::LevelWindow*)</signal>
        <slot access="public">slot()</slot>
    </customwidget>
    <customwidget>
        <class>SceneHandler</class>
        <header location="local">../AppModule/SceneHandler/scenehandler.h</header>
        <sizehint>
            <width>55</width>
            <height>55</height>
        </sizehint>
        <container>0</container>
        <sizepolicy>
            <hordata>5</hordata>
            <verdata>5</verdata>
        </sizepolicy>
        <pixmap>
            <data format="XPM.GZ" length="45">789cd3d7528808f055d0d2e72a2e492cc94c5648ce482c52d04a29cdcdad8c8eb5ade6523250004143a55a6b2e0026630c4f</data>
        </pixmap>
    </customwidget>
    <customwidget>
        <class>DataListView</class>
        <header location="local">modelorganizerview.h</header>
        <sizehint>
            <width>-1</width>
            <height>-1</height>
        </sizehint>
        <container>0</container>
        <sizepolicy>
            <hordata>5</hordata>
            <verdata>5</verdata>
        </sizepolicy>
        <pixmap>
            <data format="XPM.GZ" length="45">789cd3d7528808f055d0d2e72a2e492cc94c5648ce482c52d04a29cdcdad8c8eb5ade6523250004143a55a6b2e0026630c4f</data>
        </pixmap>
    </customwidget>
    <customwidget>
        <class>InteractionWidget</class>
        <header location="local">interactionwidget.h</header>
        <sizehint>
            <width>-1</width>
            <height>-1</height>
        </sizehint>
        <container>0</container>
        <sizepolicy>
            <hordata>5</hordata>
            <verdata>5</verdata>
        </sizepolicy>
        <pixmap>
            <data format="XPM.GZ" length="45">789cd3d7528808f055d0d2e72a2e492cc94c5648ce482c52d04a29cdcdad8c8eb5ade6523250004143a55a6b2e0026630c4f</data>
        </pixmap>
    </customwidget>
    <customwidget>
        <class>InteractorControls</class>
        <header location="local">interactorcontrols.h</header>
        <sizehint>
            <width>-1</width>
            <height>-1</height>
        </sizehint>
        <container>1</container>
        <sizepolicy>
            <hordata>5</hordata>
            <verdata>5</verdata>
        </sizepolicy>
        <pixmap>
            <data format="XPM.GZ" length="45">789cd3d7528808f055d0d2e72a2e492cc94c5648ce482c52d04a29cdcdad8c8eb5ade6523250004143a55a6b2e0026630c4f</data>
        </pixmap>
    </customwidget>
    <customwidget>
        <class>ViewOptions</class>
        <header location="local">viewoptions.h</header>
        <sizehint>
            <width>-1</width>
            <height>-1</height>
        </sizehint>
        <container>1</container>
        <sizepolicy>
            <hordata>5</hordata>
            <verdata>5</verdata>
        </sizepolicy>
        <pixmap>
            <data format="XPM.GZ" length="45">789cd3d7528808f055d0d2e72a2e492cc94c5648ce482c52d04a29cdcdad8c8eb5ade6523250004143a55a6b2e0026630c4f</data>
        </pixmap>
    </customwidget>
    <customwidget>
        <class>QmitkSliderNavigator</class>
        <header location="global">QmitkSliderNavigator.h</header>
        <sizehint>
            <width>150</width>
            <height>20</height>
        </sizehint>
        <container>0</container>
        <sizepolicy>
            <hordata>5</hordata>
            <verdata>5</verdata>
        </sizepolicy>
        <pixmap>
            <data format="XPM.GZ" length="45">789cd3d7528808f055d0d2e72a2e492cc94c5648ce482c52d04a29cdcdad8c8eb5ade6523250004143a55a6b2e0026630c4f</data>
        </pixmap>
        <slot access="public">slot()</slot>
        <slot access="public">SetStepper(mitk::Stepper*)</slot>
        <slot access="public">Refetch()</slot>
        <property type="0">4</property>
    </customwidget>
    <customwidget>
        <class>QmitkSimpleExampleControls</class>
        <header location="local">QmitkSimpleExampleControls.h</header>
        <sizehint>
            <width>-1</width>
            <height>-1</height>
        </sizehint>
        <container>0</container>
        <sizepolicy>
            <hordata>5</hordata>
            <verdata>5</verdata>
        </sizepolicy>
        <pixmap>
            <data format="XPM.GZ" length="4430">789cb597496f1d370cc7eff91446780b0a7636cd82a207ef8e1d2fcf8ed7a2076a166fef3def89eda2dfbd1af23f83c20e8af6d02807ffccbf284a22a9f1cf9f164ef6b6173efdfce1e1511e2feb85fa42ee173e354fb3d9cb6fbffffac7878f49b2d0ff8fca85e4e34f1f3e4e1e17ea859d9b79db033f05a048ff294f94937ef44ce72367ca3c72aefa6ae452f96c6451de1bb956de1fb9e9595e94b3613de191d53f7d1f59e7cb2b38b378c25607563b7523ab7f6e95f37ea87e4bb90c6cfeae9525fcc6e28f949ba88bf53ce8b9e7380ac3e2bb022771aa76a79cf443ed5f95d3d8c1ff74648d8776945d9c43ff0c2ec147e01041cf7eae9cc715ceff6160d3530aae2d1e362e07a65c59c2b0fd1f806becef58b98e1bb353619c246697cb912dbe43e53671e65fea9e833187ff2fca4952c44ee3390097c6b4aa5c260de6af1ba711ec7abe89249dd9bd18a7715428cf46b6fbda55f6698cf9dbe014bca45ca749acf9489f95bb60b7fc8cc159acfe48ef2f8dd3ca9813b0c495f2a37291c59171a35cf643e757e0cef4b239b2adbfdc7348cfdafc8be66b9664ade949f33173812d9f09dcc596cfea2fab5d857aca943be7cd2e6b3dbbc855e00d30ecb407aec13a5fd351efdbeb7dbac475967f5c18e711ea55cfcfa5791c6bfff00d18f5ecfdc86aa7b3819358f9169cda7cd6fc73d9a097136597a7c67e3ab0e5a3d77a76799e597cb20876163f693f7345ee503f7abeaeca4bece774609cafe69f93bc82bf66e044f3dd6bff733e17d44b3bb0d9691fec63db9ff60f570f767fa1dce4a847590637d8dfd5c0f01f8f6c7aed2fae1dd79b80bd9d9fccc143bfb81cd8e261bbcfae1faab7fbee823fab6f01b7a6f79a7f795cc4a8ff1d30fa0169bfcdb322b578e4019c25d66f36c039f25deb3f77c16eefcb3218f7c54b60e845cf33cf8bd04174fe05384decbdd1facc8b02f72377ca52e4c6acf99737fd502e95dba2c17a3b23eb7e45fb65919725ce370157b06bff2c8ab248acbed7c0a5ed576864b37f03633eaf8c6cfd70112cf0aff915a60f76bd8f42faa1acfdb4ece576be6a2fabaac67e4e8c25b2fecbcf235b3cfa9e94b50ce7a7ef69d904b6f85ec199e513dd0c6c7a317d27696ae75382b3d4e2d7fba9a2a0b77a3a01e789c5ff02ae10dff6c8f63eea7d55b1a05ff12a5810cfe2c0b81f3d8f2a910af11d837da2fdd3f3c8b6bede4f95f64359f3b5cac4a5567f66cf05fd951a631fd97d8ade5f55f808efc53a38467f3f1dd9ec1b60bc37520d8c78b47f56e5a0976d7092785d5fdfb7aaf219deaf17b0c37baffdbf121fa39f7a30faab68fe567e889fa6e0a15ecfc00ee7affdb8aa035b7c6b60d4231f810bd4b3be7f5513d6b7f8f7c00eefd72b7878efb7c015f87064ebffc6ad2f2dff690e16e47b3db2e9b5beaace4799dd7f0baeecfc24020bde3f7dbf240afead9fdc81c5f29573b0c77d6afe49ec338b4766e0cae6cb048cfa6507f6e8a77a3e92fa06f5b032b2c5a3fd5d42f9a25f6abf16a933e49bbe5fd2d41ef9aedf37d2d635fa83e93bdfe696cffa7ef8c877b97e1fb2f66f1f3ee88c45cfcb274d86fc3e1d18fd43ebd787cf91c2e2df053b67f5780fcec17a9fde0d7a9e821dd6d77ee68bf07963f77103ce61bf0517e0d9c8b6bf3b7009d6f7db97be163d7f367fe5689f832b63d1fee49ba6c5fde9fbe2dbc07a5e93c77f3ffeae676261ff835173c3ed5b3d777cce177cc9577ccd539ef19c6ff8368c3bbee7077ee4a7bfeb83ef73fec6dff9995ff89517798997798557798dd779833ff3e61bbdf056507fe16ddee15ddee34918fb7cc05ff9908fc2ace3377a1f223909ea533e0bea88634e380d3f9d71c68e732edee9af42242557c44424e4a9a6865aeae89c2ee8f207feaff995ae82f59aa634a339ddd02dddd17df0f0408ff4f44e3fa56ff49d9e83ef177aa5455a0aca655aa1d5e0638dd6dfe967b4112c9f6993b6e80b6d07f50eafd02eed85df4e68ff9dfe860e78425fe9908ed437d3319dd0299d057d44f13bfd9c12dea734ec320b6a4779985150191a2b4ba8cef7f188979af7a491563a39970bb9e4895cc9b54c6526f3b77ab9915bb90bfa7b79904779926ff25d9ee5455e65519664f907fe576455d6c2bd9ec9ba6cc867d994ada07e952fb22d3b6ff4b5ecf2baecc944f6e520447e1ef67e2e5f83ef433992633979a36ff8414ee54ca2f06761f8534ac26b28e1134a4aa9424a937fbbdf3664eca6f7bef68d6f7de7cffd85bff457b2ecaffdd4cffc9bfd86194fff3cded7d77fadc7ff45ffe72f1ffe02059d03ad</data>
        </pixmap>
        <slot access="public">slot()</slot>
    </customwidget>
    <customwidget>
        <class>QmitkSliceWidget</class>
        <header location="global">QmitkSliceWidget.h</header>
        <sizehint>
            <width>-1</width>
            <height>-1</height>
        </sizehint>
        <container>0</container>
        <sizepolicy>
            <hordata>5</hordata>
            <verdata>5</verdata>
        </sizepolicy>
        <pixmap>
            <data format="XPM.GZ" length="4430">789cb597496f1d370cc7eff91446780b0a7636cd82a207ef8e1d2fcf8ed7a2076a166fef3def89eda2dfbd1af23f83c20e8af6d02807ffccbf284a22a9f1cf9f164ef6b6173efdfce1e1511e2feb85fa42ee173e354fb3d9cb6fbffffac7878f49b2d0ff8fca85e4e34f1f3e4e1e17ea859d9b79db033f05a048ff294f94937ef44ce72367ca3c72aefa6ae452f96c6451de1bb956de1fb9e9595e94b3613de191d53f7d1f59e7cb2b38b378c25607563b7523ab7f6e95f37ea87e4bb90c6cfeae9525fcc6e28f949ba88bf53ce8b9e7380ac3e2bb022771aa76a79cf443ed5f95d3d8c1ff74648d8776945d9c43ff0c2ec147e01041cf7eae9cc715ceff6160d3530aae2d1e362e07a65c59c2b0fd1f806becef58b98e1bb353619c246697cb912dbe43e53671e65fea9e833187ff2fca4952c44ee3390097c6b4aa5c260de6af1ba711ec7abe89249dd9bd18a7715428cf46b6fbda55f6698cf9dbe014bca45ca749acf9489f95bb60b7fc8cc159acfe48ef2f8dd3ca9813b0c495f2a37291c59171a35cf643e757e0cef4b239b2adbfdc7348cfdafc8be66b9664ade949f33173812d9f09dcc596cfea2fab5d857aca943be7cd2e6b3dbbc855e00d30ecb407aec13a5fd351efdbeb7dbac475967f5c18e711ea55cfcfa5791c6bfff00d18f5ecfdc86aa7b3819358f9169cda7cd6fc73d9a097136597a7c67e3ab0e5a3d77a76799e597cb20876163f693f7345ee503f7abeaeca4bece774609cafe69f93bc82bf66e044f3dd6bff733e17d44b3bb0d9691fec63db9ff60f570f767fa1dce4a847590637d8dfd5c0f01f8f6c7aed2fae1dd79b80bd9d9fccc143bfb81cd8e261bbcfae1faab7fbee823fab6f01b7a6f79a7f795cc4a8ff1d30fa0169bfcdb322b578e4019c25d66f36c039f25deb3f77c16eefcb3218f7c54b60e845cf33cf8bd04174fe05384decbdd1facc8b02f72377ca52e4c6acf99737fd502e95dba2c17a3b23eb7e45fb65919725ce370157b06bff2c8ab248acbed7c0a5ed576864b37f03633eaf8c6cfd70112cf0aff915a60f76bd8f42faa1acfdb4ece576be6a2fabaac67e4e8c25b2fecbcf235b3cfa9e94b50ce7a7ef69d904b6f85ec199e513dd0c6c7a317d27696ae75382b3d4e2d7fba9a2a0b77a3a01e789c5ff02ae10dff6c8f63eea7d55b1a05ff12a5810cfe2c0b81f3d8f2a910af11d837da2fdd3f3c8b6bede4f95f64359f3b5cac4a5567f66cf05fd951a631fd97d8ade5f55f808efc53a38467f3f1dd9ec1b60bc37520d8c78b47f56e5a0976d7092785d5fdfb7aaf219deaf17b0c37baffdbf121fa39f7a30faab68fe567e889fa6e0a15ecfc00ee7affdb8aa035b7c6b60d4231f810bd4b3be7f5513d6b7f8f7c00eefd72b7878efb7c015f87064ebffc6ad2f2dff690e16e47b3db2e9b5beaace4799dd7f0baeecfc24020bde3f7dbf240afead9fdc81c5f29573b0c77d6afe49ec338b4766e0cae6cb048cfa6507f6e8a77a3e92fa06f5b032b2c5a3fd5d42f9a25f6abf16a933e49bbe5fd2d41ef9aedf37d2d635fa83e93bdfe696cffa7ef8c877b97e1fb2f66f1f3ee88c45cfcb274d86fc3e1d18fd43ebd787cf91c2e2df053b67f5780fcec17a9fde0d7a9e821dd6d77ee68bf07963f77103ce61bf0517e0d9c8b6bf3b7009d6f7db97be163d7f367fe5689f832b63d1fee49ba6c5fde9fbe2dbc07a5e93c77f3ffeae676261ff835173c3ed5b3d777cce177cc9577ccd539ef19c6ff8368c3bbee7077ee4a7bfeb83ef73fec6dff9995ff89517798997798557798dd779833ff3e61bbdf056507fe16ddee15ddee34918fb7cc05ff9908fc2ace3377a1f223909ea533e0bea88634e380d3f9d71c68e732edee9af42242557c44424e4a9a6865aeae89c2ee8f207feaff995ae82f59aa634a339ddd02dddd17df0f0408ff4f44e3fa56ff49d9e83ef177aa5455a0aca655aa1d5e0638dd6dfe967b4112c9f6993b6e80b6d07f50eafd02eed85df4e68ff9dfe860e78425fe9908ed437d3319dd0299d057d44f13bfd9c12dea734ec320b6a4779985150191a2b4ba8cef7f188979af7a491563a39970bb9e4895cc9b54c6526f3b77ab9915bb90bfa7b79904779926ff25d9ee5455e65519664f907fe576455d6c2bd9ec9ba6cc867d994ada07e952fb22d3b6ff4b5ecf2baecc944f6e520447e1ef67e2e5f83ef433992633979a36ff8414ee54ca2f06761f8534ac26b28e1134a4aa9424a937fbbdf3664eca6f7bef68d6f7de7cffd85bff457b2ecaffdd4cffc9bfd86194fff3cded7d77fadc7ff45ffe72f1ffe02059d03ad</data>
        </pixmap>
        <signal>sliderChanged(int)</signal>
        <signal>levelWindowChanged(mitk::LevelWindow)</signal>
        <slot access="public">setSliderMin(int)</slot>
        <slot access="public">setSliderMax(int)</slot>
        <slot access="public">setLevelWindow(mitk::LeveWindow)</slot>
        <slot access="public">SetData(mitk::DataTreeIterator)</slot>
        <slot access="public">setData(mitk::DataTreeIterator)</slot>
        <slot access="public">ChangeView(int)</slot>
        <slot access="public">slot()</slot>
        <slot access="public">ChangeLevelWindow(mitk::LevelWindow* lw )</slot>
    </customwidget>
    <customwidget>
        <class>DataTreeListView</class>
        <header location="global">F:/develop/reggi/ip++/AppModule/QmitkDataManager/DataTreeView.h</header>
        <sizehint>
            <width>-1</width>
            <height>-1</height>
        </sizehint>
        <container>0</container>
        <sizepolicy>
            <hordata>5</hordata>
            <verdata>5</verdata>
        </sizepolicy>
        <pixmap>
            <data format="XPM.GZ" length="4430">789cb597496f1d370cc7eff91446780b0a7636cd82a207ef8e1d2fcf8ed7a2076a166fef3def89eda2dfbd1af23f83c20e8af6d02807ffccbf284a22a9f1cf9f164ef6b6173efdfce1e1511e2feb85fa42ee173e354fb3d9cb6fbffffac7878f49b2d0ff8fca85e4e34f1f3e4e1e17ea859d9b79db033f05a048ff294f94937ef44ce72367ca3c72aefa6ae452f96c6451de1bb956de1fb9e9595e94b3613de191d53f7d1f59e7cb2b38b378c25607563b7523ab7f6e95f37ea87e4bb90c6cfeae9525fcc6e28f949ba88bf53ce8b9e7380ac3e2bb022771aa76a79cf443ed5f95d3d8c1ff74648d8776945d9c43ff0c2ec147e01041cf7eae9cc715ceff6160d3530aae2d1e362e07a65c59c2b0fd1f806becef58b98e1bb353619c246697cb912dbe43e53671e65fea9e833187ff2fca4952c44ee3390097c6b4aa5c260de6af1ba711ec7abe89249dd9bd18a7715428cf46b6fbda55f6698cf9dbe014bca45ca749acf9489f95bb60b7fc8cc159acfe48ef2f8dd3ca9813b0c495f2a37291c59171a35cf643e757e0cef4b239b2adbfdc7348cfdafc8be66b9664ade949f33173812d9f09dcc596cfea2fab5d857aca943be7cd2e6b3dbbc855e00d30ecb407aec13a5fd351efdbeb7dbac475967f5c18e711ea55cfcfa5791c6bfff00d18f5ecfdc86aa7b3819358f9169cda7cd6fc73d9a097136597a7c67e3ab0e5a3d77a76799e597cb20876163f693f7345ee503f7abeaeca4bece774609cafe69f93bc82bf66e044f3dd6bff733e17d44b3bb0d9691fec63db9ff60f570f767fa1dce4a847590637d8dfd5c0f01f8f6c7aed2fae1dd79b80bd9d9fccc143bfb81cd8e261bbcfae1faab7fbee823fab6f01b7a6f79a7f795cc4a8ff1d30fa0169bfcdb322b578e4019c25d66f36c039f25deb3f77c16eefcb3218f7c54b60e845cf33cf8bd04174fe05384decbdd1facc8b02f72377ca52e4c6acf99737fd502e95dba2c17a3b23eb7e45fb65919725ce370157b06bff2c8ab248acbed7c0a5ed576864b37f03633eaf8c6cfd70112cf0aff915a60f76bd8f42faa1acfdb4ece576be6a2fabaac67e4e8c25b2fecbcf235b3cfa9e94b50ce7a7ef69d904b6f85ec199e513dd0c6c7a317d27696ae75382b3d4e2d7fba9a2a0b77a3a01e789c5ff02ae10dff6c8f63eea7d55b1a05ff12a5810cfe2c0b81f3d8f2a910af11d837da2fdd3f3c8b6bede4f95f64359f3b5cac4a5567f66cf05fd951a631fd97d8ade5f55f808efc53a38467f3f1dd9ec1b60bc37520d8c78b47f56e5a0976d7092785d5fdfb7aaf219deaf17b0c37baffdbf121fa39f7a30faab68fe567e889fa6e0a15ecfc00ee7affdb8aa035b7c6b60d4231f810bd4b3be7f5513d6b7f8f7c00eefd72b7878efb7c015f87064ebffc6ad2f2dff690e16e47b3db2e9b5beaace4799dd7f0baeecfc24020bde3f7dbf240afead9fdc81c5f29573b0c77d6afe49ec338b4766e0cae6cb048cfa6507f6e8a77a3e92fa06f5b032b2c5a3fd5d42f9a25f6abf16a933e49bbe5fd2d41ef9aedf37d2d635fa83e93bdfe696cffa7ef8c877b97e1fb2f66f1f3ee88c45cfcb274d86fc3e1d18fd43ebd787cf91c2e2df053b67f5780fcec17a9fde0d7a9e821dd6d77ee68bf07963f77103ce61bf0517e0d9c8b6bf3b7009d6f7db97be163d7f367fe5689f832b63d1fee49ba6c5fde9fbe2dbc07a5e93c77f3ffeae676261ff835173c3ed5b3d777cce177cc9577ccd539ef19c6ff8368c3bbee7077ee4a7bfeb83ef73fec6dff9995ff89517798997798557798dd779833ff3e61bbdf056507fe16ddee15ddee34918fb7cc05ff9908fc2ace3377a1f223909ea533e0bea88634e380d3f9d71c68e732edee9af42242557c44424e4a9a6865aeae89c2ee8f207feaff995ae82f59aa634a339ddd02dddd17df0f0408ff4f44e3fa56ff49d9e83ef177aa5455a0aca655aa1d5e0638dd6dfe967b4112c9f6993b6e80b6d07f50eafd02eed85df4e68ff9dfe860e78425fe9908ed437d3319dd0299d057d44f13bfd9c12dea734ec320b6a4779985150191a2b4ba8cef7f188979af7a491563a39970bb9e4895cc9b54c6526f3b77ab9915bb90bfa7b79904779926ff25d9ee5455e65519664f907fe576455d6c2bd9ec9ba6cc867d994ada07e952fb22d3b6ff4b5ecf2baecc944f6e520447e1ef67e2e5f83ef433992633979a36ff8414ee54ca2f06761f8534ac26b28e1134a4aa9424a937fbbdf3664eca6f7bef68d6f7de7cffd85bff457b2ecaffdd4cffc9bfd86194fff3cded7d77fadc7ff45ffe72f1ffe02059d03ad</data>
        </pixmap>
    </customwidget>
    <customwidget>
        <class>QmitkNodePropertiesView</class>
        <header location="local">QmitkNodePropertiesView.h</header>
        <sizehint>
            <width>-1</width>
            <height>-1</height>
        </sizehint>
        <container>0</container>
        <sizepolicy>
            <hordata>5</hordata>
            <verdata>5</verdata>
        </sizepolicy>
        <pixmap>
            <data format="XPM.GZ" length="2910">789cb595db522a571086ef7d0acabeb352bd9903c34ca57201088a0a02a21b4de5624e829c541814d895774f4fafeea91da452c9455ceaf055ffddfdf75a6bf4db5969d4eb94cebe9dacb3307b894bf1245c95ce92cd62b1fbfd8fdf7e9c9cda7689be2dcb29d9a7bf9c9cf6b3525ceabe2ed31c70450065fe626e17ec33a7c2aec4ad82390e46ef17bc15f625ff5638b6381fa29cad72be9877c29a3f5696f8bdb06ffa032a4bfc463896fc576589bf31fbaac781b0c6df0ddbeae75a58fb6f72b6cbca7825acf55ce6c0d67a7dc38eec270c0b36fb71a92cfaa46013ef09bbc25ccfb11c5ff8ddb0abf538df091cdf09b8de5a3816fd43ceaee52a3799c34a20f977ca260eecaf62173c104e647f5f94a55ec7b06749fc4e59f6c72fd8c417cab6993f14ae487e53d9c431660e2b4925e4f8b3614ffcc35459fa750b36fecacad26f2facf32c95e53cf87c3dbbaafe6d65f1b313ae48fdbdb2c4af85a51ff27df7a8be67fcd70c17fa0be144fcf1fb5275fc8acde7895b61f59f156ce2bc7fd5a81a54238e7f1a563d7ecfd98f02a98f2dc3a1ceb729d8cc93294bbf7365c7e2f8a7b0ce5f5516fd4a59f49073e00656e0727c6638d4f9de94cdfdc50fe150fcd50a36e7152b4bbf4ec1e6fef78453f1d7301cd9c24fcad29fe70be2223e5296fa50b0d1af955d335f45d813bff5824d3d4f58e7792cd8c46f94a5df47c1661ebe1fa11b79727ef7c29a3f54967850b0c94f8553f1cfef6f1847a9c7ff0ca06b389679d111f624bf2eacfe26c2a9e8f9fd8b5cad8733c3b12d3c51ae3acc63659fef2bbe0a7be6fdc6b9b2e42f8543f1db309cc8fec25c59fcf0df872889d290ef1b2e0c17f92dc389f8ef67ff7efdac47c010a3232bc604d3433d3ee31827f882539ce19cd68256fe5ce22bbee13bae7ed653ed31ae31c30d7ee0276e71877bfa5dc33a36689d63f3401f52ed0c5ba4b8c04b6ce38a561baff01a6f28a78e9d037d444e727597546dbcc51ef671c09fef7088f747f4537272c1951ff03b8ef0119fb08c16b18dce11fd8c7cbbac1e61053daca28f0169db8000107ed1cf69be3644549bd4104342bd1e20e51a5b783ea2dfe18a54a35c9dcf0c63d24de085f45d987ed12f68075734e523665c7b850398c11c16f4a90ccb23fadccf2b3e91827cc11b757b8715ace9991dd1cfb1061baa5b860ff2fd49b5e94c296f0b3b1cc2fe88be0e35d25bb88714ead0807340f29242135a47f50db8804b3ed74b7afab08636a6a4dfd07e5e1de863ba550d3af721edf8806f032dd851ed6bb881ce97f34ae80ee6fa47e802d0e4659ae3967cb768fe0ef4a07fa04fe9c636b10303fa09e119a6b0e4b5872bb88321dcff5d4f19ab7f5e5fdfaffffa3efe2ffa3f7f3df90bdf8dabe4</data>
        </pixmap>
        <slot access="public">SelectionChanged(QListViewItem* NewItem)</slot>
        <slot access="public">SetRenderWindow(const QString &amp;name)</slot>
    </customwidget>
    <customwidget>
        <class>SegToolControls</class>
        <header location="global">F:/develop/MITKExample/ip++/AppModules/QmitkSegTools/SegToolControls.ui.h</header>
        <sizehint>
            <width>80</width>
            <height>240</height>
        </sizehint>
        <container>0</container>
        <sizepolicy>
            <hordata>5</hordata>
            <verdata>5</verdata>
        </sizepolicy>
        <pixmap>
            <data format="XPM.GZ" length="4430">789cb597496f1d370cc7eff91446780b0a7636cd82a207ef8e1d2fcf8ed7a2076a166fef3def89eda2dfbd1af23f83c20e8af6d02807ffccbf284a22a9f1cf9f164ef6b6173efdfce1e1511e2feb85fa42ee173e354fb3d9cb6fbffffac7878f49b2d0ff8fca85e4e34f1f3e4e1e17ea859d9b79db033f05a048ff294f94937ef44ce72367ca3c72aefa6ae452f96c6451de1bb956de1fb9e9595e94b3613de191d53f7d1f59e7cb2b38b378c25607563b7523ab7f6e95f37ea87e4bb90c6cfeae9525fcc6e28f949ba88bf53ce8b9e7380ac3e2bb022771aa76a79cf443ed5f95d3d8c1ff74648d8776945d9c43ff0c2ec147e01041cf7eae9cc715ceff6160d3530aae2d1e362e07a65c59c2b0fd1f806becef58b98e1bb353619c246697cb912dbe43e53671e65fea9e833187ff2fca4952c44ee3390097c6b4aa5c260de6af1ba711ec7abe89249dd9bd18a7715428cf46b6fbda55f6698cf9dbe014bca45ca749acf9489f95bb60b7fc8cc159acfe48ef2f8dd3ca9813b0c495f2a37291c59171a35cf643e757e0cef4b239b2adbfdc7348cfdafc8be66b9664ade949f33173812d9f09dcc596cfea2fab5d857aca943be7cd2e6b3dbbc855e00d30ecb407aec13a5fd351efdbeb7dbac475967f5c18e711ea55cfcfa5791c6bfff00d18f5ecfdc86aa7b3819358f9169cda7cd6fc73d9a097136597a7c67e3ab0e5a3d77a76799e597cb20876163f693f7345ee503f7abeaeca4bece774609cafe69f93bc82bf66e044f3dd6bff733e17d44b3bb0d9691fec63db9ff60f570f767fa1dce4a847590637d8dfd5c0f01f8f6c7aed2fae1dd79b80bd9d9fccc143bfb81cd8e261bbcfae1faab7fbee823fab6f01b7a6f79a7f795cc4a8ff1d30fa0169bfcdb322b578e4019c25d66f36c039f25deb3f77c16eefcb3218f7c54b60e845cf33cf8bd04174fe05384decbdd1facc8b02f72377ca52e4c6acf99737fd502e95dba2c17a3b23eb7e45fb65919725ce370157b06bff2c8ab248acbed7c0a5ed576864b37f03633eaf8c6cfd70112cf0aff915a60f76bd8f42faa1acfdb4ece576be6a2fabaac67e4e8c25b2fecbcf235b3cfa9e94b50ce7a7ef69d904b6f85ec199e513dd0c6c7a317d27696ae75382b3d4e2d7fba9a2a0b77a3a01e789c5ff02ae10dff6c8f63eea7d55b1a05ff12a5810cfe2c0b81f3d8f2a910af11d837da2fdd3f3c8b6bede4f95f64359f3b5cac4a5567f66cf05fd951a631fd97d8ade5f55f808efc53a38467f3f1dd9ec1b60bc37520d8c78b47f56e5a0976d7092785d5fdfb7aaf219deaf17b0c37baffdbf121fa39f7a30faab68fe567e889fa6e0a15ecfc00ee7affdb8aa035b7c6b60d4231f810bd4b3be7f5513d6b7f8f7c00eefd72b7878efb7c015f87064ebffc6ad2f2dff690e16e47b3db2e9b5beaace4799dd7f0baeecfc24020bde3f7dbf240afead9fdc81c5f29573b0c77d6afe49ec338b4766e0cae6cb048cfa6507f6e8a77a3e92fa06f5b032b2c5a3fd5d42f9a25f6abf16a933e49bbe5fd2d41ef9aedf37d2d635fa83e93bdfe696cffa7ef8c877b97e1fb2f66f1f3ee88c45cfcb274d86fc3e1d18fd43ebd787cf91c2e2df053b67f5780fcec17a9fde0d7a9e821dd6d77ee68bf07963f77103ce61bf0517e0d9c8b6bf3b7009d6f7db97be163d7f367fe5689f832b63d1fee49ba6c5fde9fbe2dbc07a5e93c77f3ffeae676261ff835173c3ed5b3d777cce177cc9577ccd539ef19c6ff8368c3bbee7077ee4a7bfeb83ef73fec6dff9995ff89517798997798557798dd779833ff3e61bbdf056507fe16ddee15ddee34918fb7cc05ff9908fc2ace3377a1f223909ea533e0bea88634e380d3f9d71c68e732edee9af42242557c44424e4a9a6865aeae89c2ee8f207feaff995ae82f59aa634a339ddd02dddd17df0f0408ff4f44e3fa56ff49d9e83ef177aa5455a0aca655aa1d5e0638dd6dfe967b4112c9f6993b6e80b6d07f50eafd02eed85df4e68ff9dfe860e78425fe9908ed437d3319dd0299d057d44f13bfd9c12dea734ec320b6a4779985150191a2b4ba8cef7f188979af7a491563a39970bb9e4895cc9b54c6526f3b77ab9915bb90bfa7b79904779926ff25d9ee5455e65519664f907fe576455d6c2bd9ec9ba6cc867d994ada07e952fb22d3b6ff4b5ecf2baecc944f6e520447e1ef67e2e5f83ef433992633979a36ff8414ee54ca2f06761f8534ac26b28e1134a4aa9424a937fbbdf3664eca6f7bef68d6f7de7cffd85bff457b2ecaffdd4cffc9bfd86194fff3cded7d77fadc7ff45ffe72f1ffe02059d03ad</data>
        </pixmap>
        <signal>FreehandEnabled()</signal>
        <signal>RegionGrowingEnabled()</signal>
        <signal>CorrectionEnabled()</signal>
    </customwidget>
    <customwidget>
        <class>QmitkTwoButtonNavigator</class>
        <header location="local">QmitkTwoButtonNavigator.h</header>
        <sizehint>
            <width>150</width>
            <height>150</height>
        </sizehint>
        <container>0</container>
        <sizepolicy>
            <hordata>5</hordata>
            <verdata>5</verdata>
        </sizepolicy>
        <pixmap>
            <data format="XPM.GZ" length="4430">789cb597496f1d370cc7eff91446780b0a7636cd82a207ef8e1d2fcf8ed7a2076a166fef3def89eda2dfbd1af23f83c20e8af6d02807ffccbf284a22a9f1cf9f164ef6b6173efdfce1e1511e2feb85fa42ee173e354fb3d9cb6fbffffac7878f49b2d0ff8fca85e4e34f1f3e4e1e17ea859d9b79db033f05a048ff294f94937ef44ce72367ca3c72aefa6ae452f96c6451de1bb956de1fb9e9595e94b3613de191d53f7d1f59e7cb2b38b378c25607563b7523ab7f6e95f37ea87e4bb90c6cfeae9525fcc6e28f949ba88bf53ce8b9e7380ac3e2bb022771aa76a79cf443ed5f95d3d8c1ff74648d8776945d9c43ff0c2ec147e01041cf7eae9cc715ceff6160d3530aae2d1e362e07a65c59c2b0fd1f806becef58b98e1bb353619c246697cb912dbe43e53671e65fea9e833187ff2fca4952c44ee3390097c6b4aa5c260de6af1ba711ec7abe89249dd9bd18a7715428cf46b6fbda55f6698cf9dbe014bca45ca749acf9489f95bb60b7fc8cc159acfe48ef2f8dd3ca9813b0c495f2a37291c59171a35cf643e757e0cef4b239b2adbfdc7348cfdafc8be66b9664ade949f33173812d9f09dcc596cfea2fab5d857aca943be7cd2e6b3dbbc855e00d30ecb407aec13a5fd351efdbeb7dbac475967f5c18e711ea55cfcfa5791c6bfff00d18f5ecfdc86aa7b3819358f9169cda7cd6fc73d9a097136597a7c67e3ab0e5a3d77a76799e597cb20876163f693f7345ee503f7abeaeca4bece774609cafe69f93bc82bf66e044f3dd6bff733e17d44b3bb0d9691fec63db9ff60f570f767fa1dce4a847590637d8dfd5c0f01f8f6c7aed2fae1dd79b80bd9d9fccc143bfb81cd8e261bbcfae1faab7fbee823fab6f01b7a6f79a7f795cc4a8ff1d30fa0169bfcdb322b578e4019c25d66f36c039f25deb3f77c16eefcb3218f7c54b60e845cf33cf8bd04174fe05384decbdd1facc8b02f72377ca52e4c6acf99737fd502e95dba2c17a3b23eb7e45fb65919725ce370157b06bff2c8ab248acbed7c0a5ed576864b37f03633eaf8c6cfd70112cf0aff915a60f76bd8f42faa1acfdb4ece576be6a2fabaac67e4e8c25b2fecbcf235b3cfa9e94b50ce7a7ef69d904b6f85ec199e513dd0c6c7a317d27696ae75382b3d4e2d7fba9a2a0b77a3a01e789c5ff02ae10dff6c8f63eea7d55b1a05ff12a5810cfe2c0b81f3d8f2a910af11d837da2fdd3f3c8b6bede4f95f64359f3b5cac4a5567f66cf05fd951a631fd97d8ade5f55f808efc53a38467f3f1dd9ec1b60bc37520d8c78b47f56e5a0976d7092785d5fdfb7aaf219deaf17b0c37baffdbf121fa39f7a30faab68fe567e889fa6e0a15ecfc00ee7affdb8aa035b7c6b60d4231f810bd4b3be7f5513d6b7f8f7c00eefd72b7878efb7c015f87064ebffc6ad2f2dff690e16e47b3db2e9b5beaace4799dd7f0baeecfc24020bde3f7dbf240afead9fdc81c5f29573b0c77d6afe49ec338b4766e0cae6cb048cfa6507f6e8a77a3e92fa06f5b032b2c5a3fd5d42f9a25f6abf16a933e49bbe5fd2d41ef9aedf37d2d635fa83e93bdfe696cffa7ef8c877b97e1fb2f66f1f3ee88c45cfcb274d86fc3e1d18fd43ebd787cf91c2e2df053b67f5780fcec17a9fde0d7a9e821dd6d77ee68bf07963f77103ce61bf0517e0d9c8b6bf3b7009d6f7db97be163d7f367fe5689f832b63d1fee49ba6c5fde9fbe2dbc07a5e93c77f3ffeae676261ff835173c3ed5b3d777cce177cc9577ccd539ef19c6ff8368c3bbee7077ee4a7bfeb83ef73fec6dff9995ff89517798997798557798dd779833ff3e61bbdf056507fe16ddee15ddee34918fb7cc05ff9908fc2ace3377a1f223909ea533e0bea88634e380d3f9d71c68e732edee9af42242557c44424e4a9a6865aeae89c2ee8f207feaff995ae82f59aa634a339ddd02dddd17df0f0408ff4f44e3fa56ff49d9e83ef177aa5455a0aca655aa1d5e0638dd6dfe967b4112c9f6993b6e80b6d07f50eafd02eed85df4e68ff9dfe860e78425fe9908ed437d3319dd0299d057d44f13bfd9c12dea734ec320b6a4779985150191a2b4ba8cef7f188979af7a491563a39970bb9e4895cc9b54c6526f3b77ab9915bb90bfa7b79904779926ff25d9ee5455e65519664f907fe576455d6c2bd9ec9ba6cc867d994ada07e952fb22d3b6ff4b5ecf2baecc944f6e520447e1ef67e2e5f83ef433992633979a36ff8414ee54ca2f06761f8534ac26b28e1134a4aa9424a937fbbdf3664eca6f7bef68d6f7de7cffd85bff457b2ecaffdd4cffc9bfd86194fff3cded7d77fadc7ff45ffe72f1ffe02059d03ad</data>
        </pixmap>
    </customwidget>
    <customwidget>
        <class>QmitkPrimitiveMovieNavigator</class>
        <header location="local">QmitkPrimitiveMovieNavigator.h</header>
        <sizehint>
            <width>150</width>
            <height>20</height>
        </sizehint>
        <container>0</container>
        <sizepolicy>
            <hordata>5</hordata>
            <verdata>5</verdata>
        </sizepolicy>
        <pixmap>
            <data format="XPM.GZ" length="4830">789c9597c972e3480e40eff5158ec2ada203cd3549c6c41cbcc9b6bccabbddd1078014bdcb9b6449ee987f9f240072caac9e4357d6c1cf5812890490f4ef3f562e8ff6577efcfeed7d4ad3bb72a5bca5b7951fd5ece969f9c79ffffeebdbf7285a69fec7f14af4fdb76fdf47d39572e5e079326e00c10304f24ff8b9c7d463ee71d9e3aac7e31ed70de741b3842f7bf2418f275f19fabcdee351cffea227ef9d079e7abcd6e3a3afccfb3dff370d53d42c61f5e79a25bcd9b3dfebd9df365c24cd127e6c38ce9b257cdeb35ff4e2ebdf5f221c354be4cb8ec53fbc762cf1c14bc7b21f541d93f88b3b2e85d38eab86e94c3869f7a359c7eaffa463b1a773e344e3a1838e450e8b8ed5bf13ce7d3ed57e4b989a02127dadaf2aa843cdc769c361e097c633308ec25858ceeb295239cb79c3384ccdff76c7b23f14c269e8ccdfa9716ef6b5b18f407857d88585e57bbf65b39f18971a0fc8fd7b6fc624f713925f923f2e8d4b3d1f49bd79aa54ee5b5d388a4cbed9b1c627f909c7516afea51ebcd0997d261c455998cafd3ae35c1902e13caacc3e528e03936f085354ab9ce7ca711864c2c38eb59ed59ee3d0ec73e3d858e64154c65128f50752cf51ede5cab571128a3f92fb8bc3b850861b630a0bd94fe28bb3240c84696afd55ab9cd28ed5ff55c3befc4af32ff3208992b1ea83e437493d6bbdbe19d7a1d40b4a3f24655a583ddf09d729ab9cc286d3202d8c63e3560ec6a5b1cc63293fedff4fe128adb5dee051d905d62f92af34766128f39d578dad5f79d9b1c8e9bee528147f43e358ed018593569fa55ed3d4c5c63b2d6bfdb1e42b752ed1f8e8c238d5f8e1503873a9f58bccefb470b99da76cd9f22bf592922bccdf67cb91d6b7dc6fca8eacffe72dab9cd89843391f3d0897ad9ce53ed3cab5fd77655cd9f9b65ab6fe7aec58eb5de6493aeef62363d6fcd1d0b89d0f8396351ed478ea66895cefbbf6fe74bfb9f158f559e6930bb3d0e2298cadff41de179764b1c573689c443a6f6263a7f54e926f977ab9be1f07c6765fb86f6cfa2cf9742ef31343fc7d1ac791be77324f5c96d9fd90da53e69451eacd55cd127b39af1b6795ed1776ace7957999b93c377f4fc685c9655e66599e45daffa171aee7a58f8e557e6c6cf678d8b1cebf3d635239497d7af3562efd9c51b384af1bce1b75cdefbb70519456efac4c81ce5bdce958e78b7e0f95d4e64fee37af3c6b7cbbc689f5e34ecbaa4ff2bee635c5b1e6e7dd3889357ee98f22f0fafabedd1abb48e31f1a17165fd0b1eacb7b5c8464f30a8f8cc9e2b96859ef07e4fe8b880a8d8f6f8c3992f9c9b38e757fc96f91501a6bbfc97d158e6c9e8231077a7f24f92e320eb41ef1d838d4f9cc771dabfcc4d8de139eb66cfbcb7b5ee4ad3ee5c651c4b2bff47f517062efd39971aaef2bc8fb511087363fa7c6364f59eab5e0367e18185b7ff2bd716af996fa2b4acf1adfc8d8fa0f0be3ccde2be9d7a2f2fbebf9c038b5f7eadcb87dcf9d71a14ce38ef5ef013dff9873ad77d83626abef59c7aaaff9a93948b41e578d0bcd1f3f1893de27cb7b4981f7aff363d798b43ee1c198ed7d967aa490138d87768c0bb56732b67e857b63b6f929f54a3157e6ffba638d47e639f976b5f928f1139589d59be49faa926dfec87b48e3b2b47920fd46358f9dd8a3dc3f075c3bfd9e977cb1ff605326a9578eaac4def7bb966dfe497ed87f7e64fa9ea2719a6affad193b6379af386df5f1c338b5fd653e72e63f67e43e68d7d899fcd338339e77ace75b35ce8de5bde39c4b92fce352b9932f8c0b6596fc70558dedfee4fb8ec79e255fa369b3109090b1c40ac7bd55f9dfb297c268daea638d37788b7778ff37eb011ff10927deea59f5bdef1b7cc1577cc3779ce20c3f708e0b5ce2a75fabb886ebb8819b38c0d2f4c9fb7ec52ddcc61d1ce22eeee13e1ee0211ee1088ff1044ff10ccff1022f4d9f7d24575efb1a030c31c21813bf527498618e058e0001807062faa58ff21d180328a1c218c650c30ddcfa9f2ab8837b788047afff04ad7ee5f5a738846778815778837798c20c3e600e0b58c227ac8aff355837fdb1d79fe12e6c78e9260c600bb6610786b00b7bdec33e1cc0a1d73f82d14ffa1f700c2770ea7d9fc1395cc0a5d7bc826b08bc8f10a25ff4e7107b49022938c820f7da051e1212c09288f817fd259598504563aac5f72bddd02dddd1bdd77fa0c75ff417f484294de083fca9e9855ebdc51bbdd39466f441f35fe3a1052d31a64f5aa5355aa70ddaf4fb0d688bb66987867d7ddaa53ddaf7fa0774484734a2633aa1533aa373baa04bbafa1bffd71450880954fe2ffd98fc7b45ce6b9ffbcf809c8a2ffa153e30e23103133397fece96de6ec9155df0986bbee1db2ff755fa8a5de73bbee7077ee4279ef033bff02bbff13b4f79c61f3ce7c54ff5c0bebe37f08c97fcc9abbcc6ebbcc19b3ce02dbae26ddee121eff2de4ff5e67ff2f57d0effe71f2f788ff77faa67f0110df002c8efb9e6cff575adf9df92d7eefac55b3c7b8b4b9cc004d661d45beb30f1fb5ffeaf1fff41bfffe75fdffe0b4c602e16</data>
        </pixmap>
    </customwidget>
</customwidgets>
</CW>
