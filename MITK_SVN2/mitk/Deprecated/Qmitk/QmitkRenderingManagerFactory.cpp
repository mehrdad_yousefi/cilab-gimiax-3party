/*=========================================================================

Program:   Medical Imaging & Interaction Toolkit
Language:  C++
Date:      $Date: 2008-08-01 14:25:23 +0200 (vie, 01 ago 2008) $
Version:   $Revision: 14882 $

Copyright (c) German Cancer Research Center, Division of Medical and
Biological Informatics. All rights reserved.
See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notices for more information.

=========================================================================*/


#include "QmitkRenderingManagerFactory.h"
#include "QmitkRenderingManager.h"

QmitkRenderingManagerFactory
::QmitkRenderingManagerFactory()
{
  mitk::RenderingManager::SetFactory( this );
}

QmitkRenderingManagerFactory
::~QmitkRenderingManagerFactory()
{
}

mitk::RenderingManager::Pointer
QmitkRenderingManagerFactory
::CreateRenderingManager() const
{
  QmitkRenderingManager::Pointer specificSmartPtr = QmitkRenderingManager::New();
  mitk::RenderingManager::Pointer smartPtr = specificSmartPtr.GetPointer();
  return smartPtr;
}

