/*=========================================================================
 
Program:   Medical Imaging & Interaction Toolkit
Module:    $RCSfile: mitkPropertyManager.cpp,v $
Language:  C++
Date:      $Date: 2008-11-11 14:17:21 +0100 (mar, 11 nov 2008) $
Version:   $Revision: 1.12 $
 
Copyright (c) German Cancer Research Center, Division of Medical and
Biological Informatics. All rights reserved.
See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.
 
This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notices for more information.
 
=========================================================================*/

#include "QmitkDrawPaintbrushToolGUI.h"

QmitkDrawPaintbrushToolGUI::QmitkDrawPaintbrushToolGUI()
{
}

QmitkDrawPaintbrushToolGUI::~QmitkDrawPaintbrushToolGUI()
{
}

