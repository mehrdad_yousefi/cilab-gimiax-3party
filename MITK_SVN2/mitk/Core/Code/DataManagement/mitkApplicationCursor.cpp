/*=========================================================================
 
Program:   Medical Imaging & Interaction Toolkit
Language:  C++
Date:      $Date: 2009-06-18 15:59:04 +0200 (jue, 18 jun 2009) $
Version:   $Revision: 17786 $
 
Copyright (c) German Cancer Research Center, Division of Medical and
Biological Informatics. All rights reserved.
See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.
 
This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notices for more information.
 
=========================================================================*/

#include "mitkApplicationCursor.h"

#include <iostream>

mitk::ApplicationCursor* mitk::ApplicationCursor::m_Instance = NULL;
std::map<vtkRenderWindow*, mitk::ApplicationCursorImplementation*> mitk::ApplicationCursor::m_Win2Instance;

namespace mitk {

ApplicationCursor::ApplicationCursor()
{
}

ApplicationCursor* ApplicationCursor::GetInstance()
{
  if (!m_Instance)
  {
    m_Instance = new ApplicationCursor();
  }

  return m_Instance;
}
    
void ApplicationCursor::RegisterImplementation(
	vtkRenderWindow *window, 
	ApplicationCursorImplementation* implementation)
{
  m_Win2Instance[ window ] = implementation;
}

void ApplicationCursor::PushCursor( vtkRenderWindow *window, const char* XPM[], int hotspotX, int hotspotY)
{
  ApplicationCursorImplementation* instance = m_Win2Instance[ window ];
  if ( instance )
  {
    instance->PushCursor(XPM, hotspotX, hotspotY);
  }
  else
  {
    LOG_ERROR << "in mitk::ApplicationCursor::PushCursor(): no implementation registered." << std::endl;
  }
}

void ApplicationCursor::PopCursor( vtkRenderWindow *window )
{
  ApplicationCursorImplementation* instance = m_Win2Instance[ window ];
  if (instance)
  {
    instance->PopCursor();
  }
  else
  {
    LOG_ERROR << "in mitk::ApplicationCursor::PopCursor(): no implementation registered." << std::endl;
  }
}

void ApplicationCursor::UnRegisterImplementation( vtkRenderWindow *window )
{
  m_Win2Instance.erase( window );
}

} // namespace

