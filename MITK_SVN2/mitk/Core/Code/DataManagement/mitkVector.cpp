/*=========================================================================

Program:   Medical Imaging & Interaction Toolkit
Language:  C++
Date:      $Date: 2009-05-12 19:31:16 +0200 (mar, 12 may 2009) $
Version:   $Revision: 17175 $

Copyright (c) German Cancer Research Center, Division of Medical and
Biological Informatics. All rights reserved.
See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notices for more information.

=========================================================================*/

#include "mitkVector.h"
#include <vtkSystemIncludes.h>
#include <vtkMatrix4x4.h>

const mitk::ScalarType mitk::eps     = vnl_math::float_eps*0.001;
const mitk::ScalarType mitk::sqrteps = vnl_math::float_sqrteps*0.001;
extern const double mitk::large      = VTK_LARGE_FLOAT;
