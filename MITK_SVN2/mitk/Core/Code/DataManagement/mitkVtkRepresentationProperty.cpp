/*=========================================================================
 
Program:   Medical Imaging & Interaction Toolkit
Language:  C++
Date:      $Date: 2009-05-12 19:31:16 +0200 (mar, 12 may 2009) $
Version:   $Revision: 17175 $
 
Copyright (c) German Cancer Research Center, Division of Medical and
Biological Informatics. All rights reserved.
See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.
 
This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notices for more information.
 
=========================================================================*/

#include <vtkProperty.h>
#include "mitkVtkRepresentationProperty.h"


mitk::VtkRepresentationProperty::VtkRepresentationProperty( )
{
  AddRepresentationTypes();
  SetValue( SURFACE );
}


mitk::VtkRepresentationProperty::VtkRepresentationProperty( const IdType& value )
{
  AddRepresentationTypes();
  if ( IsValidEnumerationValue( value ) )
  {
    SetValue( value );
  }
  else
  {
   SetValue( SURFACE );
  }
}


mitk::VtkRepresentationProperty::VtkRepresentationProperty( const std::string& value )
{
  AddRepresentationTypes();
  if ( IsValidEnumerationValue( value ) )
  {
    SetValue( value );
  }
  else
  {
    SetValue( SURFACE );
  }
}


int mitk::VtkRepresentationProperty::GetVtkRepresentation()
{
  return static_cast<int>( GetValueAsId() );
}


void mitk::VtkRepresentationProperty::SetRepresentationToPoints()
{
  SetValue( POINTS );
}


void mitk::VtkRepresentationProperty::SetRepresentationToWireframe()
{
  SetValue( WIREFRAME );
}


void mitk::VtkRepresentationProperty::SetRepresentationToSurface()
{
  SetValue( SURFACE );
}


void mitk::VtkRepresentationProperty::SetRepresentationToWireframeSurface()
{
	SetValue( WIRESURF );
}

void mitk::VtkRepresentationProperty::AddRepresentationTypes()
{
  AddEnum( "Points", POINTS );
  AddEnum( "Wireframe",WIREFRAME  );
  AddEnum( "Surface", SURFACE );
  AddEnum( "WireframeSurface", WIRESURF );
}


bool mitk::VtkRepresentationProperty::AddEnum( const std::string& name, const IdType& id )
{
  return Superclass::AddEnum( name, id );  
}
