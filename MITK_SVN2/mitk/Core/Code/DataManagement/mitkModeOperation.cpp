/*=========================================================================

Program:   Medical Imaging & Interaction Toolkit
Language:  C++
Date:      $Date: 2009-05-12 21:46:32 +0200 (mar, 12 may 2009) $
Version:   $Revision: 17194 $

Copyright (c) German Cancer Research Center, Division of Medical and
Biological Informatics. All rights reserved.
See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notices for more information.

=========================================================================*/


#include "mitkModeOperation.h"
#include <mitkOperation.h>

mitk::ModeOperation::ModeOperation(mitk::OperationType operationType, mitk::ModeOperation::ModeType mode)
: Operation(operationType), m_Mode(mode)
{
}

mitk::ModeOperation::~ModeOperation()
{
}

mitk::ModeOperation::ModeType mitk::ModeOperation::GetMode()
{
  return m_Mode;
}
