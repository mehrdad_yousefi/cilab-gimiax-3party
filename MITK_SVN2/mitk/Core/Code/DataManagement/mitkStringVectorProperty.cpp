/*=========================================================================
 
Program:   Medical Imaging & Interaction Toolkit
Language:  C++
Date:      $Date: 2007-12-11 14:46:19 +0100 (mar, 11 dic 2007) $
Version:   $Revision: 13129 $
 
Copyright (c) German Cancer Research Center, Division of Medical and
Biological Informatics. All rights reserved.
See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.
 
This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notices for more information.
 
=========================================================================*/

#include "mitkStringVectorProperty.h"


mitk::StringVectorProperty::StringVectorProperty()
{
	SetNumberOfTimeSteps( 1 );
}

std::vector<std::string> &mitk::StringVectorProperty::GetStringVector( int iTimeStep )
{
	if ( iTimeStep < m_StringVector.size() )
	{
		return m_StringVector[ iTimeStep ];
	}

	return m_EmptyVector;
}

bool mitk::StringVectorProperty::operator==( const BaseProperty& property ) const
{
	return false;
}

void mitk::StringVectorProperty::SetNumberOfTimeSteps( int iNumTimeSteps )
{
	m_StringVector.resize( iNumTimeSteps );
}
