/*=========================================================================

Program:   Medical Imaging & Interaction Toolkit
Language:  C++
Date:      $Date: 2008-02-08 13:23:19 +0100 (vie, 08 feb 2008) $
Version:   $Revision: 13561 $

Copyright (c) German Cancer Research Center, Division of Medical and
Biological Informatics. All rights reserved.
See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notices for more information.

=========================================================================*/

#ifndef _MITK_STRING_VECTOR_PROPERTY__H_
#define _MITK_STRING_VECTOR_PROPERTY__H_

#include "mitkBaseProperty.h"
#include <vector>
#include <string>

namespace mitk {

/**
 * @brief Property for vector of strings
 * @ingroup DataManagement
 */
class MITK_CORE_EXPORT  StringVectorProperty : public mitk::BaseProperty
{
public:

  mitkClassMacro( StringVectorProperty, mitk::BaseProperty );

  itkNewMacro(StringVectorProperty);

  //! Return the string vector reference
  std::vector<std::string> &GetStringVector( int iTimeStep = 0 );

  //! Resize vector
  void SetNumberOfTimeSteps( int iNumTimeSteps );

  //! Always return false
  virtual bool operator==(const BaseProperty& property) const;

private:
  /**
   * Default constructor. 
   */
  StringVectorProperty();

private:

  //! String vector for each time step
  std::vector< std::vector<std::string> > m_StringVector;

  //! When the time step is not set, return this vector
  std::vector<std::string> m_EmptyVector;
};

}

#endif


