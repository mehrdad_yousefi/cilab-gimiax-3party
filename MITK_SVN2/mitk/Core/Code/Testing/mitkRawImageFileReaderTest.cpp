/*=========================================================================

Program:   Medical Imaging & Interaction Toolkit
Language:  C++
Date:      $Date: 2009-05-13 14:52:01 +0200 (mié, 13 may 2009) $
Version:   $Revision: 17230 $

Copyright (c) German Cancer Research Center, Division of Medical and
Biological Informatics. All rights reserved.
See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notices for more information.

=========================================================================*/


#include "mitkRawImageFileReader.h"

int mitkRawImageFileReaderTest(int /*argc*/, char* /*argv*/[])
{
  // instantiation
  mitk::RawImageFileReader::Pointer reader = mitk::RawImageFileReader::New();

  // freeing
  reader = NULL;

  std::cout<<"[TEST DONE]"<<std::endl;
  return EXIT_SUCCESS;
}
