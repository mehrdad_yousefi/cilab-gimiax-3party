/*=========================================================================

Program:   Medical Imaging & Interaction Toolkit
Language:  C++
Date:      $Date: 2010-01-14 14:20:26 +0100 (Thu, 14 Jan 2010) $
Version:   $Revision: 21047 $

Copyright (c) German Cancer Research Center, Division of Medical and
Biological Informatics. All rights reserved.
See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notices for more information.

=========================================================================*/


#include "mitkDisplayVectorInteractorLevelWindow.h"
#include "mitkOperation.h"
#include "mitkDisplayCoordinateOperation.h"
#include "mitkStateEvent.h"
#include "mitkInteractionConst.h"
#include "mitkAction.h"
#include "mitkStandaloneDataStorage.h"

#include "mitkNodePredicateDataType.h"
#include "mitkLevelWindowProperty.h"
#include "mitkLevelWindow.h"
#include "mitkGlobalInteraction.h"
#include "mitkSlicesRotatorMovement.h"


void mitk::DisplayVectorInteractorLevelWindow::ExecuteOperation(Operation* itkNotUsed( operation ) )
{
}

bool mitk::DisplayVectorInteractorLevelWindow::ExecuteAction(Action* action, mitk::StateEvent const* stateEvent)
{
  bool ok=false;
  
  const DisplayPositionEvent* posEvent=dynamic_cast<const DisplayPositionEvent*>(stateEvent->GetEvent());
  if(posEvent==NULL) return false;

  int actionId = action->GetActionId();
  
  switch(actionId)
  {
  case AcINITMOVE:
    // MITK_INFO << "AcINITMOVE";
    {
      m_Sender=posEvent->GetSender();
      m_StartDisplayCoordinate=posEvent->GetDisplayPosition();
      m_LastDisplayCoordinate=posEvent->GetDisplayPosition();
      m_CurrentDisplayCoordinate=posEvent->GetDisplayPosition();
      ok = true;
      break;
    }
  case AcLEVELWINDOW:
    {
      // If SlicesRotatorMovement is active and the user is moving it return false
      GlobalInteraction* globalInteraction = GlobalInteraction::GetInstance();
	  GlobalInteraction::StateMachineList listeners = globalInteraction->GetListenerList( );
	  GlobalInteraction::StateMachineListIter iter;
	  for (iter = listeners.begin(); iter != listeners.end(); iter++)
	  {
		  if((*iter).IsNotNull())
		  {
			  SlicesRotatorMovement *sliceRotator;
			  sliceRotator = dynamic_cast<SlicesRotatorMovement *> ( (*iter).GetPointer() );
			  if ( sliceRotator && 
				   sliceRotator->GetVtkLastRenderWindow( ) )
			  {
				  ok = false;
				  return ok;
			  }
		  }
	  }

	  // Only for 2D renderers
	  if ( m_Sender->GetMapperID() == BaseRenderer::Standard3D )
	  {
		  return false;
	  }

      m_LastDisplayCoordinate=m_CurrentDisplayCoordinate;
      m_CurrentDisplayCoordinate=posEvent->GetDisplayPosition();
  
      mitk::DataStorage::Pointer storage =  m_Sender->GetDataStorage();
      mitk::DataTreeNode::Pointer node = NULL;
	  if ( storage.IsNull() )
	  {
		  return false;
	  }

      mitk::DataStorage::SetOfObjects::ConstPointer allImageNodes = storage->GetSubset(mitk::NodePredicateDataType::New( "Image") );

      for ( unsigned int i = 0; i < allImageNodes->size() ; i++ )
      {
        bool isActiveImage = false;
        bool propFound = allImageNodes->at( i )->GetBoolProperty( "activeImage", isActiveImage );

        if ( propFound && isActiveImage )
        { 
          node = allImageNodes->at( i );
          continue;
        }
      }
      if ( node.IsNull() )
      {
        node = storage->GetNode( mitk::NodePredicateDataType::New( "Image" ) );
      }

      if ( node.IsNull() )
      {
        return false;
      }


      mitk::LevelWindow lv = mitk::LevelWindow();
      node->GetLevelWindow(lv);
      float level = lv.GetLevel();
      float window = lv.GetWindow();
      float rangeMax = static_cast<float>(lv.GetRangeMax());
      float rangeMin = static_cast<float>(lv.GetRangeMin());
      float range = rangeMax - rangeMin;
      int *size = m_Sender->GetRenderWindow()->GetSize();
      float factorX = 1;
      if ( size[ 0 ] )
            factorX = range / size[ 0 ];
      float factorY = 1;
      if ( size[ 1 ] )
            factorY = range / size[ 1 ];

      level += ( m_CurrentDisplayCoordinate[0] - m_LastDisplayCoordinate[0] )*factorX;
      window += ( m_CurrentDisplayCoordinate[1] - m_LastDisplayCoordinate[1] )*factorY;

	  // Center the level in the middle of the window
	  level = std::min( rangeMax - (window / 2), level );
	  level = std::max( rangeMin + (window / 2), level );

      lv.SetLevelWindow( level, window );
      dynamic_cast<mitk::LevelWindowProperty*>(node->GetProperty("levelwindow"))->SetLevelWindow( lv );

      ok = true;
      break;
    }
  case AcFINISHMOVE:
    // MITK_INFO << "AcFINISHMOVE";
    {
      ok = true;
      break;
    }
  default:
    ok = false;
    break;
  }
  return ok;
}

mitk::DisplayVectorInteractorLevelWindow::DisplayVectorInteractorLevelWindow(const char * type)
  : mitk::StateMachine(type), m_Sender(NULL)
{
  m_StartDisplayCoordinate.Fill(0);
  m_LastDisplayCoordinate.Fill(0);
  m_CurrentDisplayCoordinate.Fill(0);
  m_UndoEnabled = false;

}


mitk::DisplayVectorInteractorLevelWindow::~DisplayVectorInteractorLevelWindow()
{
}

