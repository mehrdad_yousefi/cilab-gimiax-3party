/*=========================================================================

Program:   Medical Imaging & Interaction Toolkit
Language:  C++
Date:      $Date: 2009-06-05 14:38:32 +0200 (vie, 05 jun 2009) $
Version:   $Revision: 17630 $

Copyright (c) German Cancer Research Center, Division of Medical and
Biological Informatics. All rights reserved.
See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notices for more information.

=========================================================================*/


#ifndef MITKIMAGECAST_H_HEADER_INCLUDED
#define MITKIMAGECAST_H_HEADER_INCLUDED

#include "itkDiffusionTensor3D.h"
#include "itkVector.h"

#include <mitkImageAccessByItk.h>

#endif // of MITKIMAGECAST_H_HEADER_INCLUDED
