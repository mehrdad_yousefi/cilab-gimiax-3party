/*=========================================================================

Program:   Medical Imaging & Interaction Toolkit
Language:  C++
Date:      $Date: 2009-11-12 12:52:48 +0100 (jue, 12 nov 2009) $
Version:   $Revision: 20071 $

Copyright (c) German Cancer Research Center, Division of Medical and
Biological Informatics. All rights reserved.
See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notices for more information.

=========================================================================*/

#include "mitkConfig.h"
#include "mitkCoreObjectFactory.h"

#include "mitkAffineInteractor.h"
#include "mitkColorProperty.h"
#include "mitkDataTreeNode.h"
#include "mitkEnumerationProperty.h"
#include "mitkGeometry2DData.h"
#include "mitkGeometry2DDataMapper2D.h"
#include "mitkGeometry2DDataVtkMapper3D.h"
#include "mitkGeometry3D.h"
#include "mitkGeometryData.h"
#include "mitkImage.h"
#include "mitkImageMapper2D.h"
#include "mitkLevelWindowProperty.h"
#include "mitkLookupTable.h"
#include "mitkLookupTableProperty.h"
#include "mitkMaterialProperty.h"
#include "mitkPlaneGeometry.h"
#include "mitkPointSet.h"
#include "mitkPointSetMapper2D.h"
#include "mitkPointSetVtkMapper3D.h"
#include "mitkPolyDataGLMapper2D.h"
#include "mitkProperties.h"
#include "mitkPropertyList.h"
#include "mitkSlicedGeometry3D.h"
#include "mitkSmartPointerProperty.h"
#include "mitkStringProperty.h"
#include "mitkSurface.h"
#include "mitkSurface.h"
#include "mitkSurfaceMapper2D.h"
#include "mitkSurfaceVtkMapper3D.h"
#include "mitkTimeSlicedGeometry.h"
#include "mitkTransferFunctionProperty.h"
#include "mitkVolumeDataVtkMapper3D.h"
#include "mitkVtkInterpolationProperty.h"
#include "mitkVtkRepresentationProperty.h"
#include "mitkVtkResliceInterpolationProperty.h"

#include "mitkPicFileIOFactory.h"
#include "mitkPointSetIOFactory.h"
#include "mitkItkImageFileIOFactory.h"
#include "mitkSTLFileIOFactory.h"
#include "mitkVtkSurfaceIOFactory.h"
#include "mitkVtkImageIOFactory.h"
#include "mitkVtiFileIOFactory.h"
#include "mitkPicVolumeTimeSeriesIOFactory.h"

#include "mitkImageWriterFactory.h"
#include "mitkPointSetWriterFactory.h"
#include "mitkSurfaceVtkWriterFactory.h"

#define CREATE_CPP( TYPE, NAME ) else if ( className == NAME ) {pointer = new TYPE(); pointer->Register();}
#define CREATE_ITK( TYPE, NAME ) else if ( className == NAME ) pointer = TYPE::New();

mitk::CoreObjectFactory::FileWriterList mitk::CoreObjectFactory::m_FileWriters;
bool mitk::CoreObjectFactory::alreadyDone = false;

itk::Object::Pointer mitk::CoreObjectFactory::CreateCoreObject( const std::string& className )
{
  itk::Object::Pointer pointer;

  if ( className == "" )
    return NULL;

    CREATE_ITK( BoolProperty, "BoolProperty" )
    CREATE_ITK( IntProperty, "IntProperty" )
    CREATE_ITK( FloatProperty, "FloatProperty" )
    CREATE_ITK( DoubleProperty, "DoubleProperty" )
    CREATE_ITK( Vector3DProperty, "Vector3DProperty" )
    CREATE_ITK( Point3dProperty, "Point3dProperty" )
    CREATE_ITK( Point4dProperty, "Point4dProperty" )
    CREATE_ITK( Point3iProperty, "Point3iProperty" )
    CREATE_ITK( BoolProperty, "BoolProperty" )
    CREATE_ITK( ColorProperty, "ColorProperty" )
    CREATE_ITK( LevelWindowProperty, "LevelWindowProperty" )
    CREATE_ITK( LookupTableProperty, "LookupTableProperty" )
    CREATE_ITK( MaterialProperty, "MaterialProperty" )
    CREATE_ITK( StringProperty, "StringProperty" )
    CREATE_ITK( SmartPointerProperty, "SmartPointerProperty" )
    CREATE_ITK( TransferFunctionProperty, "TransferFunctionProperty" )
    CREATE_ITK( EnumerationProperty, "EnumerationProperty" )
    CREATE_ITK( VtkInterpolationProperty, "VtkInterpolationProperty" )
    CREATE_ITK( VtkRepresentationProperty, "VtkRepresentationProperty" )
    CREATE_ITK( VtkResliceInterpolationProperty, "VtkResliceInterpolationProperty" )
    CREATE_ITK( GeometryData, "GeometryData" )
    CREATE_ITK( Surface, "Surface" )
    CREATE_ITK( Image, "Image" )
    CREATE_ITK( Geometry3D, "Geometry3D" )
    CREATE_ITK( TimeSlicedGeometry, "TimeSlicedGeometry" )
    CREATE_ITK( Surface, "Surface" )
    CREATE_ITK( PointSet, "PointSet" )
    CREATE_ITK( SlicedGeometry3D, "SlicedGeometry3D" )
    CREATE_ITK( PlaneGeometry, "PlaneGeometry" )
    CREATE_ITK( PropertyList, "PropertyList" )
    CREATE_ITK( SurfaceMapper2D, "SurfaceMapper2D" )
    CREATE_ITK( SurfaceVtkMapper3D, "SurfaceVtkMapper3D" )
    CREATE_ITK( ImageMapper2D, "ImageMapper2D" )
    CREATE_ITK( VolumeDataVtkMapper3D, "VolumeDataVtkMapper3D" )
    CREATE_ITK( LookupTable, "LookupTable" )
    CREATE_ITK( PointSetMapper2D, "PointSetMapper2D" )
    CREATE_ITK( PointSetVtkMapper3D, "PointSetVtkMapper3D" )

  else
    LOG_ERROR << "ObjectFactory::CreateObject: unknown class: " << className << std::endl;

  return pointer;
}
void mitk::CoreObjectFactory::RegisterExtraFactory(CoreObjectFactoryBase::Pointer factory) {
  LOG_INFO << "Registering extra factory: " << factory->GetNameOfClass();
  m_ExtraFactories.push_back(factory);  
}

mitk::CoreObjectFactory::Pointer mitk::CoreObjectFactory::GetInstance() {
  static mitk::CoreObjectFactory::Pointer instance;
  if (instance.IsNull()) {
    std::list<itk::LightObject::Pointer> allobjects = itk::ObjectFactoryBase::CreateAllInstance("mitkCoreObjectFactoryBase");
    std::list<itk::LightObject::Pointer>::iterator factoryIt = allobjects.begin();
    while (instance.IsNull() && factoryIt != allobjects.end() ) {
      if (std::string("SBCoreObjectFactory") == (*factoryIt)->GetNameOfClass() ) {
        instance = dynamic_cast<mitk::CoreObjectFactory*>(factoryIt->GetPointer());
      }
      ++factoryIt;
    }
    factoryIt = allobjects.begin();
    while (instance.IsNull() && factoryIt != allobjects.end() ) {
      if ( std::string("QMCoreObjectFactory") == (*factoryIt)->GetNameOfClass() ) {
        instance = dynamic_cast<mitk::CoreObjectFactory*>(factoryIt->GetPointer());
      }
      ++factoryIt;
    }
    factoryIt = allobjects.begin();
    while (instance.IsNull() && factoryIt != allobjects.end() ) {
      if (std::string("CoreExtObjectFactory") == (*factoryIt)->GetNameOfClass() ) {
        instance = dynamic_cast<mitk::CoreObjectFactory*>(factoryIt->GetPointer());
      }
      ++factoryIt;
    }
    if (instance.IsNull()) {
      instance = mitk::CoreObjectFactory::New();
    }
    LOG_INFO << "CoreObjectFactory: created instance of " << instance->GetNameOfClass() << std::endl;
  }
  return instance;
}

#include <mitkDataTreeNodeFactory.h>

void mitk::CoreObjectFactory::SetDefaultProperties(mitk::DataTreeNode* node)
{
  if(node==NULL)
    return;

  mitk::DataTreeNode::Pointer nodePointer = node;

  mitk::Image::Pointer image = dynamic_cast<mitk::Image*>(node->GetData());
  if(image.IsNotNull() && image->IsInitialized())
  {
    mitk::ImageMapper2D::SetDefaultProperties(node);
    mitk::VolumeDataVtkMapper3D::SetDefaultProperties(node);
  }

  mitk::Surface::Pointer surface = dynamic_cast<mitk::Surface*>(node->GetData());
  if(surface.IsNotNull())
  {
    mitk::SurfaceMapper2D::SetDefaultProperties(node);
    mitk::SurfaceVtkMapper3D::SetDefaultProperties(node);
  }

  mitk::PointSet::Pointer pointSet = dynamic_cast<mitk::PointSet*>(node->GetData());
  if(pointSet.IsNotNull())
  {
    mitk::PointSetMapper2D::SetDefaultProperties(node);
    mitk::PointSetVtkMapper3D::SetDefaultProperties(node);
  }
  for (ExtraFactoriesList::iterator it = m_ExtraFactories.begin(); it != m_ExtraFactories.end() ; it++ ) {
    (*it)->SetDefaultProperties(node);
  }
}

mitk::CoreObjectFactory::CoreObjectFactory()
{
  if (!alreadyDone)
  {
    LOG_INFO << "CoreObjectFactory c'tor" << std::endl;

	PicFileIOFactory::Pointer picFileIOFactory = PicFileIOFactory::New();
	m_Factories.push_back( picFileIOFactory.GetPointer( ) );
	PointSetIOFactory::Pointer pointSetIOFactory = PointSetIOFactory::New( );
	m_Factories.push_back( pointSetIOFactory.GetPointer( ) );
	STLFileIOFactory::Pointer sTLFileIOFactory = STLFileIOFactory::New( );
	m_Factories.push_back( sTLFileIOFactory.GetPointer( ) );
	VtkSurfaceIOFactory::Pointer vtkSurfaceIOFactory = VtkSurfaceIOFactory::New();
	m_Factories.push_back( vtkSurfaceIOFactory.GetPointer( ) );
	VtkImageIOFactory::Pointer vtkImageIOFactory = VtkImageIOFactory::New();
	m_Factories.push_back( vtkImageIOFactory.GetPointer() );
	VtiFileIOFactory::Pointer vtiFileIOFactory = VtiFileIOFactory::New();
	m_Factories.push_back( vtiFileIOFactory.GetPointer( ) );
	ItkImageFileIOFactory::Pointer itkImageFileIOFactory = ItkImageFileIOFactory::New();
	m_Factories.push_back( itkImageFileIOFactory.GetPointer( ) );
	PicVolumeTimeSeriesIOFactory::Pointer picVolumeTimeSeriesIOFactory = PicVolumeTimeSeriesIOFactory::New();
	m_Factories.push_back( picVolumeTimeSeriesIOFactory.GetPointer( ) );

	FactoriesList::iterator it;
	for ( it = m_Factories.begin() ; it != m_Factories.end() ; it++ )
	{
		itk::ObjectFactoryBase::RegisterFactory( *it );
	}

    mitk::SurfaceVtkWriterFactory::RegisterOneFactory();
    mitk::PointSetWriterFactory::RegisterOneFactory();
    mitk::ImageWriterFactory::RegisterOneFactory();

    alreadyDone = true;
  }
}

mitk::CoreObjectFactory::~CoreObjectFactory()
{
	if (alreadyDone)
	{
		FactoriesList::iterator it;
		for ( it = m_Factories.begin() ; it != m_Factories.end() ; it++ )
		{
			itk::ObjectFactoryBase::UnRegisterFactory( *it );
		}
		m_Factories.clear();

		mitk::SurfaceVtkWriterFactory::UnRegisterOneFactory();
		mitk::PointSetWriterFactory::UnRegisterOneFactory();
		mitk::ImageWriterFactory::UnRegisterOneFactory();
	}

}

mitk::Mapper::Pointer mitk::CoreObjectFactory::CreateMapper(mitk::DataTreeNode* node, MapperSlotId id)
{
  mitk::Mapper::Pointer newMapper = NULL;
  
  // check wether extra factories provide mapper
  for (ExtraFactoriesList::iterator it = m_ExtraFactories.begin(); it != m_ExtraFactories.end() ; it++ ) 
  {
    // if mapper was already created in previous iteration, and
    // no mapper is created in current iteration -> keep previous mapper
	mitk::Mapper::Pointer tempMapper = (*it)->CreateMapper(node,id);
	if ( tempMapper.IsNotNull( ) )
	{
      newMapper = tempMapper;
	}
  }

  if (newMapper.IsNull()) 
  {

    mitk::BaseData *data = node->GetData();

    if ( id == mitk::BaseRenderer::Standard2D )
    {
      if((dynamic_cast<Image*>(data)!=NULL))
      {
        mitk::Image::Pointer image = dynamic_cast<mitk::Image*>(data);
        newMapper = mitk::ImageMapper2D::New();
        newMapper->SetDataTreeNode(node);
      }
      else if((dynamic_cast<Geometry2DData*>(data)!=NULL))
      {
        newMapper = mitk::Geometry2DDataMapper2D::New();
        newMapper->SetDataTreeNode(node);
      }
      else if((dynamic_cast<Surface*>(data)!=NULL))
      {
        newMapper = mitk::SurfaceMapper2D::New();
        // cast because SetDataTreeNode is not virtual
        mitk::SurfaceMapper2D *castedMapper = dynamic_cast<mitk::SurfaceMapper2D*>(newMapper.GetPointer());
        castedMapper->SetDataTreeNode(node);
      }
      else if((dynamic_cast<PointSet*>(data)!=NULL))
      {
        newMapper = mitk::PointSetMapper2D::New();
        newMapper->SetDataTreeNode(node);
      }
    }
    else if ( id == mitk::BaseRenderer::Standard3D )
    {
      if((dynamic_cast<Image*>(data) != NULL))
      {
        newMapper = mitk::VolumeDataVtkMapper3D::New();
        newMapper->SetDataTreeNode(node);
      }
      else if((dynamic_cast<Geometry2DData*>(data)!=NULL))
      {
        newMapper = mitk::Geometry2DDataVtkMapper3D::New();
        newMapper->SetDataTreeNode(node);
      }
      else if((dynamic_cast<Surface*>(data)!=NULL))
      {
        newMapper = mitk::SurfaceVtkMapper3D::New();
        newMapper->SetDataTreeNode(node);
      }
      else if((dynamic_cast<PointSet*>(data)!=NULL))
      {
        newMapper = mitk::PointSetVtkMapper3D::New();
        //newMapper = mitk::EnhancedPointSetVtkMapper3D::New(); // <-- use this if you want to try the new work in progres point set mapper
        newMapper->SetDataTreeNode(node);
      }
    }
  }

  return newMapper;
}

#define EXTERNAL_FILE_EXTENSIONS \
    "All known formats(*.dcm *.DCM *.dc3 *.DC3 *.gdcm *.ima *.mhd *.mps *.nii *.pic *.pic.gz *.bmp *.png *.jpg *.tiff *.pvtk *.stl *.vtk *.vtp *.vtu *.obj *.vti *.hdr *.nrrd *.nhdr );;" \
    "DICOM files(*.dcm *.DCM *.dc3 *.DC3 *.gdcm);;" \
    "DKFZ Pic (*.seq *.pic *.pic.gz *.seq.gz);;" \
    "NRRD Vector Images (*.nrrd *.nhdr);;" \
    "Point sets (*.mps);;" \
    "Sets of 2D slices (*.pic *.pic.gz *.bmp *.png *.dcm *.gdcm *.ima *.tiff);;" \
    "Surface files (*.stl *.vtk *.vtp *.obj);;" \
    "NIfTI format (*.nii)"

#define INTERNAL_FILE_EXTENSIONS \
    "all (*.seq *.mps *.nii *.pic *.pic.gz *.seq.gz *.pvtk *.stl *.vtk *.vtp *.vtu *.obj *.vti *.ves *.nrrd *.nhdr " \
         "*.uvg *.dvg *.par *.dcm *.dc3 *.gdcm *.ima *.mhd *.hdr hpsonos.db HPSONOS.DB *.ssm *msm *.bmp *.png *.jpg *.tiff *.mitk);;" \
    "Project files(*.mitk);;" \
    "DKFZ Pic (*.seq *.pic *.pic.gz *.seq.gz);;" \
    "NRRD Vector Images (*.nrrd *.nhdr);;" \
    "Point sets (*.mps);;" \
    "surface files (*.stl *.vtk *.vtp *.obj);;" \
    "stl files (*.stl);;" \
    "vtk surface files (*.vtk);;" \
    "vtk image files (*.pvtk);;" \
    "vessel files (*.ves *.uvg *.dvg);;" \
    "par/rec files (*.par);;" \
    "DSR files (hpsonos.db HPSONOS.DB);;" \
    "DICOM files (*.dcm *.gdcm *.dc3 *.ima);;" \
    "NIfTI format (*.nii)"

#define SAVE_FILE_EXTENSIONS "all (*.pic *.mhd *.vtk *.vti *.hdr *.png *.tiff *.jpg *.hdr *.bmp *.dcm *.gipl *.nii *.nrrd *.nhdr *.spr *.lsm *.dwi *.hdwi *.qbi *.hqbi)"

const char* mitk::CoreObjectFactory::GetFileExtensions()
{
  std::string fileExtensions(EXTERNAL_FILE_EXTENSIONS);

  for (ExtraFactoriesList::iterator it = m_ExtraFactories.begin(); it != m_ExtraFactories.end() ; it++ ) {
    fileExtensions.append(";;").append((*it)->GetFileExtensions());
  }
  return fileExtensions.c_str();
};

const char* mitk::CoreObjectFactory::GetSaveFileExtensions() {
  std::string fileExtensions(SAVE_FILE_EXTENSIONS);

  for (ExtraFactoriesList::iterator it = m_ExtraFactories.begin(); it != m_ExtraFactories.end() ; it++ ) {
    fileExtensions.append(";;").append((*it)->GetFileExtensions());
  }
  return fileExtensions.c_str();

};

mitk::CoreObjectFactory::FileWriterList mitk::CoreObjectFactory::GetFileWriters() {
  FileWriterList allWriters = m_FileWriters;
  for (ExtraFactoriesList::iterator it = m_ExtraFactories.begin(); it != m_ExtraFactories.end() ; it++ ) {
    FileWriterList list2 = (*it)->GetFileWriters();
    allWriters.merge(list2);
  }
  return allWriters;
}
void mitk::CoreObjectFactory::MapEvent(const mitk::Event*, const int) {

}

void mitk::CoreObjectFactory::UnRegisterExtraFactory( CoreObjectFactoryBase::Pointer factory )
{
	for ( ExtraFactoriesList::iterator i = 
		m_ExtraFactories.begin();
		i != m_ExtraFactories.end(); ++i )
	{
		if ( factory == *i )
		{
			m_ExtraFactories.remove(factory);
			return;
		}
	}

}

