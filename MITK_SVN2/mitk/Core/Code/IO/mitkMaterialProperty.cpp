/*=========================================================================
 
Program:   Medical Imaging & Interaction Toolkit
Language:  C++
Date:      $Date: 2009-06-22 11:46:21 +0200 (lun, 22 jun 2009) $
Version:   $Revision: 17830 $
 
Copyright (c) German Cancer Research Center, Division of Medical and
Biological Informatics. All rights reserved.
See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.
 
This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notices for more information.
 
=========================================================================*/

#include "mitkMaterialProperty.h"
#include "mitkProperties.h"
#include "mitkVtkInterpolationProperty.h"
#include "mitkVtkRepresentationProperty.h"
#include <vtkProperty.h>
#include "mitkDataTreeNode.h"
#include "mitkBaseRenderer.h"


mitk::MaterialProperty::MaterialProperty( Color color, vtkFloatingPointType opacity, mitk::DataTreeNode* node, mitk::BaseRenderer* renderer )
{
  InitializeStandardValues();
  SetDataTreeNode( node );
  SetRenderer( renderer );
  SetColor( color );
  SetAmbientCoefficient( GetAmbientCoefficient() );
  SetDiffuseCoefficient( GetDiffuseCoefficient() );
  SetSpecularColor( GetSpecularColor() );
  SetSpecularCoefficient( GetSpecularCoefficient() );
  SetSpecularPower( GetSpecularPower() );
  SetOpacity( opacity );
  SetInterpolation( GetInterpolation() );
  SetRepresentation( GetRepresentation() );
  SetLineWidth( GetLineWidth() );
  SetPointSize( GetPointSize() );
  m_Name = "";
}



mitk::MaterialProperty::MaterialProperty( vtkFloatingPointType red, vtkFloatingPointType green, vtkFloatingPointType blue, vtkFloatingPointType opacity, mitk::DataTreeNode* node, mitk::BaseRenderer* renderer )
{
  InitializeStandardValues();
  SetDataTreeNode( node );
  SetRenderer( renderer );
  SetColor( red, green, blue );
  SetAmbientCoefficient( GetAmbientCoefficient() );
  SetDiffuseCoefficient( GetDiffuseCoefficient() );
  SetSpecularColor( GetSpecularColor() );
  SetSpecularCoefficient( GetSpecularCoefficient() );
  SetSpecularPower( GetSpecularPower() );
  SetOpacity( opacity );
  SetInterpolation( GetInterpolation() );
  SetRepresentation( GetRepresentation() );
  SetLineWidth( GetLineWidth() );
  SetPointSize( GetPointSize() );
  m_Name = "";
}



mitk::MaterialProperty::MaterialProperty( vtkFloatingPointType red, vtkFloatingPointType green, vtkFloatingPointType blue,
    vtkFloatingPointType colorCoefficient, vtkFloatingPointType specularCoefficient,
    vtkFloatingPointType specularPower, vtkFloatingPointType opacity, mitk::DataTreeNode* node, mitk::BaseRenderer* renderer )
{
  InitializeStandardValues();
  SetDataTreeNode( node );
  SetRenderer( renderer );
  SetColor( red, green, blue );
  SetAmbientCoefficient( colorCoefficient );
  SetDiffuseCoefficient( colorCoefficient );
  SetSpecularColor( GetSpecularColor() );
  SetSpecularCoefficient( specularCoefficient );
  SetSpecularPower( specularPower );
  SetOpacity( opacity );
  SetInterpolation( GetInterpolation() );
  SetRepresentation( GetRepresentation() );
  SetLineWidth( GetLineWidth() );
  SetPointSize( GetPointSize() );
  m_Name = "";
}


mitk::MaterialProperty::MaterialProperty( mitk::MaterialProperty::Color color, vtkFloatingPointType colorCoefficient, vtkFloatingPointType specularCoefficient, vtkFloatingPointType specularPower, vtkFloatingPointType opacity, mitk::DataTreeNode* node, mitk::BaseRenderer* renderer )
{
  InitializeStandardValues();
  SetDataTreeNode( node );
  SetRenderer( renderer );
  SetColor( color );
  SetAmbientCoefficient( colorCoefficient );
  SetDiffuseCoefficient( colorCoefficient );
  SetSpecularColor( GetSpecularColor() );
  SetSpecularCoefficient( specularCoefficient );
  SetSpecularPower( specularPower );
  SetOpacity( opacity );
  SetInterpolation( GetInterpolation() );
  SetRepresentation( GetRepresentation() );
  SetLineWidth( GetLineWidth() );
  SetPointSize( GetPointSize() );
}


mitk::MaterialProperty::MaterialProperty( mitk::DataTreeNode* node, mitk::BaseRenderer* renderer )
{
  InitializeStandardValues();
  SetDataTreeNode( node );
  SetRenderer( renderer );
  SetColor( GetColor() );
  SetAmbientCoefficient( GetAmbientCoefficient() );
  SetDiffuseCoefficient( GetDiffuseCoefficient() );
  SetSpecularColor( GetSpecularColor() );
  SetSpecularCoefficient( GetSpecularCoefficient() );
  SetSpecularPower( GetSpecularPower() );
  SetOpacity( GetOpacity() );
  SetInterpolation( GetInterpolation() );
  SetRepresentation( GetRepresentation() );
  SetLineWidth( GetLineWidth() );
  SetPointSize( GetPointSize() );
}

mitk::MaterialProperty::MaterialProperty( const MaterialProperty& property ) : mitk::BaseProperty()
{
  Initialize( property );
}

mitk::MaterialProperty::MaterialProperty( const MaterialProperty& property, vtkFloatingPointType red, vtkFloatingPointType green, vtkFloatingPointType blue, vtkFloatingPointType opacity, std::string name )
{
  Initialize( property );
  SetColor( red, green, blue );
  SetOpacity( opacity );
  SetName( name );
}

bool mitk::MaterialProperty::Assignable(const BaseProperty& other) const
{
  try
  {
    dynamic_cast<const Self&>(other); // dear compiler, please don't optimize this away! Thanks.
    return true;
  }
  catch (std::bad_cast)
  {
  }
  return false;
}

mitk::BaseProperty& mitk::MaterialProperty::operator=(const mitk::BaseProperty& other)
{
  try
  {
    const Self& otherProp( dynamic_cast<const Self&>(other) );

    Initialize(otherProp, false);
  }
  catch (std::bad_cast)
  {
    // nothing to do then
  }

  return *this;
}



mitk::DataTreeNode* mitk::MaterialProperty::GetDataTreeNode() const
{
  return m_DataTreeNode;
}



mitk::BaseRenderer* mitk::MaterialProperty::GetRenderer( ) const
{
  return m_Renderer;
}

void mitk::MaterialProperty::SetColor( mitk::MaterialProperty::Color color )
{
  if ( ForwardToDataTreeNode() )
  {
    m_DataTreeNode->SetColor( color.GetRed(), color.GetGreen(), color.GetBlue(), m_Renderer );
  }
  m_Color = color;
  Modified();
}


void mitk::MaterialProperty::SetColor( vtkFloatingPointType red, vtkFloatingPointType green, vtkFloatingPointType blue )
{
  if ( ForwardToDataTreeNode() )
  {
    m_DataTreeNode->SetColor( red, green, blue, m_Renderer );
  }
  m_Color.Set( red, green, blue );
  Modified();
}


void mitk::MaterialProperty::SetAmbientColor( mitk::MaterialProperty::Color color )
{
  if ( ForwardToDataTreeNode() )
  {
    m_DataTreeNode->SetColor( color.GetRed(), color.GetGreen(), color.GetBlue(), m_Renderer, AMBIENT_COLOR_KEY );
  }
  m_AmbientColor = color;
  Modified();
}

void mitk::MaterialProperty::SetDiffuseColor( mitk::MaterialProperty::Color color )
{
  if ( ForwardToDataTreeNode() )
  {
    m_DataTreeNode->SetColor( color.GetRed(), color.GetGreen(), color.GetBlue(), m_Renderer, DIFFUSE_COLOR_KEY );
  }
  m_DiffuseColor = color;
  Modified();
}


void mitk::MaterialProperty::SetAmbientColor( vtkFloatingPointType red, vtkFloatingPointType green, vtkFloatingPointType blue )
{
  if ( ForwardToDataTreeNode() )
  {
  m_DataTreeNode->SetColor( red, green, blue, m_Renderer, AMBIENT_COLOR_KEY );
  }
  m_AmbientColor.Set( red, green, blue );
  Modified();
}



void mitk::MaterialProperty::SetDiffuseColor( vtkFloatingPointType red, vtkFloatingPointType green, vtkFloatingPointType blue )
{
  if ( ForwardToDataTreeNode() )
  {
    m_DataTreeNode->SetColor( red, green, blue, m_Renderer, DIFFUSE_COLOR_KEY );
  }
  m_DiffuseColor.Set( red, green, blue );
  Modified();
}

void mitk::MaterialProperty::SetAmbientCoefficient( vtkFloatingPointType coefficient )
{
  if ( ForwardToDataTreeNode() )
  {
    m_DataTreeNode->SetProperty( AMBIENT_COEFFICIENT_KEY, mitk::FloatProperty::New( coefficient ), m_Renderer );
  }
  m_AmbientCoefficient = coefficient;
  Modified();
}


void mitk::MaterialProperty::SetDiffuseCoefficient( vtkFloatingPointType coefficient )
{
  if ( ForwardToDataTreeNode() )
  {
    m_DataTreeNode->SetProperty( DIFFUSE_COEFFICIENT_KEY, mitk::FloatProperty::New( coefficient ), m_Renderer );
  }
  m_DiffuseCoefficient = coefficient;
  Modified();
}
void mitk::MaterialProperty::SetSpecularColor( mitk::MaterialProperty::Color specularColor )
{
  if ( ForwardToDataTreeNode() )
  {
    m_DataTreeNode->SetProperty( SPECULAR_COLOR_KEY, mitk::ColorProperty::New( specularColor.GetRed(), specularColor.GetGreen(), specularColor.GetBlue() ), m_Renderer );
  }
  m_SpecularColor = specularColor;
  Modified();
}


void mitk::MaterialProperty::SetSpecularColor( vtkFloatingPointType red, vtkFloatingPointType green, vtkFloatingPointType blue )
{
  if ( ForwardToDataTreeNode() )
  {
    m_DataTreeNode->SetProperty( SPECULAR_COLOR_KEY, mitk::ColorProperty::New( red, green, blue ), m_Renderer );
  }
  m_SpecularColor.Set( red, green, blue );
  Modified();
}

void mitk::MaterialProperty::SetSpecularCoefficient( vtkFloatingPointType specularCoefficient )
{
  if ( ForwardToDataTreeNode() )
  {
    m_DataTreeNode->SetProperty( SPECULAR_COEFFICIENT_KEY, mitk::FloatProperty::New( specularCoefficient ), m_Renderer );
  }
  m_SpecularCoefficient = specularCoefficient;
  Modified();
}


void mitk::MaterialProperty::SetSpecularPower( vtkFloatingPointType specularPower )
{
  if ( ForwardToDataTreeNode() )
  {
    m_DataTreeNode->SetProperty( SPECULAR_POWER_KEY, mitk::FloatProperty::New( specularPower ), m_Renderer );
  }
  m_SpecularPower = specularPower;
  Modified();
}


void mitk::MaterialProperty::SetOpacity( vtkFloatingPointType opacity )
{
  if ( ForwardToDataTreeNode() )
  {
    m_DataTreeNode->SetOpacity( opacity, m_Renderer );
  }
  m_Opacity = opacity;
  Modified();
}


void mitk::MaterialProperty::SetInterpolation( InterpolationType interpolation )
{
  if ( ForwardToDataTreeNode() )
  {
    m_DataTreeNode->SetProperty( INTERPOLATION_KEY, mitk::VtkInterpolationProperty::New( interpolation ), m_Renderer );
  }
  m_Interpolation = interpolation;
  Modified();
}


void mitk::MaterialProperty::SetRepresentation( RepresentationType representation )
{
  if ( ForwardToDataTreeNode() )
  {
    m_DataTreeNode->SetProperty( REPRESENTATION_KEY, mitk::VtkRepresentationProperty::New( representation ), m_Renderer );
  }
  m_Representation = representation;
  Modified();
}


void mitk::MaterialProperty::SetLineWidth( float lineWidth )
{
  if ( ForwardToDataTreeNode() )
  {
    m_DataTreeNode->SetProperty( LINE_WIDTH_KEY, mitk::FloatProperty::New( lineWidth ), m_Renderer );
  }
  m_LineWidth = lineWidth;
  Modified();
}

void mitk::MaterialProperty::SetPointSize( float pointSize )
{
  if ( ForwardToDataTreeNode() )
  {
    m_DataTreeNode->SetProperty( POINT_SIZE_KEY, mitk::FloatProperty::New( pointSize ), m_Renderer );
  }
  m_PointSize = pointSize;
  Modified();
}

mitk::MaterialProperty::Color mitk::MaterialProperty::GetColor() const
{
  if ( ForwardToDataTreeNode() )
  {
    float rgb[ 3 ];
    m_DataTreeNode->GetColor( rgb, m_Renderer );
    Color color;
    color.SetRed( rgb[ 0 ] );
    color.SetGreen( rgb[ 1 ] );
    color.SetBlue( rgb[ 2 ] );
    return color;
  }
  else
    return m_Color;
}


mitk::MaterialProperty::Color mitk::MaterialProperty::GetAmbientColor() const
{
  if ( ForwardToDataTreeNode() )
  {
    mitk::ColorProperty * colorProperty = dynamic_cast<mitk::ColorProperty*>( 
      m_DataTreeNode->GetProperty( AMBIENT_COLOR_KEY, m_Renderer ) );
    if ( colorProperty != NULL )
    {
      mitk::Color color = colorProperty->GetColor();
      Color tmpColor;
      tmpColor.Set( color[ 0 ], color[ 1 ], color[ 2 ] );
      return tmpColor;
    }
    else
      return m_AmbientColor;
  }
  else
    return m_AmbientColor;
}

mitk::MaterialProperty::Color mitk::MaterialProperty::GetDiffuseColor() const
{
  if ( ForwardToDataTreeNode() )
  {
    mitk::ColorProperty * colorProperty = dynamic_cast<mitk::ColorProperty*>( m_DataTreeNode->GetProperty( DIFFUSE_COLOR_KEY, m_Renderer ) );
    if ( colorProperty != NULL )
    {
      mitk::Color color = colorProperty->GetColor();
      Color tmpColor;
      tmpColor.Set( color[ 0 ], color[ 1 ], color[ 2 ] );
      return tmpColor;
    }
    else
      return m_DiffuseColor;
  }
  else
    return m_DiffuseColor;
}

vtkFloatingPointType mitk::MaterialProperty::GetAmbientCoefficient() const
{
  if ( ForwardToDataTreeNode() )
  {
    mitk::FloatProperty * colorCoefficient = dynamic_cast<mitk::FloatProperty*>( m_DataTreeNode->GetProperty( AMBIENT_COEFFICIENT_KEY, m_Renderer ) );
    if ( colorCoefficient != NULL )
      return colorCoefficient->GetValue();
    else
      return m_AmbientCoefficient;
  }
  else
    return m_AmbientCoefficient;
}


vtkFloatingPointType mitk::MaterialProperty::GetDiffuseCoefficient() const
{
  if ( ForwardToDataTreeNode() )
  {
    mitk::FloatProperty * colorCoefficient = dynamic_cast<mitk::FloatProperty*>( m_DataTreeNode->GetProperty( DIFFUSE_COEFFICIENT_KEY, m_Renderer ) );
    if ( colorCoefficient != NULL )
      return colorCoefficient->GetValue();
    else
      return m_DiffuseCoefficient;
  }
  else
    return m_DiffuseCoefficient;
}

mitk::MaterialProperty::Color mitk::MaterialProperty::GetSpecularColor() const
{
  if ( ForwardToDataTreeNode() )
  {
    mitk::ColorProperty * colorProperty = dynamic_cast<mitk::ColorProperty*>( m_DataTreeNode->GetProperty( SPECULAR_COLOR_KEY, m_Renderer ) );
    if ( colorProperty != NULL )
    {
      mitk::Color color = colorProperty->GetColor();
      Color tmpColor;
      tmpColor.Set( color[ 0 ], color[ 1 ], color[ 2 ] );
      return tmpColor;
    }
    else
      return m_SpecularColor;
  }
  else
    return m_SpecularColor;
}

vtkFloatingPointType mitk::MaterialProperty::GetSpecularCoefficient() const
{
  if ( ForwardToDataTreeNode() )
  {
    mitk::FloatProperty * specularCoefficient = dynamic_cast<mitk::FloatProperty*>( m_DataTreeNode->GetProperty( SPECULAR_COEFFICIENT_KEY, m_Renderer ));
    if ( specularCoefficient != NULL )
    {
      return specularCoefficient->GetValue();
    }
    else
    {
      return m_SpecularCoefficient;
    }
  }
  else
  {
    return m_SpecularCoefficient;
  }
}


vtkFloatingPointType mitk::MaterialProperty::GetSpecularPower() const
{
  if ( ForwardToDataTreeNode() )
  {
    mitk::FloatProperty * specularPower = dynamic_cast<mitk::FloatProperty*>( m_DataTreeNode->GetProperty( SPECULAR_POWER_KEY, m_Renderer ));
    if ( specularPower != NULL )
      return specularPower->GetValue();
    else
      return m_SpecularPower;
  }
  else
    return m_SpecularPower;
}


vtkFloatingPointType mitk::MaterialProperty::GetOpacity() const
{
  if ( ForwardToDataTreeNode() )
  {
    float opacity = 1.0;
    m_DataTreeNode->GetOpacity( opacity, m_Renderer );
    return opacity;
  }
  else
    return m_Opacity;
}

mitk::MaterialProperty::InterpolationType mitk::MaterialProperty::GetInterpolation() const
{
  if ( ForwardToDataTreeNode() )
  {
    mitk::VtkInterpolationProperty * interpolationProperty = dynamic_cast<mitk::VtkInterpolationProperty*>( m_DataTreeNode->GetProperty( INTERPOLATION_KEY, m_Renderer ));
    if ( interpolationProperty != NULL )
      return static_cast<mitk::MaterialProperty::InterpolationType>( interpolationProperty->GetValueAsId() );
    else
      return m_Interpolation;
  }
  else
    return m_Interpolation;
}

mitk::MaterialProperty::RepresentationType mitk::MaterialProperty::GetRepresentation() const
{
  if ( ForwardToDataTreeNode() )
  {
    mitk::VtkRepresentationProperty * representationProperty = dynamic_cast<mitk::VtkRepresentationProperty*>( m_DataTreeNode->GetProperty( REPRESENTATION_KEY, m_Renderer ));
    if ( representationProperty != NULL )
      return static_cast<mitk::MaterialProperty::RepresentationType>( representationProperty->GetValueAsId() );
    else
      return m_Representation;
  }
  else
    return m_Representation;
}


int mitk::MaterialProperty::GetVtkInterpolation() const
{
  if ( ForwardToDataTreeNode() )
  {
    mitk::VtkInterpolationProperty * interpolationProperty = dynamic_cast<mitk::VtkInterpolationProperty*>( m_DataTreeNode->GetProperty( INTERPOLATION_KEY, m_Renderer ));
    if ( interpolationProperty != NULL )
      return interpolationProperty->GetVtkInterpolation();
    else
      return ( int ) m_Interpolation;
  }
  else
  {
    switch ( m_Interpolation )
    {
    case( Flat ) : return VTK_FLAT;
    case( Gouraud ) : return VTK_GOURAUD;
    case( Phong ) : return VTK_PHONG;
    }
    return VTK_GOURAUD;
  }
}

int mitk::MaterialProperty::GetVtkRepresentation() const
{
  if ( ForwardToDataTreeNode() )
  {
    mitk::VtkRepresentationProperty * representationProperty = dynamic_cast<mitk::VtkRepresentationProperty*>( m_DataTreeNode->GetProperty( REPRESENTATION_KEY, m_Renderer ));
    if ( representationProperty != NULL )
      return representationProperty->GetVtkRepresentation();
    else
      return ( int ) m_Representation;
  }
  else
  {
    switch ( m_Representation )
    {
    case( Points ) : return VTK_POINTS;
    case( Wireframe ) : return VTK_WIREFRAME;
    case( Surface ) : return VTK_SURFACE;
    case ( WireframeSurface ): return VTK_SURFACE;
    }
    return VTK_SURFACE;
  }
}

float mitk::MaterialProperty::GetLineWidth() const
{
  if ( ForwardToDataTreeNode() )
  {
    mitk::FloatProperty * lineWidth = dynamic_cast<mitk::FloatProperty*>( m_DataTreeNode->GetProperty( LINE_WIDTH_KEY, m_Renderer ));
    if ( lineWidth != NULL )
      return lineWidth->GetValue();
    else
      return m_LineWidth;
  }
  else
    return m_LineWidth;
}

float mitk::MaterialProperty::GetPointSize() const
{
  if ( ForwardToDataTreeNode() )
  {
    mitk::FloatProperty * pointSize = dynamic_cast<mitk::FloatProperty*>( m_DataTreeNode->GetProperty( POINT_SIZE_KEY, m_Renderer ));
    if ( pointSize != NULL )
      return pointSize->GetValue();
    else
      return m_PointSize;
  }
  else
    return m_PointSize;
}

void mitk::MaterialProperty::Initialize( const MaterialProperty& property, const bool& copyDataTreeNode )
{
  if ( copyDataTreeNode )
  {
    this->SetDataTreeNode( property.GetDataTreeNode() );
    this->SetRenderer( property.GetRenderer() );
  }
  this->SetColor( property.GetColor() );
  this->SetAmbientColor( property.GetAmbientColor() );
  this->SetDiffuseColor( property.GetDiffuseColor() );
  this->SetAmbientCoefficient( property.GetAmbientCoefficient() );
  this->SetDiffuseCoefficient( property.GetDiffuseCoefficient() );
  this->SetSpecularColor( property.GetSpecularColor() );
  this->SetSpecularCoefficient( property.GetSpecularCoefficient() );
  this->SetSpecularPower( property.GetSpecularPower() );
  this->SetOpacity( property.GetOpacity() );
  this->SetInterpolation( property.GetInterpolation() );
  this->SetRepresentation( property.GetRepresentation() );
  this->SetLineWidth( property.GetLineWidth() );
  this->SetPointSize( property.GetPointSize() );
  this->SetName( property.GetName() );
}


bool mitk::MaterialProperty::operator==( const BaseProperty& property ) const
{
  const Self * other = dynamic_cast<const Self*>( &property );

  if ( other == NULL )
    return false;
  else
    return ( m_DataTreeNode == other->GetDataTreeNode() &&
             m_Renderer == other->GetRenderer() &&
             m_Color == other->GetColor() &&
             m_AmbientColor == other->GetAmbientColor() &&
             m_DiffuseColor == other->GetDiffuseColor() &&
             m_AmbientCoefficient == other->GetAmbientCoefficient() &&
             m_DiffuseCoefficient == other->GetDiffuseCoefficient() &&
             m_SpecularColor == other->GetSpecularColor() &&
             m_SpecularCoefficient == other->GetSpecularCoefficient() &&
             m_SpecularPower == other->GetSpecularPower() &&
             m_Opacity == other->GetOpacity() &&
             m_Interpolation == other->GetInterpolation() &&
             m_Name == other->GetName() &&
             m_Representation == other->GetRepresentation() &&
             m_LineWidth == other->GetLineWidth() &&
       m_PointSize == other->GetPointSize()
           );
}



bool mitk::MaterialProperty::ForwardToDataTreeNode() const
{
  return ( m_DataTreeNode != NULL );
}

void mitk::MaterialProperty::InitializeStandardValues()
{
  m_DataTreeNode = NULL;
  m_Renderer = NULL;
  m_Color.Set( 1.0, 1.0, 1.0 );
  m_AmbientColor.Set( 1.0, 1.0, 1.0 );
  m_DiffuseColor.Set( 1.0, 1.0, 1.0 );
  m_AmbientCoefficient = 0.0 ;
  m_DiffuseCoefficient = 1.0 ;
  m_SpecularColor.Set( 1.0, 1.0, 1.0 );
  m_SpecularCoefficient = 0.0;
  m_SpecularPower = 10.0;
  m_Opacity = 1.0 ;
  m_Interpolation = Gouraud;
  m_Representation = Surface;
  m_LineWidth = 1.0;
  m_PointSize = 1.0;
  m_Name = "";
}

void mitk::MaterialProperty::Update()
{
  this->SetColor( this->GetColor() );
  this->SetAmbientColor( this->GetAmbientColor() );
  this->SetDiffuseColor( this->GetDiffuseColor() );
  this->SetAmbientCoefficient( this->GetAmbientCoefficient() );
  this->SetDiffuseCoefficient( this->GetDiffuseCoefficient() );
  this->SetSpecularColor( this->GetSpecularColor() );
  this->SetSpecularCoefficient( this->GetSpecularCoefficient() );
  this->SetSpecularPower( this->GetSpecularPower() );
  this->SetOpacity( this->GetOpacity() );
  this->SetInterpolation( this->GetInterpolation() );
  this->SetRepresentation( this->GetRepresentation() );
  this->SetPointSize(this->GetPointSize());
}

void mitk::MaterialProperty::PrintSelf ( std::ostream &os ) const
{
  os << "Data tree node: " << GetDataTreeNode() << std::endl;
  os << "Renderer: " << GetRenderer() << std::endl;
  os << "Name: " << GetName() << std::endl;
  os << "Color: " << GetColor() << std::endl;
  os << "AmbientColor: " << GetAmbientColor() << std::endl;
  os << "DiffuseColor: " << GetDiffuseColor() << std::endl;
  os << "AmbientCoefficient" << GetAmbientCoefficient() << std::endl;
  os << "DiffuseCoefficient" << GetDiffuseCoefficient() << std::endl;
  os << "SpecularColor: " << GetSpecularColor() << std::endl;
  os << "SpecularCoefficient: " << GetSpecularCoefficient() << std::endl;
  os << "SpecularPower: " << GetSpecularPower() << std::endl;
  os << "Opacity: " << GetOpacity() << std::endl;
  os << "Line width: " << GetLineWidth() << std::endl;
  os << "Point Size: " << GetPointSize() << std::endl;
  switch ( GetInterpolation() )
  {
  case ( Flat ) : os << "Interpolation: Flat" << std::endl;
    break;
  case ( Gouraud ) : os << "Interpolation: Gouraud" << std::endl;
    break;
  case ( Phong ) : os << "Interpolation: Phong" << std::endl;
    break;
  }
  switch ( GetRepresentation() )
  {
  case ( Points ) : os << "Representation: Points" << std::endl;
    break;
  case ( Wireframe ) : os << "Representation: Wireframe" << std::endl;
    break;
  case ( Surface ) : os << "Representation: Surface" << std::endl;
    break;
  }
}

const char* mitk::MaterialProperty::COLOR_KEY = "color";
const char* mitk::MaterialProperty::AMBIENT_COLOR_KEY = "ambient_color";
const char* mitk::MaterialProperty::DIFFUSE_COLOR_KEY = "diffuse_color";
const char* mitk::MaterialProperty::SPECULAR_COLOR_KEY = "specular_color";
const char* mitk::MaterialProperty::AMBIENT_COEFFICIENT_KEY = "ambient_coefficient";
const char* mitk::MaterialProperty::DIFFUSE_COEFFICIENT_KEY = "diffuse_coefficient";
const char* mitk::MaterialProperty::SPECULAR_COEFFICIENT_KEY = "specular_coefficient";
const char* mitk::MaterialProperty::SPECULAR_POWER_KEY = "specular_power";
const char* mitk::MaterialProperty::OPACITY_KEY = "opacity";
const char* mitk::MaterialProperty::INTERPOLATION_KEY = "interpolation";
const char* mitk::MaterialProperty::REPRESENTATION_KEY = "representation";
const char* mitk::MaterialProperty::LINE_WIDTH_KEY = "wireframe line width";
const char* mitk::MaterialProperty::POINT_SIZE_KEY = "point size";

const char* mitk::MaterialProperty::COLOR = "COLOR";
const char* mitk::MaterialProperty::AMBIENT_COLOR = "AMBIENT_COLOR";
const char* mitk::MaterialProperty::DIFFUSE_COLOR = "DIFFUSE_COLOR";
const char* mitk::MaterialProperty::SPECULAR_COLOR = "SPECULAR_COLOR";
const char* mitk::MaterialProperty::AMBIENT_COEFFICIENT = "AMBIENT_COEFFICIENT";
const char* mitk::MaterialProperty::DIFFUSE_COEFFICIENT = "DIFFUSE_COEFFICIENT";
const char* mitk::MaterialProperty::SPECULAR_COEFFICIENT = "SPECULAR_COEFFICIENT";
const char* mitk::MaterialProperty::SPECULAR_POWER = "SPECULAR_POWER";
const char* mitk::MaterialProperty::OPACITY = "OPACITY";
const char* mitk::MaterialProperty::INTERPOLATION = "INTERPOLATION";
const char* mitk::MaterialProperty::REPRESENTATION = "REPRESENTATION";
