/*=========================================================================

Program:   Medical Imaging & Interaction Toolkit
Language:  C++
Date:      $Date: 2009-05-12 19:56:03 +0200 (mar, 12 may 2009) $
Version:   $Revision: 17179 $

Copyright (c) German Cancer Research Center, Division of Medical and
Biological Informatics. All rights reserved.
See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notices for more information.

=========================================================================*/

#ifndef SURFACEWRITERFACTORY_H_HEADER_INCLUDED
#define SURFACEWRITERFACTORY_H_HEADER_INCLUDED

#include "itkObjectFactoryBase.h"
#include "mitkBaseData.h"

namespace mitk
{

class MITK_CORE_EXPORT SurfaceVtkWriterFactory : public itk::ObjectFactoryBase
{
public:

  mitkClassMacro( mitk::SurfaceVtkWriterFactory, itk::ObjectFactoryBase )

  /** Class methods used to interface with the registered factories. */
  virtual const char* GetITKSourceVersion(void) const;
  virtual const char* GetDescription(void) const;

  /** Method for class instantiation. */
  itkFactorylessNewMacro(Self);

  /** Register one factory of this type  */
  static void RegisterOneFactory(void)
  {
    if ( !IsRegistered )
    {
      surfaceVtkWriterFactory = SurfaceVtkWriterFactory::New();
      ObjectFactoryBase::RegisterFactory( surfaceVtkWriterFactory );
      IsRegistered = true;
    }
  }

  /** UnRegister one factory of this type  */
  static void UnRegisterOneFactory( )
  {
	  if ( IsRegistered )
	  {
		  ObjectFactoryBase::UnRegisterFactory( surfaceVtkWriterFactory );
		  surfaceVtkWriterFactory = NULL;
		  IsRegistered = false;
	  }
  }

protected:
  SurfaceVtkWriterFactory();
  ~SurfaceVtkWriterFactory();

private:
  SurfaceVtkWriterFactory(const Self&); //purposely not implemented
  void operator=(const Self&); //purposely not implemented

  static bool IsRegistered;
  static SurfaceVtkWriterFactory::Pointer surfaceVtkWriterFactory;
};

} // end namespace mitk

#endif



