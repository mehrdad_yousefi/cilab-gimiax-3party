/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/

#ifndef __mitkSlicesRotatorMovement_H
#define __mitkSlicesRotatorMovement_H

#include <mitkSlicesRotator.h>
#include <mitkInteractionConst.h>
#include <mitkBaseRenderer.h>

namespace mitk{ class Geometry2D; }

class vtkRenderWindow;

namespace mitk {

typedef enum{
  GEOMETRY_CLICKED,
  GEOMETRY_ROTATED,
  GEOMETRY_OTHER,
  GEOMETRY_MAX
} GEOMETRY_TYPE;

/**
 \brief Redefinition of mitk::SlicesRotator, unifying the rotation and
 translation. The user needs to drag and drop the axis. The cursor
 icon is automatically changed when the mouse pointer can perform
 any of the actions.

 There are three states depending on the position of the mouse pointer
 related to the axis:
 - Center: Axis can be moved in any direction
 - Middle: Axis can be translated
 - Extremes: Axis can be rotated

 \ingroup NavigationControl
 \date 19 09 08
 \author Xavi Planes
 */
class MITK_CORE_EXPORT SlicesRotatorMovement : public mitk::SlicesRotator
{
public:

  mitkClassMacro(SlicesRotatorMovement, mitk::SlicesRotator);
  
  mitkNewMacro1Param(Self, const char*);

  vtkRenderWindow * GetVtkLastRenderWindow() const;

protected:

  //! 
  SlicesRotatorMovement(const char* machine);

  //! Clear list of controllers
  virtual ~SlicesRotatorMovement();

  //!
  virtual bool ExecuteAction(mitk::Action * action, mitk::StateEvent const* stateEvent);

  //!
  mitk::EActions ComputeMoveOrRotate( 
    mitk::BaseRenderer *renderer,
    mitk::Point3D cursor, 
    std::vector<mitk::Geometry2D*> &geometryArray );

  //!
  bool ComputeCursorAndCenterOfRotation( 
    mitk::Point3D cursor, 
    mitk::DisplayGeometry* displayGeometry,
    std::vector<mitk::Geometry2D*> &geometryArray,
    mitk::ScalarType &cursorCenterDistance );

  //!
  void GetGeometries( 
    mitk::BaseRenderer* renderer,
    mitk::Point3D cursor,
    std::vector<mitk::Geometry2D*> &geometryArray );

  //! Get the geometry where the event comes from
  bool GetClickedGeometry( 
    BaseRenderer* renderer,
    std::vector<SliceNavigationController*> &controllerArray );

  /** Find closest controller to cursor with distance < ThreshHoldDistancePixelsMin
  not present in controllerArray, with direction != controller
  */
  SliceNavigationController* FindClosestController( 
    BaseRenderer* renderer,
    mitk::Point3D cursor,
    std::vector<SliceNavigationController*> &controllerArray,
    mitk::ScalarType &minDistancePixels);

  //!
  Geometry2D* GetGeometry2D( SliceNavigationController* controller );

  /** Check distance from the controller 2D geometry to the cursor in the
  display geometry of the renderer < threshold
  */
  double ComputeDistance( 
    BaseRenderer* renderer,
    SliceNavigationController* controller,
    mitk::Point3D cursor );

protected:

  /**
  \brief Last accessed vtkRenderWindow
  We need to restore the cursor of the mouse when this interactor is 
  disabled from the global interactor
  */
  vtkRenderWindow  *m_vtkLastRenderWindow;

  /// all SNCs that will be moved
  SNCVector m_SNCsToBeMoved; 

};

} // namespace 

#endif  // __mitkSlicesRotatorMovement_H


