/*=========================================================================
 
Program:   Medical Imaging & Interaction Toolkit
Language:  C++
Date:      $Date: 2009-11-11 17:33:19 +0100 (mié, 11 nov 2009) $
Version:   $Revision: 16916 $
 
Copyright (c) German Cancer Research Center, Division of Medical and
Biological Informatics. All rights reserved.
See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.
 
This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notices for more information.
 
=========================================================================*/

#include "mitkCoreExtObjectFactory.h"

#include "mitkProperties.h"
#include "mitkBaseRenderer.h"
#include "mitkDataTreeNode.h"

#include "mitkParRecFileIOFactory.h"
#include "mitkObjFileIOFactory.h"
#include "mitkVtkUnstructuredGridIOFactory.h"
#include "mitkStlVolumeTimeSeriesIOFactory.h"
#include "mitkVtkVolumeTimeSeriesIOFactory.h"
#include "mitkPlanarFigureIOFactory.h"
#include "mitkPlanarFigureWriterFactory.h"

#include "mitkCone.h"
#include "mitkContour.h"
#include "mitkContourMapper2D.h"
#include "mitkContourSetMapper2D.h"
#include "mitkContourSetVtkMapper3D.h"
#include "mitkContourVtkMapper3D.h"
#include "mitkCuboid.h"
#include "mitkCylinder.h"
#include "mitkEllipsoid.h"
#include "mitkMeshMapper2D.h"
#include "mitkMeshVtkMapper3D.h"
#include "mitkUnstructuredGridMapper2D.h"
#include "mitkPointDataVtkMapper3D.h"
#include "mitkEnhancedPointSetVtkMapper3D.h"
#include "mitkSeedsImage.h"
#include "mitkUnstructuredGrid.h"
#include "mitkUnstructuredGridVtkMapper3D.h"
#include "mitkPolyDataGLMapper2D.h"
#include "mitkGPUVolumeMapper3D.h"
#include "mitkPlanarFigure.h"
#include "mitkPlanarFigureMapper2D.h"

#include "mitkVolumeDataVtkMapper3D.h"

bool mitk::CoreExtObjectFactory::alreadyDone = false;

mitk::CoreExtObjectFactory::CoreExtObjectFactory(bool registerSelf) 
:CoreObjectFactory()
{
  if (!alreadyDone)
  {
    LOG_INFO << "CoreExtObjectFactory c'tor" << std::endl;
    RegisterIOFactories();

	ParRecFileIOFactory::Pointer parRecFileIOFactory = ParRecFileIOFactory::New();
	m_Factories.push_back( parRecFileIOFactory.GetPointer( ) );
	ObjFileIOFactory::Pointer objFileIOFactory = ObjFileIOFactory::New();
	m_Factories.push_back( objFileIOFactory.GetPointer( ) );
	VtkUnstructuredGridIOFactory::Pointer vtkUnstructuredGridIOFactory = VtkUnstructuredGridIOFactory::New();
	m_Factories.push_back( vtkUnstructuredGridIOFactory.GetPointer( ) );
	StlVolumeTimeSeriesIOFactory::Pointer stlVolumeTimeSeriesIOFactory = StlVolumeTimeSeriesIOFactory::New();
	m_Factories.push_back( stlVolumeTimeSeriesIOFactory.GetPointer( ) );
	VtkVolumeTimeSeriesIOFactory::Pointer vtkVolumeTimeSeriesIOFactory = VtkVolumeTimeSeriesIOFactory::New();
	m_Factories.push_back( vtkVolumeTimeSeriesIOFactory.GetPointer( ) );
	PlanarFigureIOFactory::Pointer planarFigureIOFactory = PlanarFigureIOFactory::New();
	m_Factories.push_back( planarFigureIOFactory.GetPointer( ) );

	FactoriesList::iterator it;
	for ( it = m_Factories.begin() ; it != m_Factories.end() ; it++ )
	{
		itk::ObjectFactoryBase::RegisterFactory( *it );
	}

    PlanarFigureWriterFactory::RegisterOneFactory();

    alreadyDone = true;
  }

  if (registerSelf) 
  {
    this->RegisterOverride("mitkCoreObjectFactoryBase",
      "mitkCoreExtObjectFactory",
      "mitk Mapper Creation",
      1,
      itk::CreateObjectFunction<mitk::CoreExtObjectFactory>::New());
  }
}

mitk::CoreExtObjectFactory::~CoreExtObjectFactory()
{
	if (alreadyDone)
	{
		FactoriesList::iterator it;
		for ( it = m_Factories.begin() ; it != m_Factories.end() ; it++ )
		{
			itk::ObjectFactoryBase::UnRegisterFactory( *it );
		}
		m_Factories.clear();

		PlanarFigureWriterFactory::UnRegisterOneFactory();
	}

}

#define CREATE_CPP( TYPE, NAME ) else if ( className == NAME ) {pointer = new TYPE(); pointer->Register();}
#define CREATE_ITK( TYPE, NAME ) else if ( className == NAME ) pointer = TYPE::New();

itk::Object::Pointer mitk::CoreExtObjectFactory::CreateCoreObject( const std::string& className )
{
  itk::Object::Pointer pointer;

  if ( className == "" )
    return NULL;
  CREATE_ITK( Contour, "Contour" )
  CREATE_ITK( Ellipsoid, "Ellipsoid" )
  CREATE_ITK( Cylinder, "Cylinder" )
  CREATE_ITK( Cuboid, "Cuboid" )
  CREATE_ITK( Cone, "Cone" )
  CREATE_ITK( SeedsImage, "SeedsImage" )
  CREATE_ITK( UnstructuredGrid, "UnstructuredGrid" )
  CREATE_ITK( UnstructuredGridVtkMapper3D, "UnstructuredGridVtkMapper3D" )
  else
    pointer = Superclass::CreateCoreObject( className );

  return pointer;
}


mitk::Mapper::Pointer mitk::CoreExtObjectFactory::CreateMapper(mitk::DataTreeNode* node, MapperSlotId id) 
{
  mitk::Mapper::Pointer newMapper=NULL;
  mitk::BaseData *data = node->GetData();

  if ( id == mitk::BaseRenderer::Standard2D )
  {
    if((dynamic_cast<Mesh*>(data)!=NULL))
    {
      newMapper = mitk::MeshMapper2D::New();
      newMapper->SetDataTreeNode(node);
    }
    else if((dynamic_cast<PointData*>(data)!=NULL))
    {
      newMapper = mitk::PolyDataGLMapper2D::New();
      newMapper->SetDataTreeNode(node);
    }
    else if((dynamic_cast<Contour*>(data)!=NULL))
    {
      newMapper = mitk::ContourMapper2D::New();
      newMapper->SetDataTreeNode(node);
    }
    else if((dynamic_cast<ContourSet*>(data)!=NULL))
    {
      newMapper = mitk::ContourSetMapper2D::New();
      newMapper->SetDataTreeNode(node);
    }
    else if((dynamic_cast<UnstructuredGrid*>(data)!=NULL))
    {
      newMapper = mitk::UnstructuredGridMapper2D::New();
      newMapper->SetDataTreeNode(node);
    }
    else if((dynamic_cast<PlanarFigure*>(data)!=NULL))
    {
      newMapper = mitk::PlanarFigureMapper2D::New();
      newMapper->SetDataTreeNode(node);
    }
  }
  else if ( id == mitk::BaseRenderer::Standard3D )
  {
    if((dynamic_cast<Image*>(data) != NULL))
    {
      newMapper = mitk::GPUVolumeMapper3D::New();
      newMapper->SetDataTreeNode(node);
    }
    else if((dynamic_cast<Mesh*>(data)!=NULL))
    {
      newMapper = mitk::MeshVtkMapper3D::New();
      newMapper->SetDataTreeNode(node);
    }
    else if((dynamic_cast<PointData*>(data)!=NULL))
    {
      newMapper = mitk::PointDataVtkMapper3D::New();
      newMapper->SetDataTreeNode(node);
    }
    else if((dynamic_cast<Contour*>(data)!=NULL))
    {
      newMapper = mitk::ContourVtkMapper3D::New();
      newMapper->SetDataTreeNode(node);
    }
    else if((dynamic_cast<ContourSet*>(data)!=NULL))
    {
      newMapper = mitk::ContourSetVtkMapper3D::New();
      newMapper->SetDataTreeNode(node);
    }
    else if((dynamic_cast<UnstructuredGrid*>(data)!=NULL))
    {
      newMapper = mitk::UnstructuredGridVtkMapper3D::New();
      newMapper->SetDataTreeNode(node);
    }
  }

  if (newMapper.IsNull()) {
    newMapper = Superclass::CreateMapper(node,id);
  }
  return newMapper;
}

void mitk::CoreExtObjectFactory::SetDefaultProperties(mitk::DataTreeNode* node)
{
  Superclass::SetDefaultProperties(node);

  if(node==NULL)
    return;

  mitk::DataTreeNode::Pointer nodePointer = node;

  mitk::PlanarFigure::Pointer pf = dynamic_cast<mitk::PlanarFigure*>(node->GetData());
  if(pf.IsNotNull())
  {
    mitk::PlanarFigureMapper2D::SetDefaultProperties(node);
  }
}

const char* mitk::CoreExtObjectFactory::GetFileExtensions() 
{
  return Superclass::GetFileExtensions();
};

const char* mitk::CoreExtObjectFactory::GetSaveFileExtensions() 
{ 
  return ";;Planar Figures (*.pf)";  // for mitk::PlanarFigure and derived classes
};

void mitk::CoreExtObjectFactory::RegisterIOFactories() 
{
}

void RegisterCoreExtObjectFactory() 
{
  bool oneCoreExtObjectFactoryRegistered = false;
  if ( ! oneCoreExtObjectFactoryRegistered ) {
    LOG_INFO << "Registering CoreExtObjectFactory..." << std::endl;
    itk::ObjectFactoryBase::RegisterFactory(mitk::CoreExtObjectFactory::New());
    oneCoreExtObjectFactoryRegistered = true;
  }
}
