/*=========================================================================
/*
* Copyright (c) 2009,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* See license.txt file for details.
*/


/*=========================================================================*/
// .NAME vtkMitkOpenglXRAYVolumeTextureMapper2D - class for a volume mapper for XRay Rendering with 2D Textures
// this class is derived directly by vtkVolumeTexture2D and vtkOpenglVolumeTexture2d

// .SECTION Description
// vtkMitkOpenglXRAYVolumeTextureMapper2D renders a volume using 2D texture mapping.


// .SECTION see also
// vtkVolumeMapper

#ifndef __vtkMitkOpenglXRAYVolumeTextureMapper2D_h
#define __vtkMitkOpenglXRAYVolumeTextureMapper2D_h

//****patch for x-ray
#include "vtkOpenGLExtensionManager.h"
#include "vtkgl.h" // vtkgl namespace
#include "ParseOGLExt/headers/glext.h"
//****end of patch for x-ray

#include "vtkVolumeTextureMapper.h"

#include "mitkCommon.h"

class MITKEXT_CORE_EXPORT vtkMitkOpenglXRAYVolumeTextureMapper2D : public vtkVolumeTextureMapper
{
public:
  vtkTypeRevisionMacro(vtkMitkOpenglXRAYVolumeTextureMapper2D,vtkVolumeTextureMapper);
  void PrintSelf( ostream& os, vtkIndent indent );

    static vtkMitkOpenglXRAYVolumeTextureMapper2D *New();
  
  // Description:
  // Target size in pixels of each size of the texture for downloading. Default is
  // 512x512 - so a 512x512 texture will be tiled with as many slices of the volume
  // as possible, then all the quads will be rendered. This can be set to optimize
  // for a particular architecture. This must be set with numbers that are a power
  // of two.
  vtkSetVector2Macro( TargetTextureSize, int );
  vtkGetVector2Macro( TargetTextureSize, int );
  
  // Description:
  // This is the maximum number of planes that will be created for texture mapping
  // the volume. If the volume has more voxels than this along the viewing direction,
  // then planes of the volume will be skipped to ensure that this maximum is not
  // violated. A skip factor is used, and is incremented until the maximum condition
  // is satisfied.
  vtkSetMacro( MaximumNumberOfPlanes, int );
  vtkGetMacro( MaximumNumberOfPlanes, int );

  // Description:
  // This is the maximum size of saved textures in bytes. If this size is large
  // enough to hold the RGBA textures for all three directions (XxYxZx3x4 is
  // the approximate value - it is actually a bit larger due to wasted space in
  // the textures) then the textures will be saved.
  vtkSetMacro( MaximumStorageSize, int );
  vtkGetMacro( MaximumStorageSize, int );


  // Description:
  // Set/Get the xRayAttenuation factor. 
  vtkSetMacro( XRayAttenuation, double );
  void SetXRayAttenuationDefault()
  { this->SetXRayAttenuation(1.0); }
  vtkGetMacro( XRayAttenuation, double );

  // Description:
  // Set/Get the xRayThreshold factor. 
  vtkSetMacro( XRayThreshold, double );
  void SetXRayThresholdDefault()
  { this->SetXRayThreshold(0.5); }
  vtkGetMacro( XRayThreshold, double );

//BTX

  // Description:
  // WARNING: INTERNAL METHOD - NOT INTENDED FOR GENERAL USE
  // DO NOT USE THIS METHOD OUTSIDE OF THE RENDERING PROCESS
  // Render the volume

  // Description:
  // WARNING: INTERNAL METHOD - NOT INTENDED FOR GENERAL USE
  // DO NOT USE THIS METHOD OUTSIDE OF THE RENDERING PROCESS
  // Render the volume
  
  virtual void Render(vtkRenderer *ren, vtkVolume *vol);


  void RenderQuads( int count, float *v, float *t,
	  unsigned char *texture, int size[2], int reverseFlag);


  // Description:
  // Made public only for access from the templated method. Not a vtkGetMacro
  // to avoid the PrintSelf defect.
  int GetInternalSkipFactor() {return this->InternalSkipFactor;};
  
  int *GetAxisTextureSize() {return &(this->AxisTextureSize[0][0]);};

  int GetSaveTextures() {return this->SaveTextures;};

  unsigned char *GetTexture() {return this->Texture;};
  
//ETX


protected:
  vtkMitkOpenglXRAYVolumeTextureMapper2D();
  ~vtkMitkOpenglXRAYVolumeTextureMapper2D();

  void InitializeRender( vtkRenderer *ren, vtkVolume *vol )
    {this->InitializeRender( ren, vol, -1 );}
  
  void InitializeRender( vtkRenderer *ren, vtkVolume *vol, int majorDirection );

  void GenerateTexturesAndRenderQuads( vtkRenderer *ren, vtkVolume *vol );

  int  MajorDirection;
  int  TargetTextureSize[2];

  int  MaximumNumberOfPlanes;
  int  InternalSkipFactor;
  int  MaximumStorageSize;

  double XRayAttenuation;
  double XRayThreshold;
  
  unsigned char  *Texture;
  int             TextureSize;
  int             SaveTextures;
  vtkTimeStamp    TextureMTime;
  
  int             AxisTextureSize[3][3];
  void            ComputeAxisTextureSize( int axis, int *size );
  
  void           RenderSavedTexture();
  
private:
  vtkMitkOpenglXRAYVolumeTextureMapper2D(const vtkMitkOpenglXRAYVolumeTextureMapper2D&);  // Not implemented.
  void operator=(const vtkMitkOpenglXRAYVolumeTextureMapper2D&);  // Not implemented.
};


#endif


