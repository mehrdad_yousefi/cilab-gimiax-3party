/*=========================================================================

Program:   Medical Imaging & Interaction Toolkit
Language:  C++
Date:      $Date: 2007-12-11 14:46:19 +0100 (mar, 11 dic 2007) $
Version:   $Revision: 13129 $

Copyright (c) German Cancer Research Center, Division of Medical and
Biological Informatics. All rights reserved.
See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notices for more information.

=========================================================================*/


#ifndef MITKGLUT_H_HEADER_INCLUDED
#define MITKGLUT_H_HEADER_INCLUDED

// Since 7.8.07 MITK is not using/requiring GLUT anymore.

#ifdef __APPLE__
//#include <glut.h>
#else
//#include <GL/glut.h>
#endif

#endif /* MITKGLUT_H_HEADER_INCLUDED */

