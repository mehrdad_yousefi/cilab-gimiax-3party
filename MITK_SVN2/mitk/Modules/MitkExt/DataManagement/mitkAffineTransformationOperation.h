/*=========================================================================

Program:   Medical Imaging & Interaction Toolkit
Language:  C++
Date:      $Date: 2009-05-13 18:06:46 +0200 (mié, 13 may 2009) $
Version:   $Revision: 17258 $

Copyright (c) German Cancer Research Center, Division of Medical and
Biological Informatics. All rights reserved.
See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notices for more information.

=========================================================================*/


#ifndef MITKAFFINETRANSFORMATIONOPERATION_H_HEADER_INCLUDED
#define MITKAFFINETRANSFORMATIONOPERATION_H_HEADER_INCLUDED

#include "mitkPointOperation.h"
#include "mitkVector.h"

namespace mitk {

//##Documentation
//## @brief Operation, that holds everything necessary for an affine operation.
//##
//## @ingroup Undo
class MITKEXT_CORE_EXPORT AffineTransformationOperation : public PointOperation
{
public:
  AffineTransformationOperation(OperationType operationType, Point3D point, ScalarType angle, int index);
  virtual ~AffineTransformationOperation(void);
  ScalarType GetAngle();
protected:
  ScalarType m_Angle;
};

} // namespace mitk

#endif /* MITKAFFINETRANSFORMATIONOPERATION_H_HEADER_INCLUDED */


