/*=========================================================================

Program:   Medical Imaging & Interaction Toolkit
Language:  C++
Date:      $Date: 2009-05-13 18:06:46 +0200 (mié, 13 may 2009) $
Version:   $Revision: 17258 $

Copyright (c) German Cancer Research Center, Division of Medical and
Biological Informatics. All rights reserved.
See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notices for more information.

=========================================================================*/
#ifndef MITKCOLORSEQUENCE_H_URTESEINDEUTIGEKENNUNG_01
#define MITKCOLORSEQUENCE_H_URTESEINDEUTIGEKENNUNG_01

#include <mitkColorProperty.h>

namespace mitk
{

/*!
  \brief Inferface for creating a sequence of nice/matching/appropriate/... colors.

  See derived classes for implemented sequences. 
*/
class MITKEXT_CORE_EXPORT ColorSequence
{
public:
  ColorSequence();
  virtual ~ColorSequence();

  /*!  
  \brief Return another color
  */ 
  virtual Color GetNextColor() = 0; 

  /*!  
  \brief Set the color-index to begin again
  */ 
  virtual void GoToBegin() = 0;

};

}

#endif


