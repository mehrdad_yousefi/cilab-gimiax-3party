/*=========================================================================
 
Program:   Medical Imaging & Interaction Toolkit
Language:  C++
Date:      $Date: 2009-05-13 18:06:46 +0200 (mié, 13 may 2009) $
Version:   $Revision: 17258 $
 
Copyright (c) German Cancer Research Center, Division of Medical and
Biological Informatics. All rights reserved.
See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.
 
This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notices for more information.
 
=========================================================================*/

#ifndef MITK_BASEPROPERTYOBSERVER_H_INCLUDED
#define MITK_BASEPROPERTYOBSERVER_H_INCLUDED

#include <itkEventObject.h>
#include "mitkCommon.h"

namespace mitk {

/**
  \brief Convenience class to observe changes of a mitk::BaseProperty.

  This class registers itself as an ITK observer to a BaseProperty and gets
  informed of changes to the property. Whenever such a change occurrs, the virtual
  method PropertyChanged() or PropertyRemoved() is called. This way, derived 
  classes can implement behaviour for more specific properties (e.g. ColorProperty) 
  without the need to reimplement the Subject-Observer handling.

*/

class BaseProperty;

class MITKEXT_CORE_EXPORT PropertyObserver 
{
  public:

    PropertyObserver();
    virtual ~PropertyObserver();
    
    virtual void PropertyChanged() = 0;
    virtual void PropertyRemoved() = 0;

  protected:

    void BeginModifyProperty();
    void EndModifyProperty();

    unsigned long m_ModifiedTag;
    unsigned long m_DeleteTag;

    bool m_SelfCall;
};
  
class MITKEXT_CORE_EXPORT PropertyView : public PropertyObserver
{
  public:

    PropertyView( const mitk::BaseProperty* );
    virtual ~PropertyView();

    void OnModified(const itk::EventObject& e);
    void OnDelete(const itk::EventObject& e);

  protected:

    const mitk::BaseProperty* m_Property;
};

class MITKEXT_CORE_EXPORT PropertyEditor : public PropertyObserver
{
  public:

    PropertyEditor( mitk::BaseProperty* );
    virtual ~PropertyEditor();

    void OnModified(const itk::EventObject& e);
    void OnDelete(const itk::EventObject& e);

  protected:
    
    mitk::BaseProperty* m_Property;
};
  
}

#endif


