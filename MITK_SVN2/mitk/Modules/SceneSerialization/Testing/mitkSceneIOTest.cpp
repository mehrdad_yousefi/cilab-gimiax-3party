/*=========================================================================
 
Program:   Medical Imaging & Interaction Toolkit
Module:    $RCSfile: mitkPropertyManager.cpp,v $
Language:  C++
Date:      $Date$
Version:   $Revision: 1.12 $
 
Copyright (c) German Cancer Research Center, Division of Medical and
Biological Informatics. All rights reserved.
See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.
 
This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notices for more information.
 
=========================================================================*/

#include "mitkTestingMacros.h"

#include "mitkSceneIO.h"

#include "mitkStandaloneDataStorage.h"
#include "mitkStandardFileLocations.h"
#include "mitkDataTreeNodeFactory.h"
#include "mitkCoreObjectFactory.h"
#include "mitkBaseData.h"
#include "mitkImage.h"
#include "mitkSurface.h"
#include "mitkPointSet.h"
#include "Poco/File.h"
#include "Poco/TemporaryFile.h"

class SceneIOTestClass
{
  public:

/// returns full path to a test file
static std::string LocateFile(const std::string& filename)
{
  mitk::StandardFileLocations::Pointer locator = mitk::StandardFileLocations::GetInstance();
  MITK_TEST_CONDITION_REQUIRED(locator.IsNotNull(),"Instantiating StandardFileLocations") 
  return locator->FindFile(filename.c_str(), "Core/Code/Testing/Data");
}

static mitk::BaseData::Pointer LoadBaseData(const std::string& filename)
{
  mitk::DataTreeNodeFactory::Pointer factory = mitk::DataTreeNodeFactory::New();
  try
  {
    factory->SetFileName( filename );
    factory->Update();

    if(factory->GetNumberOfOutputs()<1)
    {
      MITK_TEST_FAILED_MSG(<< "Could not find test data '" << filename << "'");
    }

    mitk::DataTreeNode::Pointer node = factory->GetOutput( 0 );
    return node->GetData();
  }
  catch ( itk::ExceptionObject & e )
  {
    MITK_TEST_FAILED_MSG(<< "Failed loading test data '" << filename << "': " << e.what());
  }
}

static mitk::Image::Pointer LoadImage(const std::string& filename)
{
  mitk::BaseData::Pointer basedata = LoadBaseData( filename );
  mitk::Image::Pointer image = dynamic_cast<mitk::Image*>(basedata.GetPointer());
  if(image.IsNull())
  {
    MITK_TEST_FAILED_MSG(<< "Test image '" << filename << "' was not loaded as an mitk::Image");
  }
  return image;
}

static mitk::Surface::Pointer LoadSurface(const std::string& filename)
{
  mitk::BaseData::Pointer basedata = LoadBaseData( filename );
  mitk::Surface::Pointer surface = dynamic_cast<mitk::Surface*>(basedata.GetPointer());
  if(surface.IsNull())
  {
    MITK_TEST_FAILED_MSG(<< "Test surface '" << filename << "' was not loaded as an mitk::Surface");
  }
  return surface;
}

static mitk::PointSet::Pointer CreatePointSet()
{
  
  mitk::PointSet::Pointer ps = mitk::PointSet::New();
  mitk::PointSet::PointType p;
  mitk::FillVector3D(p, 1.1, -2.22, 33.33);
  ps->SetPoint(0, p);
  mitk::FillVector3D(p, 100.1, -200.22, 3300.33);
  ps->SetPoint(1, p);
  mitk::FillVector3D(p, 2.0, -3.0, 22.22);
  ps->SetPoint(2, p, mitk::PTCORNER); // add point spec
  //mitk::FillVector3D(p, -2.0, -2.0, -2.22);
  //ps->SetPoint(0, p, 1); // ID 0 in timestep 1
  //mitk::FillVector3D(p, -1.0, -1.0, -11.22);
  //ps->SetPoint(1, p, 1); // ID 1 in timestep 1
  //mitk::FillVector3D(p, 1000.0, 1000.0, 1122.22);
  //ps->SetPoint(11, p, mitk::PTCORNER, 2); // ID 11, point spec, timestep 2
  return ps;
}

static void FillStorage(mitk::DataStorage* storage)
{
  mitk::Image::Pointer image = LoadImage( LocateFile("Pic3D.pic.gz") );
  MITK_TEST_CONDITION_REQUIRED(image.IsNotNull(),"Loading test image Pic3D.pic.gz");

  image->SetProperty("image type", mitk::StringProperty::New("test image") );
  image->SetProperty("greetings", mitk::StringProperty::New("to mom") );

  mitk::DataTreeNode::Pointer imagenode = mitk::DataTreeNode::New();
  imagenode->SetData( image );
  imagenode->SetName( "Pic3D" );
  storage->Add( imagenode );
  
  mitk::Surface::Pointer surface = LoadSurface( LocateFile("binary.stl") );
  MITK_TEST_CONDITION_REQUIRED(surface.IsNotNull(),"Loading test surface binary.stl");
  
  surface->SetProperty("surface type", mitk::StringProperty::New("test surface") );
  surface->SetProperty("greetings", mitk::StringProperty::New("to dad") );
  
  mitk::DataTreeNode::Pointer surfacenode = mitk::DataTreeNode::New();
  surfacenode->SetData( surface );
  surfacenode->SetName( "binary" );
  storage->Add( surfacenode );

  mitk::PointSet::Pointer ps = CreatePointSet();
  mitk::DataTreeNode::Pointer psenode = mitk::DataTreeNode::New();
  surfacenode->SetData( ps );
  surfacenode->SetName( "points" );
  storage->Add( psenode );

}

}; // end test helper class
  
int mitkSceneIOTest(int /* argc */, char* /*argv*/[])
{
  MITK_TEST_BEGIN("SceneIO")

  itk::ObjectFactoryBase::RegisterFactory(mitk::CoreObjectFactory::New());
  
  mitk::SceneIO::Pointer sceneIO = mitk::SceneIO::New();
  MITK_TEST_CONDITION_REQUIRED(sceneIO.IsNotNull(),"SceneIO instantiation") 
  
  mitk::DataStorage::Pointer storage = mitk::StandaloneDataStorage::New().GetPointer();
  MITK_TEST_CONDITION_REQUIRED(storage.IsNotNull(),"StandaloneDataStorage instantiation");

  SceneIOTestClass::FillStorage(storage);

  std::string  sceneFileName = Poco::Path::temp() + /*Poco::Path::separator() +*/ "scene.zip";
  MITK_TEST_CONDITION_REQUIRED( sceneIO->SaveScene( storage->GetAll(), storage, sceneFileName), "Saving scene file '" << sceneFileName << "'");

  mitk::SceneIO::FailedBaseDataListType::ConstPointer failedNodes = sceneIO->GetFailedNodes();
  if (failedNodes.IsNotNull())
  {
    MITK_TEST_OUTPUT( << "The following nodes could not be serialized:");
    for ( mitk::SceneIO::FailedBaseDataListType::const_iterator iter = failedNodes->begin();
          iter != failedNodes->end();
          ++iter )
    {
      MITK_TEST_OUTPUT_NO_ENDL( << " - ");
      if ( mitk::BaseData* data =(*iter)->GetData() )
      {
        MITK_TEST_OUTPUT_NO_ENDL( << data->GetNameOfClass());
      }
      else
      {
        MITK_TEST_OUTPUT_NO_ENDL( << "(NULL)");
      }

      MITK_TEST_OUTPUT( << " contained in node '" << (*iter)->GetName() << "'");
      // \TODO: should we fail the test case if failed properties exist?
    }
  }

  mitk::PropertyList::ConstPointer failedProperties = sceneIO->GetFailedProperties();
  if (failedProperties.IsNotNull())
  {
    std::cout << "The following properties could not be serialized:" << std::endl;
    const mitk::PropertyList::PropertyMap* propmap = failedProperties->GetMap();
    for ( mitk::PropertyList::PropertyMap::const_iterator iter = propmap->begin();
          iter != propmap->end();
          ++iter )
    {
      MITK_TEST_OUTPUT( << " - " << iter->second.first->GetNameOfClass() << " associated to key '" << iter->first << "'");
      // \TODO: should we fail the test case if failed properties exist?
    }
  }
  MITK_TEST_END()
}
