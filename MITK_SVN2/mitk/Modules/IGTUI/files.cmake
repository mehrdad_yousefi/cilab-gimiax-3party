SET(CPP_FILES
  Qmitk/QmitkTrackingDeviceWidget.cpp
  Qmitk/QmitkTrackingDeviceConfigurationWidget.cpp
)
SET(UI_FILES
  Qmitk/QmitkTrackingDeviceConfigurationWidgetControls.ui
)

SET(MOC_H_FILES
  Qmitk/QmitkTrackingDeviceWidget.h
  Qmitk/QmitkTrackingDeviceConfigurationWidget.h
)