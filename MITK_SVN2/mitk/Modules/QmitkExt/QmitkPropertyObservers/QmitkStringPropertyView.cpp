/*=========================================================================

Program:   Medical Imaging & Interaction Toolkit
Language:  C++
Date:      $Date: 2008-06-25 00:44:44 +0200 (mié, 25 jun 2008) $
Version:   $Revision: 14642 $ 
 
Copyright (c) German Cancer Research Center, Division of Medical and
Biological Informatics. All rights reserved.
See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notices for more information.

=========================================================================*/
#include "QmitkStringPropertyView.h"

QmitkStringPropertyView::QmitkStringPropertyView( const mitk::StringProperty* property, QWidget* parent )
: QLabel( parent ),
  PropertyView( property ),
  m_StringProperty(property)
{
  PropertyChanged();
}

QmitkStringPropertyView::~QmitkStringPropertyView()
{
}

void QmitkStringPropertyView::PropertyChanged()
{
  if ( m_Property )
    setText( m_StringProperty->GetValue() );
}

void QmitkStringPropertyView::PropertyRemoved()
{
  m_Property = NULL;
  m_StringProperty = NULL;
  setText("n/a");
}

