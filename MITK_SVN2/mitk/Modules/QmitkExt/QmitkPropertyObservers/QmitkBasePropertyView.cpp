/*=========================================================================

Program:   Medical Imaging & Interaction Toolkit
Language:  C++
Date:      $Date: 2008-06-25 00:44:44 +0200 (mié, 25 jun 2008) $
Version:   $Revision: 14642 $ 
 
Copyright (c) German Cancer Research Center, Division of Medical and
Biological Informatics. All rights reserved.
See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notices for more information.

=========================================================================*/
#include "QmitkBasePropertyView.h"

QmitkBasePropertyView::QmitkBasePropertyView( const mitk::BaseProperty* property, QWidget* parent)
: QLabel( parent ),
  PropertyView( property )
{
  PropertyChanged();
}

QmitkBasePropertyView::~QmitkBasePropertyView()
{
}

void QmitkBasePropertyView::PropertyChanged()
{
  if ( m_Property )
    setText( m_Property->GetValueAsString().c_str() );
}

void QmitkBasePropertyView::PropertyRemoved()
{
  m_Property = NULL;
  setText("n/a");
}

