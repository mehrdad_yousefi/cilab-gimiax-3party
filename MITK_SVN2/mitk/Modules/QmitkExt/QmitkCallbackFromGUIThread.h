/*=========================================================================
 
Program:   Medical Imaging & Interaction Toolkit
Language:  C++
Date:      $Date: 2009-05-15 18:09:46 +0200 (vie, 15 may 2009) $
Version:   $Revision: 17280 $
 
Copyright (c) German Cancer Research Center, Division of Medical and
Biological Informatics. All rights reserved.
See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.
 
This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notices for more information.
 
=========================================================================*/

#ifndef QMITK_CALLBACK_WITHIN_GUI_TREAD_H_INCLUDGEWQ
#define QMITK_CALLBACK_WITHIN_GUI_TREAD_H_INCLUDGEWQ

#include "mitkCallbackFromGUIThread.h"

#include <QObject>

/*!
  \brief Qt specific implementation of mitk::CallbackFromGUIThreadImplementation
*/
class QMITKEXT_EXPORT QmitkCallbackFromGUIThread : public QObject, public mitk::CallbackFromGUIThreadImplementation
{

  Q_OBJECT

  public:
    
    /// Change the current application cursor
    virtual void CallThisFromGUIThread(itk::Command*, itk::EventObject*);

    QmitkCallbackFromGUIThread();
    virtual ~QmitkCallbackFromGUIThread();

    virtual bool event( QEvent* e );

  protected:
  private:
};
  
#endif

