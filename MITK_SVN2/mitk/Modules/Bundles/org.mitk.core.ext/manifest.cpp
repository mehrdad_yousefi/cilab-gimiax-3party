/*=========================================================================

Program:   MITK Platform
Language:  C++
Date:      $Date: 2009-05-18 17:39:41 +0200 (lun, 18 may 2009) $
Version:   $Revision: 17020 $

Copyright (c) German Cancer Research Center, Division of Medical and
Biological Informatics. All rights reserved.
See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notices for more information.

=========================================================================*/

#include <Poco/ClassLibrary.h>

#include <cherryIBundleActivator.h>
#include "src/internal/mitkCoreExtActivator.h"

POCO_BEGIN_MANIFEST(cherry::IBundleActivator)
  POCO_EXPORT_CLASS(mitk::CoreExtActivator)
POCO_END_MANIFEST
