/*=========================================================================

 Program:   openCherry Platform
 Language:  C++
 Date:      $Date: 2009-05-18 17:39:41 +0200 (lun, 18 may 2009) $
 Version:   $Revision: 17020 $

 Copyright (c) German Cancer Research Center, Division of Medical and
 Biological Informatics. All rights reserved.
 See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.

 This software is distributed WITHOUT ANY WARRANTY; without even
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the above copyright notices for more information.

 =========================================================================*/

#include "mitkCoreExtActivator.h"

#include <mitkCoreExtObjectFactory.h>

namespace mitk
{

void
CoreExtActivator::Start(cherry::IBundleContext::Pointer /*context*/)
{
  RegisterCoreExtObjectFactory();
}

}
