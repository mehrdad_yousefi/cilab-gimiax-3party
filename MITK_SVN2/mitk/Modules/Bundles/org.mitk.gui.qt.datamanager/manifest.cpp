/*=========================================================================

Program:   Medical Imaging & Interaction Toolkit
Language:  C++
Date:      $Date: 2009-10-16 15:40:45 +0200 (vie, 16 oct 2009) $
Version:   $Revision: 19512 $

Copyright (c) German Cancer Research Center, Division of Medical and
Biological Informatics. All rights reserved.
See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notices for more information.

=========================================================================*/

#include <Poco/ClassLibrary.h>

#include <cherryIViewPart.h>
#include "src/internal/QmitkPropertyListView.h"
#include "src/QmitkDataManagerView.h"
#include "src/QmitkDataManagerPreferencePage.h"
#include "src/QmitkDataManagerHotkeysPrefPage.h"

POCO_BEGIN_NAMED_MANIFEST(cherryIViewPart, cherry::IViewPart)
  POCO_EXPORT_CLASS(QmitkPropertyListView)
  POCO_EXPORT_CLASS(QmitkDataManagerView)
POCO_END_MANIFEST

POCO_BEGIN_NAMED_MANIFEST(cherryIPreferencePage, cherry::IPreferencePage)
  POCO_EXPORT_CLASS(QmitkDataManagerPreferencePage)
  POCO_EXPORT_CLASS(QmitkDataManagerHotkeysPrefPage)
POCO_END_MANIFEST
