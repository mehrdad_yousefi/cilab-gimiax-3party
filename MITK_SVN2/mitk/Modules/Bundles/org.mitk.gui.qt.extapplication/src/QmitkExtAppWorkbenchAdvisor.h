/*=========================================================================

 Program:   Medical Imaging & Interaction Toolkit
 Language:  C++
 Date:      $Date: 2009-10-23 02:14:03 +0200 (vie, 23 oct 2009) $
 Version:   $Revision: 19649 $

 Copyright (c) German Cancer Research Center, Division of Medical and
 Biological Informatics. All rights reserved.
 See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.

 This software is distributed WITHOUT ANY WARRANTY; without even
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the above copyright notices for more information.

 =========================================================================*/

#ifndef QMITKEXTAPPWORKBENCHADVISOR_H_
#define QMITKEXTAPPWORKBENCHADVISOR_H_

#ifdef __MINGW32__
// We need to inlclude winbase.h here in order to declare
// atomic intrinsics like InterlockedIncrement correctly.
// Otherwhise, they would be declared wrong within qatomic_windows.h .
#include <windows.h>
#endif

#include <cherryQtWorkbenchAdvisor.h>

#include "mitkQtExtAppDll.h"

class MITK_QT_EXTAPP QmitkExtAppWorkbenchAdvisor: public cherry::QtWorkbenchAdvisor
{
public:

  static const std::string DEFAULT_PERSPECTIVE_ID; // = "org.mitk.extapp.defaultperspective"

  void Initialize(cherry::IWorkbenchConfigurer::Pointer configurer);

  cherry::WorkbenchWindowAdvisor* CreateWorkbenchWindowAdvisor(
      cherry::IWorkbenchWindowConfigurer::Pointer configurer);

  std::string GetInitialWindowPerspectiveId();

};

#endif /*QMITKEXTAPPWORKBENCHADVISOR_H_*/
