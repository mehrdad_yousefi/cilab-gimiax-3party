/*=========================================================================

Program:   Medical Imaging & Interaction Toolkit
Language:  C++
Date:      $Date: 2009-10-23 02:14:03 +0200 (vie, 23 oct 2009) $
Version:   $Revision: 19649 $

Copyright (c) German Cancer Research Center, Division of Medical and
Biological Informatics. All rights reserved.
See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notices for more information.

=========================================================================*/

#ifndef QMITKEXTACTIONBARADVISOR_H_
#define QMITKEXTACTIONBARADVISOR_H_

#include <cherryActionBarAdvisor.h>

#include "mitkQtCommonExtDll.h"

class MITK_QT_COMMON_EXT_EXPORT QmitkExtActionBarAdvisor : public cherry::ActionBarAdvisor
{
public:

  QmitkExtActionBarAdvisor(cherry::IActionBarConfigurer::Pointer configurer);

protected:

  void FillMenuBar(void* menuBar);
};

#endif /*QMITKEXTACTIONBARADVISOR_H_*/
