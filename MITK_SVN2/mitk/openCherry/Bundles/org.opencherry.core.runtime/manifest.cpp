/*=========================================================================

Program:   openCherry Platform
Language:  C++
Date:      $Date: 2008-12-24 15:39:29 +0100 (mié, 24 dic 2008) $
Version:   $Revision: 16000 $

Copyright (c) German Cancer Research Center, Division of Medical and
Biological Informatics. All rights reserved.
See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notices for more information.

=========================================================================*/

#include "Poco/ClassLibrary.h"

#include <cherryIBundleActivator.h>
#include "src/cherryRuntimePlugin.h"

POCO_BEGIN_MANIFEST(cherry::IBundleActivator)
  POCO_EXPORT_CLASS(cherry::RuntimePlugin)
POCO_END_MANIFEST
