/*=========================================================================

Program:   openCherry Platform
Language:  C++
Date:      $Date: 2009-06-01 15:50:41 +0200 (lun, 01 jun 2009) $
Version:   $Revision: 17533 $

Copyright (c) German Cancer Research Center, Division of Medical and
Biological Informatics. All rights reserved.
See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notices for more information.

=========================================================================*/

#ifndef CHERRYRUNTIMEDLL_H_
#define CHERRYRUNTIMEDLL_H_


//
// The following block is the standard way of creating macros which make exporting
// from a DLL simpler. All files within this DLL are compiled with the org_opencherry_core_runtime_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see
// org_opencherry_core_runtime_EXPORTS functions as being imported from a DLL, wheras this DLL sees symbols
// defined with this macro as being exported.
//
#if defined(_WIN32) && !defined(CHERRY_STATIC)
  #if defined(org_opencherry_core_runtime_EXPORTS)
    #define CHERRY_RUNTIME __declspec(dllexport)
  #else
    #define CHERRY_RUNTIME __declspec(dllimport)
  #endif
#endif


#if !defined(CHERRY_RUNTIME)
  #define CHERRY_RUNTIME
#endif

#endif /*CHERRYRUNTIMEDLL_H_*/
