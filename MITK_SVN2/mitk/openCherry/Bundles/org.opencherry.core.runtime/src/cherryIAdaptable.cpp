/*=========================================================================

Program:   openCherry Platform
Language:  C++
Date:      $Date: 2009-04-14 17:51:34 +0200 (mar, 14 abr 2009) $
Version:   $Revision: 16014 $

Copyright (c) German Cancer Research Center, Division of Medical and
Biological Informatics. All rights reserved.
See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notices for more information.

=========================================================================*/

#include "cherryIAdaptable.h"

namespace cherry {

IAdaptable::~IAdaptable()
{
}

}
