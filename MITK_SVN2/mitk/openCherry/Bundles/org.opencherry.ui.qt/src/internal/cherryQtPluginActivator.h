/*=========================================================================
 
 Program:   openCherry Platform
 Language:  C++
 Date:      $Date: 2009-10-19 19:48:49 +0200 (lun, 19 oct 2009) $
 Version:   $Revision: 19546 $
 
 Copyright (c) German Cancer Research Center, Division of Medical and
 Biological Informatics. All rights reserved.
 See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.
 
 This software is distributed WITHOUT ANY WARRANTY; without even
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the above copyright notices for more information.
 
 =========================================================================*/


#ifndef CHERRYQTPLUGINACTIVATOR_H_
#define CHERRYQTPLUGINACTIVATOR_H_

#include <cherryAbstractUIPlugin.h>

namespace cherry {

class QtPluginActivator : public AbstractUIPlugin
{
public:

  void Start(IBundleContext::Pointer context);
};

}

#endif /* CHERRYQTPLUGINACTIVATOR_H_ */
