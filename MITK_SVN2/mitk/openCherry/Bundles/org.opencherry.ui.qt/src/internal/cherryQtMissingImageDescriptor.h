/*=========================================================================

 Program:   openCherry Platform
 Language:  C++
 Date:      $Date: 2009-03-21 19:23:33 +0100 (sáb, 21 mar 2009) $
 Version:   $Revision: 16712 $

 Copyright (c) German Cancer Research Center, Division of Medical and
 Biological Informatics. All rights reserved.
 See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.

 This software is distributed WITHOUT ANY WARRANTY; without even
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the above copyright notices for more information.

 =========================================================================*/


#ifndef CHERRYQTMISSINGIMAGEDESCRIPTOR_H_
#define CHERRYQTMISSINGIMAGEDESCRIPTOR_H_

#include <cherryImageDescriptor.h>

class QIcon;

namespace cherry {

class QtMissingImageDescriptor : public ImageDescriptor
{

private:

public:

  virtual void* CreateImage(bool returnMissingImageOnError = true);

  virtual void DestroyImage(void* img);

  virtual bool operator==(const Object* o) const;

};

}

#endif /* CHERRYQTICONIMAGEDESCRIPTOR_H_ */
