/*=========================================================================

Program:   openCherry Platform
Language:  C++
Date:      $Date: 2009-10-09 16:36:23 +0200 (vie, 09 oct 2009) $
Version:   $Revision: 19414 $

Copyright (c) German Cancer Research Center, Division of Medical and
Biological Informatics. All rights reserved.
See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notices for more information.

=========================================================================*/

#ifndef CHERRYQTSHOWVIEWDIALOG_H_
#define CHERRYQTSHOWVIEWDIALOG_H_

#ifdef __MINGW32__
// We need to inlclude winbase.h here in order to declare
// atomic intrinsics like InterlockedIncrement correctly.
// Otherwhise, they would be declared wrong within qatomic_windows.h .
#include <windows.h>
#endif

#include <QDialog>

#include <cherryIViewRegistry.h>
#include <cherryIShowViewDialog.h>

#include "ui_cherryQtShowViewDialog.h"

namespace cherry {

class QtShowViewDialog : public QDialog, public IShowViewDialog
{
public:
  cherryObjectMacro(QtShowViewDialog);

  QtShowViewDialog(IViewRegistry* registry, QWidget* parent = 0, Qt::WindowFlags f = 0);

  std::vector<IViewDescriptor::Pointer> GetSelection();
  int Open();

protected:

  Ui::QtShowViewDialog_ m_UserInterface;
};

}

#endif /*CHERRYQTSHOWVIEWDIALOG_H_*/
