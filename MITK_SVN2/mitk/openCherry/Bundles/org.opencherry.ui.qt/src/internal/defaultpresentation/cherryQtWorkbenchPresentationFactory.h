/*=========================================================================

 Program:   openCherry Platform
 Language:  C++
 Date:      $Date: 2009-10-21 10:19:43 +0200 (mié, 21 oct 2009) $
 Version:   $Revision: 19598 $

 Copyright (c) German Cancer Research Center, Division of Medical and
 Biological Informatics. All rights reserved.
 See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.

 This software is distributed WITHOUT ANY WARRANTY; without even
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the above copyright notices for more information.

 =========================================================================*/

#ifndef CHERRYQTWORKBENCHPRESENTATIONFACTORY_H_
#define CHERRYQTWORKBENCHPRESENTATIONFACTORY_H_

#include <cherryIPresentationFactory.h>

namespace cherry
{

/**
 * The default presentation factory for the Workbench.
 *
 */
class QtWorkbenchPresentationFactory: public IPresentationFactory
{

  //  // don't reset these dynamically, so just keep the information static.
  //  // see bug:
  //  // 75422 [Presentations] Switching presentation to R21 switches immediately,
  //  // but only partially
  //  private static int editorTabPosition = PlatformUI.getPreferenceStore()
  //      .getInt(IWorkbenchPreferenceConstants.EDITOR_TAB_POSITION);
  //  private static int viewTabPosition = PlatformUI.getPreferenceStore()
  //      .getInt(IWorkbenchPreferenceConstants.VIEW_TAB_POSITION);

public:

  /*
   * (non-Javadoc)
   *
   * @see org.opencherry.ui.presentations.AbstractPresentationFactory#createEditorPresentation(org.opencherry.swt.widgets.Composite,
   *      org.opencherry.ui.presentations.IStackPresentationSite)
   */
  StackPresentation::Pointer CreateEditorPresentation(void* parent,
      IStackPresentationSite::Pointer site);

  /*
   * (non-Javadoc)
   *
   * @see org.opencherry.ui.presentations.AbstractPresentationFactory#createViewPresentation(org.opencherry.swt.widgets.Composite,
   *      org.opencherry.ui.presentations.IStackPresentationSite)
   */
  StackPresentation::Pointer CreateViewPresentation(void* parent,
      IStackPresentationSite::Pointer site);

  /*
   * (non-Javadoc)
   *
   * @see org.opencherry.ui.presentations.AbstractPresentationFactory#createStandaloneViewPresentation(org.opencherry.swt.widgets.Composite,
   *      org.opencherry.ui.presentations.IStackPresentationSite, boolean)
   */
  StackPresentation::Pointer CreateStandaloneViewPresentation(void* parent,
      IStackPresentationSite::Pointer site, bool showTitle);

  std::string GetId();

  void* CreateSash(void* parent, int style);

  int GetSashSize(int style);

  void UpdateTheme();

};

}

#endif /* CHERRYQTWORKBENCHPRESENTATIONFACTORY_H_ */
