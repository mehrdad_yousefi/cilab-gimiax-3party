/*=========================================================================

 Program:   openCherry Platform
 Language:  C++
 Date:      $Date: 2008-11-01 17:59:53 +0100 (sáb, 01 nov 2008) $
 Version:   $Revision: 15615 $

 Copyright (c) German Cancer Research Center, Division of Medical and
 Biological Informatics. All rights reserved.
 See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.

 This software is distributed WITHOUT ANY WARRANTY; without even
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the above copyright notices for more information.

 =========================================================================*/


#ifndef CHERRYREPLACEDRAGHANDLER_H_
#define CHERRYREPLACEDRAGHANDLER_H_

#include "cherryTabDragHandler.h"

#include "cherryAbstractTabFolder.h"

namespace cherry {

class ReplaceDragHandler : public TabDragHandler {

private:

  struct DragCookie : public Object {
        int insertPosition;

        DragCookie(int pos);
    };

    AbstractTabFolder* tabFolder;

  public:

    ReplaceDragHandler(AbstractTabFolder* folder);

    /* (non-Javadoc)
     * @see org.eclipse.ui.internal.presentations.util.TabDragHandler#dragOver(org.eclipse.swt.widgets.Control, org.eclipse.swt.graphics.Point)
     */
    StackDropResult::Pointer DragOver(QWidget* currentControl, const Point& location,
            int dragStart);

    /* (non-Javadoc)
     * @see org.eclipse.ui.internal.presentations.util.TabDragHandler#getInsertionPosition(java.lang.Object)
     */
    int GetInsertionPosition(Object::Pointer cookie);

};

}

#endif /* CHERRYREPLACEDRAGHANDLER_H_ */
