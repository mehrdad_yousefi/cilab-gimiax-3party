/*=========================================================================

Program:   openCherry Platform
Language:  C++
Date:      $Date: 2009-10-22 00:45:04 +0200 (jue, 22 oct 2009) $
Version:   $Revision: 19612 $

Copyright (c) German Cancer Research Center, Division of Medical and
Biological Informatics. All rights reserved.
See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notices for more information.

=========================================================================*/

#ifndef CHERRYQTWORKBENCHTWEAKLET_H_
#define CHERRYQTWORKBENCHTWEAKLET_H_

#include <cherryWorkbenchTweaklet.h>

#include "../cherryUiQtDll.h"

namespace cherry {

class CHERRY_UI_QT QtWorkbenchTweaklet : public WorkbenchTweaklet
{

public:

  cherryObjectMacro(QtWorkbenchTweaklet);

  Display* CreateDisplay();

  bool IsRunning();

  SmartPointer<WorkbenchWindow> CreateWorkbenchWindow(int number);

  void* CreatePageComposite(void* parent);

  IDialog::Pointer CreateStandardDialog(const std::string& id);

};

} // namespace cherry

#endif /*CHERRYQTWORKBENCHTWEAKLET_H_*/
