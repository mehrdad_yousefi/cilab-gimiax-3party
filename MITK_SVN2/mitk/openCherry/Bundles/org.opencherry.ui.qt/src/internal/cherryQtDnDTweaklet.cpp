/*=========================================================================

 Program:   openCherry Platform
 Language:  C++
 Date:      $Date: 2008-11-01 17:59:53 +0100 (sáb, 01 nov 2008) $
 Version:   $Revision: 15615 $

 Copyright (c) German Cancer Research Center, Division of Medical and
 Biological Informatics. All rights reserved.
 See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.

 This software is distributed WITHOUT ANY WARRANTY; without even
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the above copyright notices for more information.

 =========================================================================*/

#include "cherryQtDnDTweaklet.h"

#include "cherryQtTracker.h"

namespace cherry {

ITracker* QtDnDTweaklet::CreateTracker()
{
  return new QtTracker();
}

}
