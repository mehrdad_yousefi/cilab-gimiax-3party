/*=========================================================================

 Program:   openCherry Platform
 Language:  C++
 Date:      $Date: 2009-04-27 17:09:12 +0200 (lun, 27 abr 2009) $
 Version:   $Revision: 16987 $

 Copyright (c) German Cancer Research Center, Division of Medical and
 Biological Informatics. All rights reserved.
 See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.

 This software is distributed WITHOUT ANY WARRANTY; without even
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the above copyright notices for more information.

 =========================================================================*/


#ifndef CHERRYQTGLOBALEVENTFILTER_H_
#define CHERRYQTGLOBALEVENTFILTER_H_

#include <QObject>

class QEvent;

namespace cherry {

class QtGlobalEventFilter : public QObject
{
  Q_OBJECT

public:

  QtGlobalEventFilter(QObject* parent = 0);

  bool eventFilter(QObject* obj, QEvent* event);
};

}

#endif /* CHERRYQTGLOBALEVENTFILTER_H_ */
