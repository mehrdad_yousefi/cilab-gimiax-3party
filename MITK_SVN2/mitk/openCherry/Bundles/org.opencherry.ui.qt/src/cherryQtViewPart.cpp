/*=========================================================================
 
Program:   openCherry Platform
Language:  C++
Date:      $Date: 2009-02-13 14:26:11 +0100 (vie, 13 feb 2009) $
Version:   $Revision: 16274 $
 
Copyright (c) German Cancer Research Center, Division of Medical and
Biological Informatics. All rights reserved.
See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.
 
This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notices for more information.
 
=========================================================================*/

#include "cherryQtViewPart.h"

namespace cherry
{

void QtViewPart::CreatePartControl(void* parent)
{
  this->CreateQtPartControl(static_cast<QWidget*>(parent));
}

}
