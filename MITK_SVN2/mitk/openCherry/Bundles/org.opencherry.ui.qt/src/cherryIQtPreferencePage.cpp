/*=========================================================================
 
 Program:   openCherry Platform
 Language:  C++
 Date:      $Date: 2009-10-16 15:40:45 +0200 (vie, 16 oct 2009) $
 Version:   $Revision: 19512 $
 
 Copyright (c) German Cancer Research Center, Division of Medical and
 Biological Informatics. All rights reserved.
 See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.
 
 This software is distributed WITHOUT ANY WARRANTY; without even
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the above copyright notices for more information.
 
 =========================================================================*/

#include "cherryIQtPreferencePage.h"

namespace cherry {

void IQtPreferencePage::CreateControl(void* parent)
{
  this->CreateQtControl(static_cast<QWidget*>(parent));
}

void* IQtPreferencePage::GetControl() const
{
  return (void*)this->GetQtControl();
}

}
