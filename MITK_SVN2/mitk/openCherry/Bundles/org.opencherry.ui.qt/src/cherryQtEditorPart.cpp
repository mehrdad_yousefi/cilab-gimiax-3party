/*=========================================================================
 
Program:   openCherry Platform
Language:  C++
Date:      $Date: 2008-12-24 15:39:29 +0100 (mié, 24 dic 2008) $
Version:   $Revision: 16000 $
 
Copyright (c) German Cancer Research Center, Division of Medical and
Biological Informatics. All rights reserved.
See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.
 
This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notices for more information.
 
=========================================================================*/

#include "cherryQtEditorPart.h"

namespace cherry
{

void QtEditorPart::CreatePartControl(void* parent)
{
  this->CreateQtPartControl(static_cast<QWidget*>(parent));
}

}
