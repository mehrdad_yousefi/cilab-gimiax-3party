/*=========================================================================
 
Program:   openCherry Platform
Language:  C++
Date:      $Date: 2009-10-09 16:36:23 +0200 (vie, 09 oct 2009) $
Version:   $Revision: 19414 $
 
Copyright (c) German Cancer Research Center, Division of Medical and
Biological Informatics. All rights reserved.
See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.
 
This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notices for more information.
 
=========================================================================*/

#ifdef __MINGW32__
// We need to inlclude winbase.h here in order to declare
// atomic intrinsics like InterlockedIncrement correctly.
// Otherwhise, they would be declared wrong within qatomic_windows.h .
#include <windows.h>
#endif

#include "cherryLogView.h"

#include "cherryQtLogView.h"

#include <QHBoxLayout>

namespace cherry {

void LogView::CreateQtPartControl(QWidget* parent)
{
  QHBoxLayout* layout = new QHBoxLayout(parent);
  layout->setContentsMargins(0,0,0,0);
  QtLogView* logView = new QtLogView(parent);
  layout->addWidget(logView);
}

void LogView::SetFocus()
{
  
}

}
