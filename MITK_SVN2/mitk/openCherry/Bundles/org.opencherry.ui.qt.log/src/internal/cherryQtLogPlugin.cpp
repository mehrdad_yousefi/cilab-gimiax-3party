/*=========================================================================
 
Program:   openCherry Platform
Language:  C++
Date:      $Date: 2009-08-04 15:41:33 +0200 (mar, 04 ago 2009) $
Version:   $Revision: 18434 $
 
Copyright (c) German Cancer Research Center, Division of Medical and
Biological Informatics. All rights reserved.
See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.
 
This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notices for more information.
 
=========================================================================*/

#include "cherryQtLogPlugin.h"

namespace cherry {

QtLogPlugin* QtLogPlugin::instance = 0;

QtLogPlugin::QtLogPlugin()
{
  instance = this;
}

void 
QtLogPlugin::Start(IBundleContext::Pointer /*context*/)
{
  m_LogModel = new QtPlatformLogModel();
}

void 
QtLogPlugin::Stop(IBundleContext::Pointer /*context*/)
{
  delete m_LogModel;
}
  
QtLogPlugin*
QtLogPlugin::GetInstance()
{
  return instance;
}

QtPlatformLogModel* 
QtLogPlugin::GetLogModel()
{
  return m_LogModel;
}

}
