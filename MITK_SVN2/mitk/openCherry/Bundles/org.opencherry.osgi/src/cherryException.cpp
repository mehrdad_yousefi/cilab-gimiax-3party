/*=========================================================================

 Program:   openCherry Platform
 Language:  C++
 Date:      $Date: 2009-02-06 16:02:58 +0100 (vie, 06 feb 2009) $
 Version:   $Revision: 16207 $

 Copyright (c) German Cancer Research Center, Division of Medical and
 Biological Informatics. All rights reserved.
 See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.

 This software is distributed WITHOUT ANY WARRANTY; without even
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the above copyright notices for more information.

 =========================================================================*/

#include "cherryException.h"

#include <typeinfo>

namespace cherry {

POCO_IMPLEMENT_EXCEPTION(BadWeakPointerException, Poco::Exception, "Bad WeakPointer exception")

}

