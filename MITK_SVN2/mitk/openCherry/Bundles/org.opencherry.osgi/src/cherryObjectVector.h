/*=========================================================================

Program:   openCherry Platform
Language:  C++
Date:      $Date: 2009-06-07 15:05:24 +0200 (dom, 07 jun 2009) $
Version:   $Revision: 17646 $

Copyright (c) German Cancer Research Center, Division of Medical and
Biological Informatics. All rights reserved.
See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notices for more information.

=========================================================================*/

#ifndef CHERRYOSGIOBJECTVECTOR_H_
#define CHERRYOSGIOBJECTVECTOR_H_

#include "cherryMacros.h"

#include <vector>

namespace cherry {

template<typename T>
class ObjectVector : public Object, public std::vector<T>
{
public:
  cherryObjectMacro(ObjectVector<T>);

  bool operator==(const Object* obj) const
  {
    if (const ObjectVector* other = dynamic_cast<const ObjectVector*>(obj))
      static_cast<const std::vector<T> &>(*this) == static_cast<const std::vector<T>& >(*other);

    return false;
  }
};

}

#endif /*CHERRYOSGIOBJECTVECTOR_H_*/
