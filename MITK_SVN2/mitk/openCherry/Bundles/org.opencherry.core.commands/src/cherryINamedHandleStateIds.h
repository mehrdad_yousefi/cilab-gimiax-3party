/*=========================================================================

 Program:   openCherry Platform
 Language:  C++
 Date:      $Date: 2009-03-21 15:02:40 +0100 (sáb, 21 mar 2009) $
 Version:   $Revision: 16712 $

 Copyright (c) German Cancer Research Center, Division of Medical and
 Biological Informatics. All rights reserved.
 See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.

 This software is distributed WITHOUT ANY WARRANTY; without even
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the above copyright notices for more information.

 =========================================================================*/


#ifndef CHERRYINAMEDHANDLESTATEIDS_H_
#define CHERRYINAMEDHANDLESTATEIDS_H_

#include <string>

namespace cherry {

/**
 * <p>
 * State identifiers that are understood by named handle objects that implement
 * {@link IObjectWithState}.
 * </p>
 * <p>
 * Clients may implement or extend this class.
 * </p>
 *
 */
struct INamedHandleStateIds {

  /**
   * The state id used for overriding the description of a named handle
   * object. This state's value must return a {@link String}.
   */
  static const std::string DESCRIPTION; // = "DESCRIPTION";

  /**
   * The state id used for overriding the name of a named handle object. This
   * state's value must return a {@link String}.
   */
  static const std::string NAME; // = "NAME";
};

}

#endif /* CHERRYINAMEDHANDLESTATEIDS_H_ */
