/*=========================================================================

 Program:   openCherry Platform
 Language:  C++
 Date:      $Date: 2009-07-08 13:02:46 +0200 (mié, 08 jul 2009) $
 Version:   $Revision: 18037 $

 Copyright (c) German Cancer Research Center, Division of Medical and
 Biological Informatics. All rights reserved.
 See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.

 This software is distributed WITHOUT ANY WARRANTY; without even
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the above copyright notices for more information.

 =========================================================================*/

#include "cherryLog.h"

#include "cherryCommandTracing.h"

#include <iostream>

namespace cherry {

const std::string CommandTracing::SEPARATOR = " >>> ";

void CommandTracing::PrintTrace(const std::string& component,
    const std::string& message)
{
  std::string buffer(component);
  if ((!component.empty()) && (!message.empty()))
  {
    buffer += SEPARATOR;
  }
  if (!message.empty())
  {
    buffer += message;
  }
  CHERRY_INFO << buffer;
}

CommandTracing::CommandTracing()
{
  // Do nothing.
}

}
