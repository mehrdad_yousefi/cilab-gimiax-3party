/*=========================================================================

 Program:   openCherry Platform
 Language:  C++
 Date:      $Date: 2009-03-21 15:02:40 +0100 (sáb, 21 mar 2009) $
 Version:   $Revision: 16712 $

 Copyright (c) German Cancer Research Center, Division of Medical and
 Biological Informatics. All rights reserved.
 See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.

 This software is distributed WITHOUT ANY WARRANTY; without even
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the above copyright notices for more information.

 =========================================================================*/


#ifndef CHERRYITYPEDPARAMETER_H_
#define CHERRYITYPEDPARAMETER_H_

#include <cherryObject.h>
#include <cherryMacros.h>

#include "cherryCommandsDll.h"

namespace cherry {

class ParameterType;

/**
 * A command parameter that has a declared type. This interface is intended to
 * be implemented by implementors of {@link IParameter} that will support
 * parameter types.
 *
 */
struct CHERRY_COMMANDS ITypedParameter : public virtual Object {

  cherryInterfaceMacro(ITypedParameter, cherry)

  /**
   * Returns the {@link ParameterType} associated with a command parameter or
   * <code>null</code> if the parameter does not declare a type.
   * <p>
   * Note that the parameter type returned may be undefined.
   * </p>
   *
   * @return the parameter type associated with a command parameter or
   *         <code>null</code> if the parameter does not declare a type
   */
  virtual SmartPointer<ParameterType> GetParameterType() = 0;
};

}

#endif /* CHERRYITYPEDPARAMETER_H_ */
