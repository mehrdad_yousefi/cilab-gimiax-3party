/*=========================================================================

 Program:   openCherry Platform
 Language:  C++
 Date:      $Date: 2009-03-21 15:02:40 +0100 (sáb, 21 mar 2009) $
 Version:   $Revision: 16712 $

 Copyright (c) German Cancer Research Center, Division of Medical and
 Biological Informatics. All rights reserved.
 See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.

 This software is distributed WITHOUT ANY WARRANTY; without even
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the above copyright notices for more information.

 =========================================================================*/

#include "cherryICommandCategoryListener.h"

#include "cherryCommandCategory.h"
#include "cherryCommandCategoryEvent.h"

namespace cherry {

void
ICommandCategoryListener::Events
::AddListener(ICommandCategoryListener::Pointer l)
{
  if (l.IsNull()) return;

  categoryChanged += Delegate(l.GetPointer(), &ICommandCategoryListener::CategoryChanged);
}

void
ICommandCategoryListener::Events
::RemoveListener(ICommandCategoryListener::Pointer l)
{
  if (l.IsNull()) return;

  categoryChanged -= Delegate(l.GetPointer(), &ICommandCategoryListener::CategoryChanged);
}

}


