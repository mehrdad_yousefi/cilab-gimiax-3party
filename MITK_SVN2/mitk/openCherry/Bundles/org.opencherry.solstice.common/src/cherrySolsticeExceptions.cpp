/*=========================================================================
 
 Program:   openCherry Platform
 Language:  C++
 Date:      $Date: 2009-09-24 14:27:31 +0200 (jue, 24 sep 2009) $
 Version:   $Revision: 19128 $
 
 Copyright (c) German Cancer Research Center, Division of Medical and
 Biological Informatics. All rights reserved.
 See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.
 
 This software is distributed WITHOUT ANY WARRANTY; without even
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the above copyright notices for more information.
 
 =========================================================================*/

#include "cherrySolsticeExceptions.h"

#include <typeinfo>

namespace cherry {

POCO_IMPLEMENT_EXCEPTION(OperationCanceledException, Poco::RuntimeException, "Operation canceled exception");

}
