/*=========================================================================
 
Program:   openCherry Platform
Language:  C++
Date:      $Date: 2008-12-24 15:39:29 +0100 (mié, 24 dic 2008) $
Version:   $Revision: 16000 $
 
Copyright (c) German Cancer Research Center, Division of Medical and
Biological Informatics. All rights reserved.
See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.
 
This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notices for more information.
 
=========================================================================*/

#include "cherryExpressionTagNames.h"

namespace cherry {

const std::string ExpressionTagNames::ENABLEMENT= "enablement"; //$NON-NLS-1$
const std::string ExpressionTagNames::AND= "and"; //$NON-NLS-1$
const std::string ExpressionTagNames::OR= "or"; //$NON-NLS-1$
const std::string ExpressionTagNames::NOT= "not"; //$NON-NLS-1$
const std::string ExpressionTagNames::INSTANCEOF= "instanceof"; //$NON-NLS-1$
const std::string ExpressionTagNames::TEST= "test"; //$NON-NLS-1$
const std::string ExpressionTagNames::WITH= "with"; //$NON-NLS-1$
const std::string ExpressionTagNames::ADAPT= "adapt"; //$NON-NLS-1$
const std::string ExpressionTagNames::COUNT= "count"; //$NON-NLS-1$
const std::string ExpressionTagNames::ITERATE= "iterate"; //$NON-NLS-1$
const std::string ExpressionTagNames::RESOLVE= "resolve"; //$NON-NLS-1$
const std::string ExpressionTagNames::SYSTEM_TEST= "systemTest"; //$NON-NLS-1$
const std::string ExpressionTagNames::EQUALS= "equals"; //$NON-NLS-1$
const std::string ExpressionTagNames::REFERENCE= "reference"; //$NON-NLS-1$

}
