/*=========================================================================

 Program:   openCherry Platform
 Language:  C++
 Date:      $Date: 2009-10-05 17:10:08 +0200 (lun, 05 oct 2009) $
 Version:   $Revision: 15350 $

 Copyright (c) German Cancer Research Center, Division of Medical and
 Biological Informatics. All rights reserved.
 See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.

 This software is distributed WITHOUT ANY WARRANTY; without even
 PURPOSE.  See the above copyright notices for more information.

 =========================================================================*/

#include "cherryIJobManager.h"

namespace cherry
{

const std::string IJobManager::PROP_USE_DAEMON_THREADS = "opencherry.jobs.daemon";

}

