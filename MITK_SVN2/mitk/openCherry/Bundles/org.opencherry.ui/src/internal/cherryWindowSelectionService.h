/*=========================================================================
 
 Program:   openCherry Platform
 Language:  C++
 Date:      $Date: 2009-09-29 15:34:52 +0200 (mar, 29 sep 2009) $
 Version:   $Revision: 19224 $
 
 Copyright (c) German Cancer Research Center, Division of Medical and
 Biological Informatics. All rights reserved.
 See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.
 
 This software is distributed WITHOUT ANY WARRANTY; without even
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the above copyright notices for more information.
 
 =========================================================================*/


#ifndef CHERRYWINDOWSELECTIONSERVICE_H_
#define CHERRYWINDOWSELECTIONSERVICE_H_

#include "cherryAbstractSelectionService.h"

namespace cherry {

struct IWorkbenchWindow;

/**
 * The selection service for a window.
 */
/* package */
class WindowSelectionService : public AbstractSelectionService {

private:

    /**
     * The window.
     */
    SmartPointer<IWorkbenchWindow> window;

     /**
     * Sets the window.
     */
    void SetWindow(SmartPointer<IWorkbenchWindow> window);

protected:

    /**
     * Returns the window.
     */
    SmartPointer<IWorkbenchWindow> GetWindow() const;

    /*
     * @see AbstractSelectionService#createPartTracker(String)
     */
    AbstractPartSelectionTracker::Pointer CreatePartTracker(const std::string& partId) const;

public:

    /**
     * Creates a new selection service for the given window.
     */
    WindowSelectionService(SmartPointer<IWorkbenchWindow> window);

};

}

#endif /* CHERRYWINDOWSELECTIONSERVICE_H_ */
