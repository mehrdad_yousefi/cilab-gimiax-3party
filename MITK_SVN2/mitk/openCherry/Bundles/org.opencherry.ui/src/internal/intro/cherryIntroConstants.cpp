/*=========================================================================
 
 Program:   openCherry Platform
 Language:  C++
 Date:      $Date: 2009-11-03 17:02:46 +0100 (mar, 03 nov 2009) $
 Version:   $Revision: 19866 $
 
 Copyright (c) German Cancer Research Center, Division of Medical and
 Biological Informatics. All rights reserved.
 See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.
 
 This software is distributed WITHOUT ANY WARRANTY; without even
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the above copyright notices for more information.
 
 =========================================================================*/

#include "cherryIntroConstants.h"

namespace cherry {

const std::string IntroConstants::INTRO_VIEW_ID = "org.opencherry.ui.internal.introview";
const std::string IntroConstants::INTRO_EDITOR_ID = "org.opencherry.ui.internal.introeditor";
const int IntroConstants::INTRO_ROLE_VIEW = 0x01;
const int IntroConstants::INTRO_ROLE_EDITOR = 0x02;

}
