/*=========================================================================

Program:   openCherry Platform
Language:  C++
Date:      $Date: 2009-10-18 21:46:13 +0200 (dom, 18 oct 2009) $
Version:   $Revision: 19521 $

Copyright (c) German Cancer Research Center, Division of Medical and
Biological Informatics. All rights reserved.
See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notices for more information.

=========================================================================*/

#include "cherryPartPlaceholder.h"

#include "cherryPartStack.h"

namespace cherry
{

const std::string PartPlaceholder::WILD_CARD = "*"; //$NON-NLS-1$

PartPlaceholder::PartPlaceholder(const std::string& id) :
  StackablePart(id)
{

}

void PartPlaceholder::CreateControl(void*  /*parent*/)
{
  // do nothing
}

void* PartPlaceholder::GetControl()
{
  return 0;
}

bool PartPlaceholder::HasWildCard()
{
  return this->GetId().find_first_of(WILD_CARD) != std::string::npos;
}

bool PartPlaceholder::IsPlaceHolder() const
{
  return true;
}

}
