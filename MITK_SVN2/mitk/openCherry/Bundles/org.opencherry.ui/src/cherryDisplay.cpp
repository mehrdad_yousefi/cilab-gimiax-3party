/*=========================================================================

 Program:   openCherry Platform
 Language:  C++
 Date:      $Date: 2009-07-17 02:14:52 +0200 (vie, 17 jul 2009) $
 Version:   $Revision: 18253 $

 Copyright (c) German Cancer Research Center, Division of Medical and
 Biological Informatics. All rights reserved.
 See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.

 This software is distributed WITHOUT ANY WARRANTY; without even
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the above copyright notices for more information.

 =========================================================================*/

#include "cherryDisplay.h"

#include <Poco/Bugcheck.h>

namespace cherry {

Display* Display::instance = 0;

Display* Display::GetDefault()
{
  poco_assert(instance);
  return instance;
}

}
