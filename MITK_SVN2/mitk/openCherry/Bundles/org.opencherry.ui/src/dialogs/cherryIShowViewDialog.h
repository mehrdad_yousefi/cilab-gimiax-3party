/*=========================================================================

Program:   openCherry Platform
Language:  C++
Date:      $Date: 2009-01-23 09:44:29 +0100 (vie, 23 ene 2009) $
Version:   $Revision: 16084 $

Copyright (c) German Cancer Research Center, Division of Medical and
Biological Informatics. All rights reserved.
See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notices for more information.

=========================================================================*/

#ifndef CHERRYISHOWVIEWDIALOG_H_
#define CHERRYISHOWVIEWDIALOG_H_

#include "cherryIDialog.h"

#include <vector>
#include "../cherryIViewDescriptor.h"

namespace cherry {

/**
 * \ingroup org_opencherry_ui
 *
 */
struct CHERRY_UI IShowViewDialog : public IDialog
{
  cherryInterfaceMacro(IShowViewDialog, cherry);


  virtual std::vector<IViewDescriptor::Pointer> GetSelection() = 0;

};

}

#endif /*CHERRYISHOWVIEWDIALOG_H_*/
