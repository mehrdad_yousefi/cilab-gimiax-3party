/*=========================================================================

 Program:   openCherry Platform
 Language:  C++
 Date:      $Date: 2008-10-24 17:29:30 +0200 (vie, 24 oct 2008) $
 Version:   $Revision: 15582 $

 Copyright (c) German Cancer Research Center, Division of Medical and
 Biological Informatics. All rights reserved.
 See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.

 This software is distributed WITHOUT ANY WARRANTY; without even
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the above copyright notices for more information.

 =========================================================================*/

#include "cherryIDialog.h"

namespace cherry {

const int IDialog::NONE = 0;
const int IDialog::ERR = 1;
const int IDialog::INFORMATION = 2;
const int IDialog::QUESTION = 3;
const int IDialog::WARNING = 4;

}
