/*=========================================================================

 Program:   openCherry Platform
 Language:  C++
 Date:      $Date: 2008-10-01 11:54:41 +0200 (mié, 01 oct 2008) $
 Version:   $Revision: 15350 $

 Copyright (c) German Cancer Research Center, Division of Medical and
 Biological Informatics. All rights reserved.
 See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.

 This software is distributed WITHOUT ANY WARRANTY; without even
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the above copyright notices for more information.

 =========================================================================*/

#include "cherryIPropertyChangeListener.h"

#include "cherryIWorkbenchPartConstants.h"
#include <cherryObjects.h>

namespace cherry {

void
IPropertyChangeListener::Events
::AddListener(IPropertyChangeListener::Pointer listener)
{
  if (listener.IsNull()) return;

  this->propertyChange += Delegate(listener.GetPointer(), &IPropertyChangeListener::PropertyChange);
}

void
IPropertyChangeListener::Events
::RemoveListener(IPropertyChangeListener::Pointer listener)
{
  if (listener.IsNull()) return;

  this->propertyChange -= Delegate(listener.GetPointer(), &IPropertyChangeListener::PropertyChange);
}

void IPropertyChangeListener::PropertyChange(PropertyChangeEvent::Pointer event)
{
  if (event->GetProperty() == IWorkbenchPartConstants::INTEGER_PROPERTY)
  {
    this->PropertyChange(event->GetSource(), event->GetNewValue().Cast<ObjectInt>()->GetValue());
  }
}

}
