/*=========================================================================
 
Program:   openCherry Platform
Language:  C++
Date:      $Date: 2009-04-14 10:10:47 +0200 (mar, 14 abr 2009) $
Version:   $Revision: 16824 $
 
Copyright (c) German Cancer Research Center, Division of Medical and
Biological Informatics. All rights reserved.
See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.
 
This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notices for more information.
 
=========================================================================*/

#include "cherryIEditorRegistry.h"

namespace cherry
{

const int IEditorRegistry::PROP_CONTENTS = 0x01;
const std::string IEditorRegistry::SYSTEM_EXTERNAL_EDITOR_ID =
    "org.opencherry.ui.systemExternalEditor"; //$NON-NLS-1$
const std::string IEditorRegistry::SYSTEM_INPLACE_EDITOR_ID =
    "org.opencherry.ui.systemInPlaceEditor"; //$NON-NLS-1$

IEditorRegistry::~IEditorRegistry()
{
}

}
