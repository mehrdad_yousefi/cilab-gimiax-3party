/*=========================================================================

 Program:   openCherry Platform
 Language:  C++
 Date:      $Date: 2009-03-21 15:02:40 +0100 (sáb, 21 mar 2009) $
 Version:   $Revision: 16712 $

 Copyright (c) German Cancer Research Center, Division of Medical and
 Biological Informatics. All rights reserved.
 See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.

 This software is distributed WITHOUT ANY WARRANTY; without even
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the above copyright notices for more information.

 =========================================================================*/

#include "cherryImageDescriptor.h"

#include "tweaklets/cherryImageTweaklet.h"

namespace cherry {

ImageDescriptor::Pointer ImageDescriptor::CreateFromFile(const std::string& filename, const std::string& pluginid)
{
  return Tweaklets::Get(ImageTweaklet::KEY)->CreateFromFile(filename, pluginid);
}

ImageDescriptor::Pointer ImageDescriptor::CreateFromImage(void* img)
{
  return Tweaklets::Get(ImageTweaklet::KEY)->CreateFromImage(img);
}

ImageDescriptor::Pointer ImageDescriptor::GetMissingImageDescriptor()
{
  return Tweaklets::Get(ImageTweaklet::KEY)->GetMissingImageDescriptor();
}

ImageDescriptor::ImageDescriptor()
{

}

}
