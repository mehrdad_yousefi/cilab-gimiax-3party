/*=========================================================================

 Program:   openCherry Platform
 Language:  C++
 Date:      $Date: 2009-05-26 13:47:06 +0200 (mar, 26 may 2009) $
 Version:   $Revision: 17408 $

 Copyright (c) German Cancer Research Center, Division of Medical and
 Biological Informatics. All rights reserved.
 See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.

 This software is distributed WITHOUT ANY WARRANTY; without even
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the above copyright notices for more information.

 =========================================================================*/

#include "cherrySelectionChangedEvent.h"
#include "cherryISelectionProvider.h"


namespace cherry
{

SelectionChangedEvent::SelectionChangedEvent(
    ISelectionProvider::Pointer source, ISelection::ConstPointer selection)
{
  poco_assert(source.IsNotNull());
  poco_assert(selection.IsNotNull());

  this->source = source;
  this->selection = selection;
}

ISelectionProvider::Pointer SelectionChangedEvent::GetSource() const
{
  return source;
}

ISelection::ConstPointer SelectionChangedEvent::GetSelection() const
{
  return selection;
}

ISelectionProvider::Pointer SelectionChangedEvent::GetSelectionProvider() const
{
  return this->GetSource();
}

}
