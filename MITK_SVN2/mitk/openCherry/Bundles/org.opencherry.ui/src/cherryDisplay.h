/*=========================================================================

 Program:   openCherry Platform
 Language:  C++
 Date:      $Date: 2009-09-07 12:05:58 +0200 (lun, 07 sep 2009) $
 Version:   $Revision: 18832 $

 Copyright (c) German Cancer Research Center, Division of Medical and
 Biological Informatics. All rights reserved.
 See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.

 This software is distributed WITHOUT ANY WARRANTY; without even
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the above copyright notices for more information.

 =========================================================================*/


#ifndef CHERRYDISPLAY_H_
#define CHERRYDISPLAY_H_

#include <Poco/Runnable.h>

#include "cherryUiDll.h"

namespace cherry {

class CHERRY_UI Display
{

public:

  static Display* GetDefault();

  /**
   * Returns true if the calling thread is the same thread
   * who created this Display instance.
   * @return
   */
  virtual bool InDisplayThread() = 0;

  virtual void AsyncExec(Poco::Runnable*) = 0;
  virtual void SyncExec(Poco::Runnable*) = 0;

  virtual int RunEventLoop() = 0;
  virtual void ExitEventLoop(int code) = 0;

protected:

  /**
   * This method must be called from within the UI thread
   * and should create the Display instance and initialize
   * variables holding implementation specific thread data.
   */
  virtual void CreateDisplay() = 0;

  static Display* instance;

};

}

#endif /* CHERRYDISPLAY_H_ */
