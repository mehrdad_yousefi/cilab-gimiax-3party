/*=========================================================================

 Program:   openCherry Platform
 Language:  C++
 Date:      $Date: 2009-03-21 15:02:40 +0100 (sáb, 21 mar 2009) $
 Version:   $Revision: 16712 $

 Copyright (c) German Cancer Research Center, Division of Medical and
 Biological Informatics. All rights reserved.
 See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.

 This software is distributed WITHOUT ANY WARRANTY; without even
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the above copyright notices for more information.

 =========================================================================*/

#include "cherryGuiTkControlEvent.h"

namespace cherry
{

namespace GuiTk
{

ControlEvent::ControlEvent() :
  Event()
{
}

ControlEvent::ControlEvent(void* item, int x, int y, int width, int height) :
  Event()
{
  this->item = item;
  this->x = x;
  this->y = y;
  this->width = width;
  this->height = height;
}

}

}
