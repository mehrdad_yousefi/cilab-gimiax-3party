/*=========================================================================

 Program:   openCherry Platform
 Language:  C++
 Date:      $Date: 2008-10-24 17:29:30 +0200 (vie, 24 oct 2008) $
 Version:   $Revision: 15582 $

 Copyright (c) German Cancer Research Center, Division of Medical and
 Biological Informatics. All rights reserved.
 See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.

 This software is distributed WITHOUT ANY WARRANTY; without even
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the above copyright notices for more information.

 =========================================================================*/

#include "cherryISelectionChangedListener.h"
#include "cherryISelectionProvider.h"

namespace cherry {

void
ISelectionChangedListener::Events
::AddListener(ISelectionChangedListener::Pointer listener)
{
  if (listener.IsNull()) return;

  this->selectionChanged += Delegate(listener.GetPointer(), &ISelectionChangedListener::SelectionChanged);
}

void
ISelectionChangedListener::Events
::RemoveListener(ISelectionChangedListener::Pointer listener)
{
  if (listener.IsNull()) return;

  this->selectionChanged -= Delegate(listener.GetPointer(), &ISelectionChangedListener::SelectionChanged);
}

}
