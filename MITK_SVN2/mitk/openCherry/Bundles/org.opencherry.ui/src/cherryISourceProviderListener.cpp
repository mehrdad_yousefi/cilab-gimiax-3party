/*=========================================================================

 Program:   openCherry Platform
 Language:  C++
 Date:      $Date: 2008-10-01 11:54:41 +0200 (mié, 01 oct 2008) $
 Version:   $Revision: 15350 $

 Copyright (c) German Cancer Research Center, Division of Medical and
 Biological Informatics. All rights reserved.
 See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.

 This software is distributed WITHOUT ANY WARRANTY; without even
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the above copyright notices for more information.

 =========================================================================*/

#include "cherryISourceProviderListener.h"

namespace cherry {

void ISourceProviderListener::Events::AddListener(ISourceProviderListener::Pointer l)
{
  if (l == 0) return;

  multipleSourcesChanged += Delegate2(l.GetPointer(), &ISourceProviderListener::SourceChanged);
  singleSourceChanged += Delegate3(l.GetPointer(), &ISourceProviderListener::SourceChanged);
}

void ISourceProviderListener::Events::RemoveListener(ISourceProviderListener::Pointer l)
{
  if (l == 0) return;

  multipleSourcesChanged -= Delegate2(l.GetPointer(), &ISourceProviderListener::SourceChanged);
  singleSourceChanged -= Delegate3(l.GetPointer(), &ISourceProviderListener::SourceChanged);
}

}
