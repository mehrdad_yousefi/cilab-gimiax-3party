/*=========================================================================

Program:   openCherry Platform
Language:  C++
Date:      $Date: 2009-06-08 18:45:12 +0200 (lun, 08 jun 2009) $
Version:   $Revision: 17662 $

Copyright (c) German Cancer Research Center, Division of Medical and
Biological Informatics. All rights reserved.
See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notices for more information.

=========================================================================*/

#ifndef CHERRYISELECTION_H_
#define CHERRYISELECTION_H_

#include <cherryMacros.h>
#include <cherryObject.h>

#include "cherryUiDll.h"

namespace cherry {

/**
 * \ingroup org_opencherry_ui
 *
 * Interface for a selection.
 *
 * @see ISelectionProvider
 * @see ISelectionChangedListener
 * @see SelectionChangedEvent
 *
 **/
struct ISelection : public Object
{
  cherryInterfaceMacro(ISelection, cherry);

  /**
   * Returns whether this selection is empty.
   *
   * @return <code>true</code> if this selection is empty,
   *   and <code>false</code> otherwise
   */
  virtual bool IsEmpty() const = 0;

};

}

#endif /*CHERRYISELECTION_H_*/
