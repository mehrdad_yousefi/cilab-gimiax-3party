/*=========================================================================
 
 Program:   openCherry Platform
 Language:  C++
 Date:      $Date: 2009-09-07 12:05:58 +0200 (lun, 07 sep 2009) $
 Version:   $Revision: 18832 $
 
 Copyright (c) German Cancer Research Center, Division of Medical and
 Biological Informatics. All rights reserved.
 See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.
 
 This software is distributed WITHOUT ANY WARRANTY; without even
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the above copyright notices for more information.
 
 =========================================================================*/

#include "cherryTestRegistry.h"
#include "cherryTestDescriptor.h"

#include <cherryPlatform.h>
#include <service/cherryIExtensionPointService.h>

namespace cherry
{

const std::string TestRegistry::TAG_TEST = "test";
const std::string TestRegistry::ATT_ID = "id";
const std::string TestRegistry::ATT_CLASS = "class";
const std::string TestRegistry::ATT_DESCRIPTION = "description";
const std::string TestRegistry::ATT_UITEST = "uitest";

const std::string TestRegistry::TEST_MANIFEST = "CppUnitTest";

TestRegistry::TestRegistry()
{
  std::vector<IConfigurationElement::Pointer> elements(
      Platform::GetExtensionPointService()->GetConfigurationElementsFor(
          "org.opencherry.tests"));

  for (std::vector<IConfigurationElement::Pointer>::iterator i =
      elements.begin(); i != elements.end(); ++i)
  {
    if ((*i)->GetName() == TAG_TEST)
    {
      this->ReadTest(*i);
    }
  }
}

const std::vector<ITestDescriptor::Pointer>&
TestRegistry::GetTestsForId(const std::string& pluginid)
{
  return mapIdToTests[pluginid];
}

void TestRegistry::ReadTest(IConfigurationElement::Pointer testElem)
{
  ITestDescriptor::Pointer descriptor(new TestDescriptor(testElem));
  poco_assert(descriptor->GetId() != "");
  mapIdToTests[descriptor->GetContributor()].push_back(descriptor);
}

}
