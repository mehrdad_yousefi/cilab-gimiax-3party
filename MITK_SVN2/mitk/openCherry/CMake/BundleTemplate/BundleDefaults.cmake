SET(BUNDLE_NAMESPACE "cherry")
SET(DEFAULT_REQUIRED_BUNDLES "org.opencherry.osgi")
SET(DEFAULT_REQUIRED_BUNDLES_FOR_GUI "org.opencherry.ui.qt")
SET(DEFAULT_VIEW_BASE_CLASS "cherry::QtViewPart")#qt
SET(DEFAULT_VIEW_BASE_CLASS_H "cherryQtViewPart.h")#qt)
SET(PROJECT_STATIC_VAR "CHERRY_STATIC")
SET(DOXYGEN_INGROUP "openCherryPlugins")

SET(DEFAULT_CREATE_PLUGIN_MACRO "MACRO_CREATE_PLUGIN()")
SET(DEFAULT_CREATE_GUI_PLUGIN_MACRO "MACRO_CREATE_QT_PLUGIN()")

SET(PLUGIN_COPYRIGHT "/*=========================================================================
 
Program:   openCherry Platform
Language:  C++
Date:      $Date: 2008-06-13 21:02:28 +0200 (Fr, 13 Jun 2008) $
Version:   $Revision: 14620 $
 
Copyright (c) German Cancer Research Center, Division of Medical and
Biological Informatics. All rights reserved.
See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.
 
This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notices for more information.
 
=========================================================================*/")
