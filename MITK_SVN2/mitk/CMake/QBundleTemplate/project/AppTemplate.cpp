#include <org.opencherry.osgi/src/application/cherryStarter.h>

int main(int argc, char** argv)
{
  return cherry::Starter::Run(argc, argv);
}
