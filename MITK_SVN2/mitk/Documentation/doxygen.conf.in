# Doxyfile 1.3-rc3

#---------------------------------------------------------------------------
# General configuration options
#---------------------------------------------------------------------------
PROJECT_NAME           = mitk
PROJECT_NUMBER         = @MITK_VERSION_STRING@
OUTPUT_DIRECTORY       = @MITK_DOXYGEN_OUTPUT_DIR@
OUTPUT_LANGUAGE        = English
EXTRACT_ALL            = YES
EXTRACT_PRIVATE        = NO
EXTRACT_STATIC         = YES
EXTRACT_LOCAL_CLASSES  = YES
HIDE_UNDOC_MEMBERS     = NO
HIDE_UNDOC_CLASSES     = NO
HIDE_FRIEND_COMPOUNDS  = NO
HIDE_IN_BODY_DOCS      = NO
BRIEF_MEMBER_DESC      = YES
REPEAT_BRIEF           = YES
ALWAYS_DETAILED_SEC    = NO
INLINE_INHERITED_MEMB  = NO
FULL_PATH_NAMES        = NO
STRIP_FROM_PATH        = 
INTERNAL_DOCS          = NO
CASE_SENSE_NAMES       = YES
SHORT_NAMES            = NO
HIDE_SCOPE_NAMES       = NO
VERBATIM_HEADERS       = YES
SHOW_INCLUDE_FILES     = YES
JAVADOC_AUTOBRIEF      = NO
MULTILINE_CPP_IS_BRIEF = NO
DETAILS_AT_TOP         = NO
INHERIT_DOCS           = YES
INLINE_INFO            = YES
SORT_MEMBER_DOCS       = YES
DISTRIBUTE_GROUP_DOC   = NO
TAB_SIZE               = 8
GENERATE_TODOLIST      = YES
GENERATE_TESTLIST      = YES
GENERATE_BUGLIST       = YES
GENERATE_DEPRECATEDLIST= YES
ALIASES                = "FIXME=\par Fix Me's:\n" \
						 "openCherry=\if OPENCHERRY" \
						 "endopenCherry=\endif"
ENABLED_SECTIONS       = @MITK_DOXYGEN_ENABLED_SECTIONS@
MAX_INITIALIZER_LINES  = 30
OPTIMIZE_OUTPUT_FOR_C  = NO
OPTIMIZE_OUTPUT_JAVA   = NO
SHOW_USED_FILES        = YES
#---------------------------------------------------------------------------
# configuration options related to warning and progress messages
#---------------------------------------------------------------------------
QUIET                  = NO
WARNINGS               = YES
WARN_IF_UNDOCUMENTED   = YES
WARN_IF_DOC_ERROR      = YES
WARN_FORMAT            = "$file:$line: $text"
WARN_LOGFILE           = 
#---------------------------------------------------------------------------
# configuration options related to the input files
#---------------------------------------------------------------------------
INPUT                  = ./ \
                         @MITK_SOURCE_DIR@ @MITK_BINARY_DIR@ \
                         @MBIQM_SOURCE_DIR@ @MBIQM_BINARY_DIR@ \
                         @MBISB_SOURCE_DIR@ @MBISB_BINARY_DIR@ \
FILE_PATTERNS          = *.h \
                         *.cpp \
                         *.dox \
                         *.txx \
                         *.cxx
RECURSIVE              = YES
EXCLUDE                = @MITK_SOURCE_DIR@/Utilities/ipPic/ \
                         @MITK_SOURCE_DIR@/Utilities/ipFunc/ \
                         @MITK_SOURCE_DIR@/Utilities/vecmath/ \
                         @MITK_SOURCE_DIR@/openCherry/ \
                         @MITK_SOURCE_DIR@/Deprecated/ \
                         @MITK_SOURCE_DIR@/Utilities/Poco/ \
                         @MITK_SOURCE_DIR@/Core/Testing/ \
                         @MITK_SOURCE_DIR@/CMake/ \
                         \
                         @MBIQM_SOURCE_DIR@/QFunctionalities/
                         @MBIQM_SOURCE_DIR@/Qmitk/ \
                         \
                         @MBISB_SOURCE_DIR@/QApplications/ \
                         @MBISB_SOURCE_DIR@/CMake/ \
                         @MBISB_SOURCE_DIR@/QFunctionalities/ \
                         @MBISB_SOURCE_DIR@/Qmitk/
EXCLUDE_SYMLINKS       = NO
EXCLUDE_PATTERNS       = moc_* \
                         Register*
EXAMPLE_PATH           = @MITK_SOURCE_DIR@/Applications/Tutorial/
EXAMPLE_PATTERNS       = 
EXAMPLE_RECURSIVE      = NO
IMAGE_PATH             = @MITK_SOURCE_DIR@/Documentation/Doxygen/ \
                         @MITK_SOURCE_DIR@/Documentation/Doxygen/Modules/ \
                         @MITK_SOURCE_DIR@/Documentation/Doxygen/Tutorial/ \
                         @MITK_SOURCE_DIR@/Core/DataStructures/ \
                         @MITK_SOURCE_DIR@ \
                         @USERMANUAL_DIRS@
INPUT_FILTER           = 
FILTER_SOURCE_FILES    = NO
#---------------------------------------------------------------------------
# configuration options related to source browsing
#---------------------------------------------------------------------------
SOURCE_BROWSER         = YES
INLINE_SOURCES         = YES
STRIP_CODE_COMMENTS    = YES
REFERENCED_BY_RELATION = YES
REFERENCES_RELATION    = YES
#---------------------------------------------------------------------------
# configuration options related to the alphabetical class index
#---------------------------------------------------------------------------
ALPHABETICAL_INDEX     = YES
COLS_IN_ALPHA_INDEX    = 3
IGNORE_PREFIX          = 
#---------------------------------------------------------------------------
# configuration options related to the HTML output
#---------------------------------------------------------------------------
GENERATE_HTML          = YES
HTML_OUTPUT            = html
HTML_FILE_EXTENSION    = .html
HTML_HEADER            = 
HTML_FOOTER            = 
HTML_STYLESHEET        = 
HTML_ALIGN_MEMBERS     = YES
GENERATE_HTMLHELP      = NO
CHM_FILE               = 
HHC_LOCATION           = 
GENERATE_CHI           = NO
BINARY_TOC             = NO
TOC_EXPAND             = NO
DISABLE_INDEX          = NO
ENUM_VALUES_PER_LINE   = 4
GENERATE_TREEVIEW      = NO
TREEVIEW_WIDTH         = 250
#---------------------------------------------------------------------------
# configuration options related to the LaTeX output
#---------------------------------------------------------------------------
GENERATE_LATEX         = NO
LATEX_OUTPUT           = latex
LATEX_CMD_NAME         = latex
MAKEINDEX_CMD_NAME     = makeindex
COMPACT_LATEX          = NO
PAPER_TYPE             = a4wide
EXTRA_PACKAGES         = 
LATEX_HEADER           = 
PDF_HYPERLINKS         = NO
USE_PDFLATEX           = NO
LATEX_BATCHMODE        = NO
#---------------------------------------------------------------------------
# configuration options related to the RTF output
#---------------------------------------------------------------------------
GENERATE_RTF           = NO
RTF_OUTPUT             = rtf
COMPACT_RTF            = NO
RTF_HYPERLINKS         = NO
RTF_STYLESHEET_FILE    = 
RTF_EXTENSIONS_FILE    = 
#---------------------------------------------------------------------------
# configuration options related to the man page output
#---------------------------------------------------------------------------
GENERATE_MAN           = NO
MAN_OUTPUT             = man
MAN_EXTENSION          = .3
MAN_LINKS              = NO
#---------------------------------------------------------------------------
# configuration options related to the XML output
#---------------------------------------------------------------------------
GENERATE_XML           = NO
XML_SCHEMA             = 
XML_DTD                = 
#---------------------------------------------------------------------------
# configuration options for the AutoGen Definitions output
#---------------------------------------------------------------------------
GENERATE_AUTOGEN_DEF   = NO
#---------------------------------------------------------------------------
# configuration options related to the Perl module output
#---------------------------------------------------------------------------
GENERATE_PERLMOD       = NO
PERLMOD_LATEX          = NO
PERLMOD_PRETTY         = YES
PERLMOD_MAKEVAR_PREFIX = 
#---------------------------------------------------------------------------
# Configuration options related to the preprocessor   
#---------------------------------------------------------------------------
ENABLE_PREPROCESSING   = YES
MACRO_EXPANSION        = YES
EXPAND_ONLY_PREDEF     = NO
SEARCH_INCLUDES        = YES
INCLUDE_PATH           = 
INCLUDE_FILE_PATTERNS  = 
PREDEFINED             = itkNotUsed(x)= \
                         "itkSetMacro(name,type)=                   virtual void Set##name (type _arg);" \
                         "itkGetMacro(name,type)=                   virtual type Get##name ();" \
                         "itkGetConstMacro(name,type)=                   virtual type Get##name () const;" \
                         "itkSetStringMacro(name)=                   virtual void Set##name (const char* _arg);" \
                         "itkGetStringMacro(name)=                   virtual const char* Get##name () const;" \
                         "itkSetClampMacro(name,type,min,max)=                   virtual void Set##name (type _arg);" \
                         "itkSetObjectMacro(name,type)=                   virtual void Set##name (type* _arg);" \
                         "itkGetObjectMacro(name,type)=                   virtual type* Get##name ();" \
                         "itkSetConstObjectMacro(name,type)=                   virtual void Set##name ( const type* _arg);" \
                         "itkGetConstObjectMacro(name,type)=                   virtual const type* Get##name ();" \
                         "itkGetConstReferenceMacro(name,type)=                   virtual const type& Get##name ();" \
                         "itkGetConstReferenceObjectMacro(name,type)=                   virtual const type::Pointer& Get##name () const;" \
                         "itkBooleanMacro(name)=                   virtual void name##On ();                   virtual void name##Off ();" \
                         "itkSetVector2Macro(name,type)=                   virtual void Set##name (type _arg1, type _arg2)                   virtual void Set##name (type _arg[2]);" \
                         "itkGetVector2Macro(name,type)=                   virtual type* Get##name () const;                   virtual void Get##name (type& _arg1, type& _arg2) const;                   virtual void Get##name (type _arg[2]) const;" \
                         "itkSetVector3Macro(name,type)=                   virtual void Set##name (type _arg1, type _arg2, type _arg3)                   virtual void Set##name (type _arg[3]);" \
                         "itkGetVector3Macro(name,type)=                   virtual type* Get##name () const;                   virtual void Get##name (type& _arg1, type& _arg2, type& _arg3) const;                   virtual void Get##name (type _arg[3]) const;" \
                         "itkSetVector4Macro(name,type)=                   virtual void Set##name (type _arg1, type _arg2, type _arg3, type _arg4)                   virtual void Set##name (type _arg[4]);" \
                         "itkGetVector4Macro(name,type)=                   virtual type* Get##name () const;                   virtual void Get##name (type& _arg1, type& _arg2, type& _arg3, type& _arg4) const;                   virtual void Get##name (type _arg[4]) const;" \
                         "itkSetVector6Macro(name,type)=                   virtual void Set##name (type _arg1, type _arg2, type _arg3, type _arg4, type _arg5, type _arg6)                   virtual void Set##name (type _arg[6]);" \
                         "itkGetVector6Macro(name,type)=                   virtual type* Get##name () const;                   virtual void Get##name (type& _arg1, type& _arg2, type& _arg3, type& _arg4, type& _arg5, type& _arg6) const;                   virtual void Get##name (type _arg[6]) const;" \
                         "itkSetVectorMacro(name,type,count)=                   virtual void Set##name(type data[]);" \
                         "itkGetVectorMacro(name,type,count)=                   virtual type* Get##name () const;" \
                         "itkNewMacro(type)=                   static Pointer New();" \
                         "itkTypeMacro(thisClass,superclass)=                   virtual const char *GetClassName() const;" \
                         "itkConceptMacro(name,concept)=                   enum { name = 0 };" \
                         "ITK_NUMERIC_LIMITS=                   std::numeric_limits" \
                         "ITK_TYPENAME=                   typename" \
                         "FEM_ABSTRACT_CLASS(thisClass,parentClass)=                   public:                                                                   /** Standard Self typedef.*/                                          typedef thisClass Self;                                                 /** Standard Superclass typedef. */                                   typedef parentClass Superclass;                                         /** Pointer or SmartPointer to an object. */                            typedef Self* Pointer;                                                  /** Const pointer or SmartPointer to an object. */                      typedef const Self* ConstPointer;                                     private:" \
                         "FEM_CLASS(thisClass,parentClass)=                   FEM_ABSTRACT_CLASS(thisClass,parentClass)                               public:                                                                   /** Create a new object from the existing one  */                       virtual Baseclass::Pointer Clone() const;                               /** Class ID for FEM object factory */                                  static const int CLID;                                                  /** Virtual function to access the class ID */                          virtual int ClassID() const                                               { return CLID; }                                                      /** Object creation in an itk compatible way */                         static Self::Pointer New()                                                { return new Self(); }                                              private:" \
                         FREEVERSION \
                         ERROR_CHECKING \
                         HAS_TIFF \
                         HAS_JPEG \
                         HAS_NETLIB \
                         HAS_PNG \
                         HAS_ZLIB \
                         HAS_GLUT \
                         HAS_QT \
                         VCL_USE_NATIVE_STL=1 \
                         VCL_USE_NATIVE_COMPLEX=1 \
                         VCL_HAS_BOOL=1 \
                         VXL_BIG_ENDIAN=1 \
                         VXL_LITTLE_ENDIAN=0 \
                         VNL_DLL_DATA= \
                         size_t=vcl_size_t \
                         DOXYGEN_SKIP 
EXPAND_AS_DEFINED      = 
SKIP_FUNCTION_MACROS   = YES
#---------------------------------------------------------------------------
# Configuration::additions related to external references   
#---------------------------------------------------------------------------
TAGFILES               = @OPENCHERRY_TAGFILE@
GENERATE_TAGFILE       = 
ALLEXTERNALS           = NO
EXTERNAL_GROUPS        = NO
PERL_PATH              = /usr/bin/perl
#---------------------------------------------------------------------------
# Configuration options related to the dot tool   
#---------------------------------------------------------------------------
CLASS_DIAGRAMS         = YES
HIDE_UNDOC_RELATIONS   = YES
HAVE_DOT               = @HAVE_DOT@
CLASS_GRAPH            = YES
COLLABORATION_GRAPH    = YES
TEMPLATE_RELATIONS     = YES
INCLUDE_GRAPH          = YES
INCLUDED_BY_GRAPH      = YES
GRAPHICAL_HIERARCHY    = YES
DOT_IMAGE_FORMAT       = png
DOT_PATH               = @DOXYGEN_DOT_PATH@
DOTFILE_DIRS           = 
MAX_DOT_GRAPH_WIDTH    = 1024
MAX_DOT_GRAPH_HEIGHT   = 1024
GENERATE_LEGEND        = YES
DOT_CLEANUP            = YES
