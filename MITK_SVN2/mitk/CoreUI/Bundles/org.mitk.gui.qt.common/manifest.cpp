/*=========================================================================
 
 Program:   Medical Imaging & Interaction Toolkit
 Language:  C++
 Date:      $Date: 2009-10-23 02:14:03 +0200 (vie, 23 oct 2009) $
 Version:   $Revision: 19649 $
 
 Copyright (c) German Cancer Research Center, Division of Medical and
 Biological Informatics. All rights reserved.
 See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.
 
 This software is distributed WITHOUT ANY WARRANTY; without even
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the above copyright notices for more information.
 
 =========================================================================*/

#include <Poco/ClassLibrary.h>

#include <cherryIEditorPart.h>
#include <cherryIBundleActivator.h>

#include "src/QmitkStdMultiWidgetEditor.h"
#include "src/internal/QmitkCommonActivator.h"
#include "src/QmitkGeneralPreferencePage.h"

POCO_BEGIN_NAMED_MANIFEST(cherryIEditorPart, cherry::IEditorPart)
  POCO_EXPORT_CLASS(QmitkStdMultiWidgetEditor)
POCO_END_MANIFEST

POCO_BEGIN_MANIFEST(cherry::IBundleActivator)
  POCO_EXPORT_CLASS(QmitkCommonActivator)
POCO_END_MANIFEST

POCO_BEGIN_NAMED_MANIFEST(cherryIPreferencePage, cherry::IPreferencePage)
  POCO_EXPORT_CLASS(QmitkGeneralPreferencePage)
POCO_END_MANIFEST
