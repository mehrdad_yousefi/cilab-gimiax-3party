/*=========================================================================

Program:   Medical Imaging & Interaction Toolkit
Language:  C++
Date:      $Date: 2009-10-23 02:14:03 +0200 (vie, 23 oct 2009) $
Version:   $Revision: 19649 $

Copyright (c) German Cancer Research Center, Division of Medical and
Biological Informatics. All rights reserved.
See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notices for more information.

=========================================================================*/

#ifndef QMITKWORKBENCHWINDOWADVISOR_H_
#define QMITKWORKBENCHWINDOWADVISOR_H_

#include <cherryWorkbenchWindowAdvisor.h>

#include "mitkQtAppDll.h"

class MITK_QT_APP QmitkWorkbenchWindowAdvisor : public cherry::WorkbenchWindowAdvisor
{
public:

    QmitkWorkbenchWindowAdvisor(cherry::IWorkbenchWindowConfigurer::Pointer configurer);

    cherry::ActionBarAdvisor::Pointer CreateActionBarAdvisor(
        cherry::IActionBarConfigurer::Pointer configurer);

    void PostWindowCreate();
};

#endif /*QMITKWORKBENCHWINDOWADVISOR_H_*/
