/*=========================================================================

Program:   Medical Imaging & Interaction Toolkit
Language:  C++
Date:      $Date: 2009-10-15 00:24:17 +0200 (jue, 15 oct 2009) $
Version:   $Revision: 19488 $

Copyright (c) German Cancer Research Center, Division of Medical and
Biological Informatics. All rights reserved.
See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notices for more information.

=========================================================================*/

#include "QmitkActionBarAdvisor.h"


QmitkActionBarAdvisor::QmitkActionBarAdvisor(cherry::IActionBarConfigurer::Pointer configurer)
 : cherry::ActionBarAdvisor(configurer)
{

}

void QmitkActionBarAdvisor::FillMenuBar(void* )
{

}
