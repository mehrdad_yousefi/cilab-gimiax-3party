/*=========================================================================

 Program:   openCherry Platform
 Language:  C++
 Date:      $Date: 2009-10-22 16:51:14 +0200 (jue, 22 oct 2009) $
 Version:   $Revision: 19636 $

 Copyright (c) German Cancer Research Center, Division of Medical and
 Biological Informatics. All rights reserved.
 See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.

 This software is distributed WITHOUT ANY WARRANTY; without even
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the above copyright notices for more information.

 =========================================================================*/


#ifndef QMITKCUSTOMVARIANTS_H_
#define QMITKCUSTOMVARIANTS_H_

#include <mitkDataTreeNode.h>

Q_DECLARE_METATYPE(mitk::DataTreeNode::Pointer)
Q_DECLARE_METATYPE(mitk::DataTreeNode*)

#endif /* QMITKCUSTOMVARIANTS_H_ */
