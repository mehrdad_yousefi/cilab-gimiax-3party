/*=========================================================================

 Program:   openCherry Platform
 Language:  C++
 Date:      $Date: 2009-05-12 20:04:59 +0200 (mar, 12 may 2009) $
 Version:   $Revision: 17180 $

 Copyright (c) German Cancer Research Center, Division of Medical and
 Biological Informatics. All rights reserved.
 See MITKCopyright.txt or http://www.mitk.org/copyright.html for details.

 This software is distributed WITHOUT ANY WARRANTY; without even
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the above copyright notices for more information.

 =========================================================================*/


#ifndef QMITKENUMS_H_
#define QMITKENUMS_H_

enum QmitkItemModelRole
{
  QmitkDataTreeNodeRole = 64
};

#endif /* QMITKENUMS_H_ */
