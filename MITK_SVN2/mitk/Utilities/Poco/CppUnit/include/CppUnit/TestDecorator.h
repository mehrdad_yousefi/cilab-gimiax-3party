//
// TestDecorator.h
//
// $Id: TestDecorator.h 15789 2008-11-27 16:25:11Z anonymous $
//


#ifndef CppUnit_TestDecorator_INCLUDED
#define CppUnit_TestDecorator_INCLUDED


#include "CppUnit/CppUnit.h"
#include "CppUnit/Guards.h"
#include "CppUnit/Test.h"


namespace CppUnit {


class TestResult;


/*
 * A Decorator for Tests
 *
 * Does not assume ownership of the test it decorates
 *
 */
class CppUnit_API TestDecorator: public Test
{
	REFERENCEOBJECT(TestDecorator)

public:
	TestDecorator(Test* test);

	virtual ~TestDecorator();

	int countTestCases();

	void run(TestResult* result);

	std::string toString();

protected:
	Test* _test;
};


} // namespace CppUnit


#endif // CppUnit_TestDecorator_INCLUDED
