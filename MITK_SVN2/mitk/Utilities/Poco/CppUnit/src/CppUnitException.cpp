//
// CppUnitException.cpp
//
// $Id: CppUnitException.cpp 15789 2008-11-27 16:25:11Z anonymous $
//


#include "CppUnit/CppUnitException.h"


namespace CppUnit {


const std::string CppUnitException::CPPUNIT_UNKNOWNFILENAME = "<unknown>";
const int CppUnitException::CPPUNIT_UNKNOWNLINENUMBER = -1;


} // namespace CppUnit
