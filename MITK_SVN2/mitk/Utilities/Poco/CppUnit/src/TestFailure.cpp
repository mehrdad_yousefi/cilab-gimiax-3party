//
// TestFailure.cpp
//
// $Id: TestFailure.cpp 15789 2008-11-27 16:25:11Z anonymous $
//


#include "CppUnit/TestFailure.h"
#include "CppUnit/Test.h"


namespace CppUnit {


// Returns a short description of the failure.
std::string TestFailure::toString()
{
	return _failedTest->toString () + ": " + _thrownException->what();
}


} // namespace CppUnit
