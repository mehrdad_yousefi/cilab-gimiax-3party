# Used to configure MITK
from csnGIMIASDef import *
from csnAPIPublic import GetAPI

api = GetAPI("2.5.0-beta")

mitk = api.CreateThirdPartyProject("MITK")
mitk.AddProjects([ann, itk, vtk])
mitk.SetUseFilePath("%s/MITK_SVN2/UseMITK_SVN2.cmake" % mitk.GetBuildFolder())
mitk.SetConfigFilePath("%s/MITK_SVN2/MITK_SVN2Config.cmake" % mitk.GetBuildFolder())
if api.GetCompiler().TargetIsWindows():
  mitk.AddFilesToInstall(mitk.Glob("mitk/lib/Debug/*.dll"))
mitk.AddFilesToInstall(mitk.Glob("mitk/Core/Code/Interactions/StateMachine.xml"))
mitk.AddFilesToInstall(mitk.Glob("mitk/Config/mitkLevelWindowPresets.xml"))

# add dlls to be installed
if api.GetCompiler().TargetIsWindows():
  mitk.AddFilesToInstall(["bin\Debug\mitkCore.dll"], debugOnly = 1)
  mitk.AddFilesToInstall(["bin\Debug\mitkCoreExt.dll"], debugOnly = 1)
  mitk.AddFilesToInstall(["bin\Debug\mbilog.dll"], debugOnly = 1)
  mitk.AddFilesToInstall(["bin\Release\mitkCore.dll"], releaseOnly = 1)
  mitk.AddFilesToInstall(["bin\Release\mitkCoreExt.dll"], releaseOnly = 1)
  mitk.AddFilesToInstall(["bin\Release\mbilog.dll"], releaseOnly = 1)


