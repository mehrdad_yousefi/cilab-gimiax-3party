#include "ModuleDescriptionUtilities.h"
#include "ModuleParameterGroup.h"
#include "ModuleDescription.h"
#include <string>
#include <vector>

void
replaceSubWithSub(std::string& s, const char *o, const char  *n)
{
  if (s.size())
    {
    std::string from(o), to(n);
    std::string::size_type start = 0;
    while ((start = s.find(from, start)) != std::string::npos)
      {
      s.replace(start, from.size(), to);
      start += to.size();
      }
    }
}

void
trimLeading(std::string& s, const char* extraneousChars)
{
  if (s.size())
    {
    std::string::size_type pos = s.find_first_not_of(extraneousChars);
    if (pos != std::string::npos)
      {
      s = s.substr(pos);
      }
    else
      {
      s = "";
      }
    }
}

void
trimTrailing(std::string& s, const char* extraneousChars)
{
  if (s.size())
    {
    std::string::size_type pos = s.find_last_not_of(extraneousChars);
    if (pos != std::string::npos)
      {
      s = s.substr(0, pos + 1);
      }
    else
      {
      s = "";
      }
    }
}

void
trimLeadingAndTrailing(std::string& s, const char* extraneousChars)
{
  trimLeading(s, extraneousChars);
  trimTrailing(s, extraneousChars);
}

ModuleParameter *findModuleParameter( 
	ModuleDescription* module, 
	const std::string &channel, 
	int count, 
	const std::string &name,
	const std::string &tag )
{
	ModuleParameter *parameter = NULL;
	int parameterCount = 0;

	std::vector<ModuleParameterGroup> parameterGroups = module->GetParameterGroups();
	std::vector<ModuleParameterGroup>::iterator it;
	std::vector<ModuleParameterGroup>::iterator itGroup;
	itGroup = module->GetParameterGroups().begin();
	while (itGroup != module->GetParameterGroups().end())
	{
		std::vector<ModuleParameter>::iterator itParam;
		itParam = itGroup->GetParameters().begin();
		while (itParam != itGroup->GetParameters().end())
		{
			bool channelOk = channel.empty( ) || itParam->GetChannel() == channel;
			bool nameOk = name.empty() || itParam->GetName() == name;
			bool tagOk = tag.empty() || itParam->GetTag() == tag;
			if ( channelOk && nameOk && tagOk )
			{
				bool countOk = parameterCount == count;
				if ( countOk )
				{
					parameter = &(*itParam);
				}
				parameterCount++;
			}
			itParam++;
		}
		itGroup++;
	}

	return parameter;
}

