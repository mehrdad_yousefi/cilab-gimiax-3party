# Used to configure slicer
from csnAPIPublic import GetAPI
api = GetAPI("2.5.0")

slicer = api.CreateThirdPartyProject("SLICER")
# config files locations
slicer.SetUseFilePath("%s/SLICER/UseSLICER.cmake" % slicer.GetBuildFolder())
slicer.SetConfigFilePath("%s/SLICER/SLICERConfig.cmake" % slicer.GetBuildFolder())
# files to install
if api.GetCompiler().TargetIsWindows():
  slicer.AddFilesToInstall(["bin/Debug/ModuleDescriptionParser.dll"], debugOnly = 1)
  slicer.AddFilesToInstall(["bin/Release/ModuleDescriptionParser.dll"], releaseOnly = 1)

generateClp = api.CreateThirdPartyProject("GenerateCLP")
# config files locations
generateClp.SetUseFilePath("%s/SLICER/Slicer3/GenerateCLP/UseGenerateCLP.cmake" % slicer.GetBuildFolder())
generateClp.SetConfigFilePath("%s/SLICER/Slicer3/GenerateCLP/GenerateCLPConfig.cmake" % slicer.GetBuildFolder())

tclap = api.CreateThirdPartyProject("TCLAP")
# config files locations
tclap.SetUseFilePath("%s/SLICER/Slicer3/tclap/UseTCLAP.cmake" % slicer.GetBuildFolder())
tclap.SetConfigFilePath("%s/SLICER/Slicer3/tclap/TCLAPConfig.cmake" % slicer.GetBuildFolder())
