-------------------------------------------------------------------------------
SLICER: free, open source software package for visualization and image analysis
-------------------------------------------------------------------------------
Main Web: http://www.slicer.org

Version: 3.6
Web: 
 * http://svn.slicer.org/Slicer3/trunk/Libs/SlicerExecutionModel/tclap
 * http://svn.slicer.org/Slicer3/trunk/Libs/SlicerExecutionModel/ModuleDescriptionParser
 * http://svn.slicer.org/Slicer3/trunk/Libs/SlicerExecutionModel/GenerateCLP 
And
 * http://svn.slicer.org/Slicer3/trunk/Applications/CLI (only Templates)
 * http://svn.slicer.org/Slicer3/trunk/Base/CLI
 
