--------------------------------------------------------
ITK: the Insight Segmentation and Registration Toolkit
--------------------------------------------------------
Main Web: http://www.itk.org/

Version: 3.20
Web: http://sourceforge.net/projects/itk/files/itk/3.20/InsightToolkit-3.20.0.tar.gz

The tagfile was found at: http://www.itk.org/Doxygen320/InsightDoxygen.tag
