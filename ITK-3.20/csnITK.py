# Used to configure ITK
from csnToolkitOpen import *
from csnAPIPublic import GetAPI
api = GetAPI("2.5.0")

itk = api.CreateThirdPartyProject("ITK")
itk.SetUseFilePath("%s/ITK-3.20/UseITK-3.20.cmake" % itk.GetBuildFolder())
itk.SetConfigFilePath("%s/ITK-3.20/ITK-3.20Config.cmake" % itk.GetBuildFolder())

if api.GetCompiler().TargetIsWindows():
  itk.AddFilesToInstall(["bin/Debug/ITKCommon.dll"], debugOnly = 1)
  itk.AddFilesToInstall(["bin/Release/ITKCommon.dll"], releaseOnly = 1)

itk.AddProjects([vtk])
