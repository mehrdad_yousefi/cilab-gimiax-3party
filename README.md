# Installation Process

This project is an on going project in **alpha** state. Some things are fixed in order to compile it successfully but they will be fixed. Those things are:

- Update to a proper cmake usage
- Improve project folder structure
- 3party binaries when possible
- OSX and GNU/Linux first class citizens


## Third Party Package

This repository is a dependence of [Gimiax][1] so you have to follow [these instructions][2] in order to compile properly Gimiax.

[1]: https://bitbucket.org/CISTIB/cilab-gimiax/overview
[2]: https://bitbucket.org/CISTIB/cilab-gimiax