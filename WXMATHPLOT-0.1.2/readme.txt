wxMathPlot is a framework for mathematical graph plotting in wxWindows.

wxMathPlot v0.1.2
http://sourceforge.net/projects/wxmathplot/files/wxmathplot/0.1.2/wxMathPlot-0.1.2.tar.gz/download

Modifications for GIMIAS:

- Modified files: .\wxMathPlot-0.1.2\CMakeLists.txt